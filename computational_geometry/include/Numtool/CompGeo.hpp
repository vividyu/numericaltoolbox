/**
 * @file CompGeo.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Computational Geometry Utilities
 * @version 0.1
 * @date 2022-08-08
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "transformations/Jacobians.hpp"