#include <Numtool/matrixT.hpp>
namespace MATH::COMP_GEO {
    namespace TRANSFORMATIONS {
        template<typename T, int ndim>
        struct Jacobian {
            T jac[ndim][ndim]; /// The jacobian

            /**
             * @brief convert to 1D array
             * 
             * @return double * the head of the 2d jacobian array 
             */
            operator double *() {return jac[0];}

            T AdjJ[ndim][ndim];

            T detJ;

            void formAdjandDet(){
                detJ = MATH::MATRIX_T::determinant<ndim, T>(jac);
                MATH::MATRIX_T::adjugate<ndim, T>(jac, AdjJ);
            }
        };
    }
}