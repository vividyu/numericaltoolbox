/**
 * @file optimization.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Optimization framework
 * @version 0.1
 * @date 2021-08-23
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#pragma once
#include <Numtool/tensor.hpp>
#include <Numtool/tensorUtils.hpp>

#include <petscsys.h>
#include <petscksp.h>
#include <mpi.h>
namespace OPTIMIZATION {

    /**
     * @brief A cost function (the residual of a nonlinear optimization problem)
     * 
     * @tparam T the floating point type
     * @tparam IDX_TYPE the index type
     */
    template<typename T, typename IDX_TYPE>
    class CostFunction{
        private:
        static constexpr T EPSILON = 1e-8;
        public:

        /**
         * @brief Evaluate the residuals at a given point x
         * 
         * @param x the point (size = n)
         * @param f the residuals (size = m)
         */
        virtual void eval(T *x, T *f) = 0;

        /**
         * @brief Evaluate the jacobian to a dense matrix
         * 
         * @param x the point to evaluate the jacobian at
         * @param jac the jacobian matrix
         */
        virtual void jacobianDense(T *x, MATH::TENSOR::Matrix<T, IDX_TYPE> &jac) {
            T *r = new T[getM()];
            T *r_peturb = new T[getM()];
            T epsilon = std::max(EPSILON, MATH::TENSOR::norm<T, IDX_TYPE, 2>(r, getM()) * EPSILON);
            eval(x, r);
            for(IDX_TYPE j = 0; j < getN(); j++){
                T temp = x[j];
                x[j] += epsilon;
                eval(x, r_peturb);
                for(IDX_TYPE i = 0; i < getM(); i++){
                    jac[i][j] = (r_peturb[i] - r[i]) / epsilon;
                }
                x[j] = temp;
            }
        }

        /**
         * @brief Evaluate the jacobian to a sparse matrix
         * 
         * @param x the point to evaluate the jacobian at
         * @param jac the jacobian matrix
         */
        //virtual void jacobianSparse(const T *x, MATH::TENSOR::SPARSE::SparseMatrix<T, IDX_TYPE> &jac) = 0;

        /**
         * @brief Evaluate the hessian to a dense matrix
         * 
         * @param x the point to evaluate the hessian at
         * @param jac the hessian matrix
         */
        virtual void hessianDense(const T *x, MATH::TENSOR::Matrix<T, IDX_TYPE> &jac) {};

        /**
         * @brief Evaluate the hessian to a sparse matrix
         * 
         * @param x the point to evaluate the hessian at
         * @param jac the hessian matrix
         */
        //virtual void hessianSparse(const T *x, MATH::TENSOR::SPARSE::SparseMatrix<T, IDX_TYPE>  &jac) = 0;

        virtual IDX_TYPE getM() = 0;
        virtual IDX_TYPE getN() = 0;

    };

    template<typename T, typename IDX>
    class CostFunctionLS {
        private:
        CostFunction<T, IDX> *cost;
        MATH::TENSOR::Vector<T, IDX> fvec;
        static constexpr T EPSILON = 1e-8;

        public:
        CostFunctionLS(CostFunction<T, IDX> &costref) : cost{&costref} , fvec(costref.getM()) {}

        IDX getN() {return cost->getN();}
        T eval(T *x){
            cost->eval(x, fvec.data);
            return fvec.dot(fvec);
        }

        void gradient(T *x, T *g){
            T fbase = eval(x);
            T epsilon = std::max(EPSILON, fbase * EPSILON);
            for(IDX i = 0; i < cost->getN(); i++){
                T tmp = x[i];
                x[i] += epsilon;
                T fpeturb = eval(x);
                x[i] = tmp;
                g[i] = (fpeturb - fbase) / epsilon;
            }
        }

        void hess(T *x, MATH::TENSOR::Matrix<T, IDX> &hess){
            MATH::TENSOR::Vector<T, IDX> g(cost->getN());
            MATH::TENSOR::Vector<T, IDX> gpeturb(cost->getN());
            gradient(x, g.data);
            T epsilon = std::max(EPSILON, g.norm() * EPSILON);
            for(int i = 0; i < cost->getN(); i++){
                T tmp = x[i];
                x[i] += epsilon;
                gradient(x, gpeturb.data);
                x[i] = tmp;
                for(int j = 0; j < cost->getN(); j++ ){
                    hess[i][j] = (gpeturb[j] - g[j]) / epsilon;
                }
            }
        }
    };

    /**
     * @brief A Cost function representing an optimization problem
     * written for use with Petsc
     * 
     * @tparam T the floating point type
     * @tparam IDX the indexing type
     */
    template<typename T, typename IDX>
    class CostFunctionPetsc {
        public:

        /**
         * @brief get the number of unknowns
         * 
         * @return IDX the number of unknowns
         */
        virtual IDX getN() = 0;

        /**
         * @brief get the number of Equations
         * 
         * @return IDX the number of equations
         */
        virtual IDX getM() = 0;



        explicit CostFunctionPetsc(){
            if constexpr (!std::same_as<T, PetscReal>) ERR::throwError("Real type does not match petsc!");
            if constexpr (!std::same_as<IDX, PetscInt>) ERR::throwError("Integer type does not match petsc!");
        }

        T JacobianCutoff = 1e-8; // cutoff for when to include a value in the jacobian when using default calculation
        T EPSILON = 1e-8;
        virtual PetscErrorCode residual(Vec x, Vec f) = 0;

        virtual PetscErrorCode formResidualAndJacobian(Vec x, Vec f, Mat J) {
            PetscInt first_row;
            PetscInt last_row;
            Vec r_peturb, x_peturb;
            PetscReal *xview, *rview;
            const PetscReal *r_peturb_view;

            residual(x, f);
            PetscReal fnorm, fnormAll;
            VecNorm(f, NORM_2, &fnorm);
            fnorm = SQUARED(fnorm);
            MPI_Allreduce(&fnorm, &fnormAll, 1, MPI_DOUBLE, MPI_SUM, PETSC_COMM_WORLD);
            fnormAll = std::sqrt(fnormAll);
            PetscReal epsilon = std::max(EPSILON, EPSILON * fnormAll);
            PetscReal epsilonm = 1.0 / epsilon;
            VecDuplicate(f, &r_peturb);
            VecDuplicate(x, &x_peturb);
            VecCopy(x, x_peturb);

            MatGetOwnershipRange(J, &first_row, &last_row);
            for(PetscInt j = first_row; j < last_row; j++){
                // peturb x
                VecGetArray(x_peturb, &xview);
                PetscReal temp_x = xview[j - first_row];
                xview[j - first_row] += epsilon;
                VecRestoreArray(x, &xview);

                // get the residual
                residual(x_peturb, r_peturb);

                // Calculate the Jacobian entries
                VecAXPY(r_peturb, -1.0, f);
                VecScale(r_peturb, epsilonm);

                // Assign in matrix
                VecGetArrayRead(r_peturb, &r_peturb_view);
                for(int i = 0; i < getM(); i++){
                    if(r_peturb_view[i] >= fnormAll * JacobianCutoff || j == i) {
                        CHKERRQ(MatSetValue(J, i, j, r_peturb_view[i], ADD_VALUES));
                    }
                }
                VecRestoreArrayRead(r_peturb, &r_peturb_view);

                // undo peturbation
                VecGetArray(x_peturb, &xview);
                xview[j - first_row] = temp_x;
                VecRestoreArray(x_peturb, &xview);
            }
            MatAssemblyBegin(J, MAT_FINAL_ASSEMBLY); MatAssemblyEnd(J, MAT_FINAL_ASSEMBLY);
            return 0;
        }



        virtual PetscErrorCode formJacobian(Vec x, Mat J) {

            return 0;
        }

    };

    template class CostFunction<double, long long>;
    template class CostFunction<double, long>;
    template class CostFunction<double, int>;

    template class CostFunction<long double, long long>;
    template class CostFunction<long double, long>;
    template class CostFunction<long double, int>;

    template class CostFunction<float, long long>;
    template class CostFunction<float, long>;
    template class CostFunction<float, int>;

    
    /**
     * @brief Levenberg Marquardt Optimization Algorithm
     * 
     * Nocedal-Wright Trust region implementation
     * 
     * @tparam T the floating point type
     * @tparam IDX_TYPE the index type
     */
/*
    template<typename T, typename IDX_TYPE>
    class LevenbergMarquardt {
        public:
        T deltamax = 100; /// the upper bound for the trust region size
        T deltainit = 100; /// trust region size set to initial size
        T eta;   /// actual reduction threshold
        IDX_TYPE kmax = 1000; /// maximum number of iterations

        bool solve(T *x, CostFunction<T, IDX_TYPE> &f);


    };
*/    
}