/**
 * @file pso.hpp
 * @author Guanyu Hou
 * @brief Particle Swarm Optimization
 * @version 0.1
 * @date 2022-12-03
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "optimization.hpp"
#include <Numtool/tensorUtils.hpp>
#include <Numtool/tensor.hpp>
#include <iostream>
#include <random>
#include <ctime>
#include <limits>
// #include <cmath>
#pragma once
namespace OPTIMIZATION
{

    /**
     * @brief Particle Swarm Optimization
     *
     * @tparam T The floating point type
     * @tparam IDX the index type
     */
    template <typename T, typename IDX>
    class PSO
    {
    private:
        // ---------------------
        // - SOLVER PARAMETERS -
        // ---------------------
        T w;  /// an initial constant (slightly less than 1)
        T c1; /// cognitive coefficient
        T c2; /// social coefficient

        IDX max_gen; /// Maximum allowed iterations
        T p_size;    /// the swarm size

        T vMax; /// max velocity
        T vMin; /// min velocity
        T xMax; /// max position
        T xMin; /// min position

    public:
        /**
         * @brief Construct a new PSO Solver
         *
         * @param w
         * @param c1
         * @param c2
         * @param p_size
         * @param vMax
         * @param vMin
         * @param xMax
         * @param xMin
         */
        PSO(
            T w = 0.9,
            T c1 = 1.8,
            T c2 = 1.8,
            IDX max_gen = 1000,
            T p_size = 100,
            T vMax = 1,
            T vMin = -1,
            T xMax = 1,
            T xMin = -1) : w(w), c1(c1), c2(c2), max_gen(max_gen), p_size(p_size),
                           vMax(vMax), vMin(vMin), xMax(xMax), xMin(xMin)
        {
        }

        /**
         * @brief Solve the nonlinear problem using the cost function
         *
         * @param cost the cost function
         * @param x set x to the initial guess or zero fill
         * @return the final lambda
         */
        T solve(CostFunction<T, IDX> &cost, MATH::TENSOR::Vector<T, IDX> &x, std::vector<T> &residuals)
        {
            using namespace MATH::TENSOR;

            // initialize variables
            MATH::TENSOR::Vector<T, IDX> r(cost.getM());
            IDX dimension = cost.getN();

            std::vector<MATH::TENSOR::Vector<T, IDX>> v_xstep(p_size);
            std::vector<MATH::TENSOR::Vector<T, IDX>> v_vstep(p_size);

            std::vector<MATH::TENSOR::Vector<T, IDX>> pBestX(p_size);
            MATH::TENSOR::Vector<T, IDX> gBestX(dimension);

            std::vector<T> v_pBest(p_size);
            T gBest = std::numeric_limits<T>::max();

            // initialize random seed
            std::default_random_engine e(time(NULL));
            std::uniform_real_distribution<T> randV(vMin, vMax);
            std::uniform_real_distribution<T> randX(xMin, xMax);
            std::uniform_real_distribution<T> rand_r1(-1, 1);
            std::uniform_real_distribution<T> rand_r2(-1, 1);

            // initialize the particle, velocity and v_pBest
            for (IDX p = 0; p < p_size; p++)
            {
                MATH::TENSOR::Vector<T, IDX> xstep(dimension);
                MATH::TENSOR::Vector<T, IDX> vstep(dimension);
                for (IDX i = 0; i < dimension; i++)
                {
                    xstep[i] = randX(e);
                    vstep[i] = randV(e);
                }
                v_xstep[p] = xstep;
                v_vstep[p] = vstep;

                x = xstep;
                cost.eval(x.data, r.data);
                v_pBest[p] = r.norm();
            }

            // initialize the gBest
            for (IDX g = 0; g < p_size; g++)
            {
                // x = v_xstep[g];
                cost.eval(v_xstep[g].data, r.data);
                T g0 = r.norm();
                if (g0 < gBest)
                {
                    gBest = g0;
                    gBestX = v_xstep[g];
                    x = v_xstep[g];
                    residuals.push_back(g0);
                }
            }

            IDX k;
            for (k = 0; k < max_gen; k++)
            { // main loop

                for (IDX p1 = 0; p1 < p_size; p1++)
                {
                    // update all particles
                    for (IDX i = 0; i < dimension; i++)
                    {
                        // update velocity first
                        T r1 = rand_r1(e);
                        T r2 = rand_r2(e);

                        v_vstep[p1][i] = w * v_vstep[p1][i] +
                                         c1 * r1 * (pBestX[p1][i] - v_xstep[p1][i]) +
                                         c2 * r2 * (gBestX[i] - v_xstep[p1][i]);
                        v_vstep[p1][i] = v_vstep[p1][i] < vMin ? vMin : v_vstep[p1][i];
                        v_vstep[p1][i] = v_vstep[p1][i] > vMax ? vMax : v_vstep[p1][i];

                        // update location
                        v_xstep[p1][i] = v_xstep[p1][i] + v_vstep[p1][i] * 1; // default time set to 1
                        v_xstep[p1][i] = v_xstep[p1][i] < xMin ? xMin : v_xstep[p1][i];
                        v_xstep[p1][i] = v_xstep[p1][i] > xMax ? xMax : v_xstep[p1][i];
                    }

                    // calculate current fitness
                    // x = v_xstep[p1];
                    cost.eval(v_xstep[p1].data, r.data);
                    T pFitness = r.norm();

                    // update best fitness and pBest
                    if (pFitness < v_pBest[p1])
                    {
                        v_pBest[p1] = pFitness;
                        for (T i = 0; i < dimension; i++)
                        {
                            pBestX[p1][i] = v_xstep[p1][i];
                        }

                        // update pBest residuals
                        // x = pBestX[p1];
                        // cost.eval(x.data, r.data);
                        // residuals.push_back(r.norm());
                    }
                }

                // update global best
                for (IDX p2 = 0; p2 < p_size; p2++)
                {
                    // search global best fitness
                    T gFitness = std::numeric_limits<T>::max();
                    //x = v_xstep[p2];
                    cost.eval(v_xstep[p2].data, r.data);
                    gFitness = r.norm();

                    if (gFitness < gBest)
                    {
                        gBest = gFitness;

                        // update gBest
                        for (T j = 0; j < dimension; j++)
                        {
                            gBestX[j] = v_xstep[p2][j];
                        }

                        // update gBest residuals
                        x = gBestX;
                        cost.eval(x.data, r.data);
                        residuals.push_back(r.norm());
                    }
                }
            }
            std::cout << "PSO finished in: " << k << " iterations.\n";
            return 0.0;
        }
    };

}