/**
 * @file single_objective_cg.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Single objective Conjugate gradient algorithm
 * @version 0.1
 * @date 2022-10-20
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once
#include "RAJA/RAJA.hpp"
#include "single_objective.hpp"
#include <Numtool/tensorUtils.hpp>
namespace OPTIMIZATION {
    template<typename T, typename IDX>
    class SingleObjectiveCG {
        public:

        IDX kmax = 100; /// the maximum number of iterations
        T gtol = 1e-8; /// Tolerance for gradient convergence criteria
        void solve(SingleObjectiveCost<T, IDX> &cost, T *x){
            T *g = new double[cost.getN()];
            for(IDX k = 0; k < kmax; k++){
                cost.calculateGradient(x, g);

            }
            // cleanup
            delete[] g;
        }
    };
}