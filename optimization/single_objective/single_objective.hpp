/**
 * @file single_objective.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Single objective optimization problems
 * @version 0.1
 * @date 2022-10-20
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#pragma once

namespace OPTIMIZATION {


    /**
     * @brief Single objective cost function that
     * Vector Type: pointers
     * 
     * @tparam T the floating point type
     * @tparam IDX the index type
     */
    template<typename T, typename IDX>
    class SingleObjectiveCost {
        private:
        static constexpr T EPSILONFD = 1e-8;

        public:

        /**
         * @brief Evaluate the cost function at a given point
         * 
         * @param x the point x which is an indexable vector
         * @return T the value of the cost function at the given point
         */
        virtual T eval(T *x) = 0;

        /**
         * @brief Evaluate the gradient of the function at a given point
         * 
         * @param [in] x the point vector
         * @param [out] g the gradient at the point 
         * (must be preallocated if using default implementation)
         */
        virtual void calculateGradient(T *x, T *g){
            T f0 = eval(x);
            for(IDX i = 0; i < getN(); i++){
                T temp = x[i];
                x[i] += EPSILONFD;
                T fph = eval(x);
                g[i] (fph - f0) / EPSILONFD;
                x[i] = temp; 
            }
        }

        /**
         * @brief applies Hb
         * Where H is the hessian evaluated at the point x
         * 
         * The default implementation uses a directional derivative style
         * matrix-free implementation 
         * 
         * @param [in] x the point at which to evaluate the hessian
         * @param [in] b the vector to apply the hessian to
         * @param [out] result the result of the application of the hessian matrix onto the vector b
         */
        virtual void applyHessian(T *x, T *b, T *result){
            // TODO: 
            // also think about allocation for temporary V type objects
        }

        /**
         * @brief Get the size of the input vector
         * 
         * @return IDX the size of the input vector
         */
        virtual IDX getN() = 0;

    };
}