#include "optimization.hpp"
#ifdef USE_EIGEN
#include <Eigen/Sparse>
#include <Eigen/SparseQR>

namespace OPTIMIZATION {

    using namespace Eigen;

    template<typename T, typename IDX_TYPE>
    void GaussNetwonStep(T *Qtb, CostFunction<T, IDX_TYPE> &f, T *p){

    }

    template<typename T, typename IDX_TYPE>
    bool LevenbergMarquardt<T, IDX_TYPE>::solve(T *x, CostFunction<T, IDX_TYPE> &f){
        

        T delta = deltainit;
        Vector<T, Dynamic> diag = Vector<T, Dynamic>::Ones(f.n);
        

        for(IDX_TYPE k = 0; k < kmax; k++){ // MAIN LOOP

            // Get the residual and Jacobian
            Vector<T, Dynamic> b{f.m};
            MATH::TENSOR::SPARSE::SparseMatrix<T, IDX_TYPE> jac{};

            f.eval(x, b.data());
            f.jacobianSparse(jac);

            // convert the jacobian to eigen
            SparseMatrix<T, 0, IDX_TYPE> eigenmat{};
            jac.toEigenMat(eigenmat);
            eigenmat.makeCompressed();

            // get the QR factors
            SparseQR<
                SparseMatrix<T, 0, IDX_TYPE>,
                COLAMDOrdering<IDX_TYPE>
            > qrdecomp{eigenmat};
            qrdecomp.analyzePattern();
            qrdecomp.factorize();
            SparseMatrix<T, 0, IDX_TYPE> Q;
            Q = qrdecomp.matrixQ;
            
            // get the gauss newton step, sparse QR solve should be fine even with singular matrix
            // b will already be pivoted back
            Vector<T, Dynamic> p = qrdecomp.solve(b);

            // -----------
            // get lambda
            // -----------
            constexpr T sigma = 0.1; // the desired relative error in ||Dp(\lambda)||
            // work array: the outer product of p and diag: Dq(\lambda)
            Vector<T, Dynamic> q{p};
            q.cwiseProduct(diag);
            T dxnorm = q.norm(); // norm of p * diag
            T phi = dxnorm - delta; // get phi at the origin (lambda = 0)

            if(phi < sigma * delta){
                break; // p is the step
            }

            SparseMatrix<T, 0, IDX_TYPE> Rref = qrdecomp.matrixR();
            SparseMatrix<T, 0, IDX_TYPE> RT{Rref}; // sort the rows
            RT = RT.transpose();

            // Upper and Lower bounds
            T parL = 0;
            T parU;
            // if J is not rank deficient find parL_0 = -\phi(0) / phi'(0), otherwise leave at 0
            if(Q.rank() == f.n()){
                // compute R^-T \pi^T D^T q(\lambda) but ignore the transposes becuase its inside a norm
                Vector<T, Dynamic> pTdTq{diag};
                pTdTq = pTdTq *qrdecomp.colsPermutation();
                pTdTq.cwiseProduct(q);
                pTdTq.cwiseProduct(1.0 / dxnorm);
                
            }

        }
        return true;
    }
}
#endif