/**
 * @file testProblems.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Test problems for optimization
 * @version 0.1
 * @date 2021-08-24
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "optimization.hpp"
#include <Numtool/Errors.h>

namespace OPTIMIZATION {

    /**
     * @brief A trivial multidimensional case of the rosenbrock function where the zero is always at the origin
     * 
     * @tparam T 
     * @tparam IDX_TYPE 
     */
    template<typename T, typename IDX_TYPE>
    class Rosenbrock : public CostFunction<T, IDX_TYPE>{
        private:
        using cf = CostFunction<T, IDX_TYPE>;
        static constexpr T b = 100;
        IDX_TYPE m;
        IDX_TYPE n;

        public:

        Rosenbrock(IDX_TYPE m, IDX_TYPE n) : m(m), n(n){}

        void eval(T *x, T *f);

        void jacobianDense(T *x, MATH::TENSOR::Matrix<T> &jac){ ERR::throwError("Not implemented"); }

        //void jacobianSparse(const T *x, MATH::TENSOR::SPARSE::SparseMatrix<T, IDX_TYPE> &jac);

        void hessianDense(const T *x, MATH::TENSOR::Matrix<T> &jac){ ERR::throwError("Not implemented"); }

        //void hessianSparse(const T *x, MATH::TENSOR::SPARSE::SparseMatrix<T, IDX_TYPE>  &jac){ ERR::throwError("Not implemented"); }

        IDX_TYPE getM(){ return m; }
        IDX_TYPE getN(){ return n; }
    };

    /**
     * @brief A multidimensional nonlinear problem from Numerical Analysis
     * 
     * @tparam T the floating point type
     * @tparam IDX_TYPE the index type
     */
    template<typename T, typename IDX_TYPE>
    class Test1 : public CostFunction<T, IDX_TYPE>{
        private:
        using cf = CostFunction<T, IDX_TYPE>;
        static constexpr T b = 100;

        public:

        Test1(){}

        void eval(T *x, T *f);

//        void jacobianDense(T *x, MATH::TENSOR::Matrix<T, IDX_TYPE> &jac);

        //void jacobianSparse(const T *x, MATH::TENSOR::SPARSE::SparseMatrix<T, IDX_TYPE> &jac){ ERR::throwError("Not implemented"); }

        void hessianDense(const T *x, MATH::TENSOR::Matrix<T, IDX_TYPE> &jac){ ERR::throwError("Not implemented"); }

        //void hessianSparse(const T *x, MATH::TENSOR::SPARSE::SparseMatrix<T, IDX_TYPE>  &jac){ ERR::throwError("Not implemented"); }

        IDX_TYPE getM(){return 4;}
        IDX_TYPE getN(){return 4;}
    };
    
    template class Test1<double, int>;

   /*
   template<typename T, typename IDX_TYPE>
   class QuadratricBowl: public CostFunction<T, IDX_TYPE>{
       public:

        QuadratricBowl(IDX_TYPE m, IDX_TYPE n);

       void eval(const T *x, T *f)
   };
   */
}