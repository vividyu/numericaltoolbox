#include "gltr.hpp"
#include <Numtool/tensor.hpp>
#include <Numtool/MathUtils.hpp>
#include <Numtool/tensorUtils.hpp>
#include <cmath>
#include <iostream>
namespace OPTIMIZATION {
    /**
     * @brief Factorize the matrix T_k + \lambda I_{k+1} into BDB^T 
     * where B is unit upper bidiagonal
     * and D is diagonal
     * Only the off diagonal entries of B are stored
     * 
     * @tparam T The floating point type
     * @tparam IDX the index type
     * @param [in] Tmat the Tridiagonal matrix
     * @param [in] lambda the lambda to add
     * @param [out] Boff the upper diagonal of the B matrix as a vector
     * @param [out] D the diagonal matrix as a vector
     */
    template<typename T, std::integral IDX>
    void BDBFactorize(
        MATH::TENSOR::SymTridiagMat<T, IDX> &Tmat,
        T lambda,
        std::vector<T> &Boff,
        std::vector<T> &D)
    {
        IDX k = Tmat.getM();
        Boff.resize(k);
        D.resize(k + 1);
        D[k] = lambda;
        Boff[k-1] = 0;
        D[k-1] = Tmat.getMaindiag()[k-1];
        for(IDX j = k-2; j >= 0; j--){
            Boff[j] = Tmat.getOffdiag()[j] / D[j + 1];
            D[j] = Tmat.getMaindiag()[j] - SQUARED(Boff[j]) * D[j+1];
        }
        // TODO: Note to self, because of sparsity of h in the algorithm (see eq 5.10)
        //       maybe don't store entire vectors and compute h on the fly and only store
        //       relevant entries of B and D
    }

    template<typename T, std::integral IDX>
    void solveh(std::vector<T> &Boff, std::vector<T> &D, T gamma0, MATH::TENSOR::Vector<T, IDX> &h){
        IDX n = D.size();
        h.resize(n);
        T rhs0 = -gamma0 / D[0];
        h[0] = rhs0;
        for(IDX i = 1; i < n; i++) h[i] = -Boff[i-1] * h[i-1];
    }

    template<typename T, std::integral IDX>
    void solvew(std::vector<T> &Boff, MATH::TENSOR::Vector<T, IDX> &h, std::vector<T> &w){
        IDX n = h.size();
        w.resize(n);
        w[n-1] = h[n-1];
        for(IDX i = n-2; i >= 0; i--) w[i] = h[i] - Boff[i] * w[i+1];
    }

    /**
     * @brief Newton iteration (Algorithm 5.2 in Gould et al)
     * Used for finding the step when the minimum is on the boundary
     * @tparam T The floating point type
     * @tparam IDX the index type
     * @param [in] tri the tridiagonal matrix
     * @param [in] delta the trust region radius
     * @param [in] gamma0 the first gamma from CG iteration
     * @param [in, out] lambda the initial guess for lambda (will get updated) best to use previous iteration
     * @param [out] h the h vector from Gould et al
     */
    template<typename T, std::integral IDX>
    void hNewtonTRBdy(MATH::TENSOR::SymTridiagMat<T, IDX> &tri, T delta, T gamma0, T &lambda, MATH::TENSOR::Vector<T, IDX> &h){
        using namespace MATH::TENSOR;
        constexpr double EPSILON = 1e-6;
        std::vector<T> Boff{};
        std::vector<T> D{};
        std::vector<T> w{};
        T normh2; // the 2 norm of h

        BDBFactorize(tri, lambda, Boff, D);
        solveh<T, IDX>(Boff, D, gamma0, h);
        solvew<T, IDX>(Boff, h, w);
        IDX n = h.size();
        normh2 = h.norm();
        for(IDX k = 0; k < 10 && std::abs(normh2 - delta) > EPSILON; k++){
            T normwDinv = 0; // the D^-1 norm of w
            for(IDX i = 0; i < w.size(); i++){
                normwDinv += SQUARED( w[i] / D[i] * w[i]);
            }
            normwDinv = sqrt(normwDinv);
            // update lambda
            lambda += (normh2 - delta) / delta * SQUARED(normh2) / normwDinv;
            // compute new h and w
            BDBFactorize(tri, lambda, Boff, D);
            solveh<T, IDX>(Boff, D, gamma0, h);
            solvew<T, IDX>(Boff, h, w);
            normh2 = h.norm();
        }
        
    }

    template<typename T, std::integral IDX>
    void GLTR_TR<T, IDX>::testFactorize(){
        using namespace std;
        using namespace MATH::TENSOR;
        SymTridiagMat<T, IDX> tri{};
        tri.addRow(4);
        tri.addRow(5, -1);
        tri.addRow(6, -2);
        tri.addRow(7, -3);
        std::cout << tri;

        std::vector<T> Boff{};
        std::vector<T> D{};
        BDBFactorize(tri, 0.1, Boff, D);
        Vector<T, IDX> h{};
        solveh<T, IDX>(Boff, D, 2, h);
        std::vector<T> w{};
        solvew<T, IDX>(Boff, h, w);
    }

    enum class TR_STATUS {
        INTERIOR,
        BOUNDARY,
        EXTERIOR
    };

    template<typename T, std::integral IDX>
    void applyH(
        MATH::TENSOR::Matrix<T, IDX> &jac,
        MATH::TENSOR::Vector<T, IDX> &x,
        MATH::TENSOR::Vector<T, IDX> &out)
    {
        MATH::TENSOR::Vector<T, IDX> temp(jac.m);
        jac.mult(x, temp);
        jac.TransposeMultiply(temp, out);
    }

    constexpr double subproblemmax = 0.5; //TODO: move into class constant maybe based on jac size

    /**
     * @brief GLTR subproblem according to Gould et al
     * 
     * @tparam T The floating point type
     * @tparam IDX the index type
     * @param [in] jac the jacobian
     * @param [in] M the approximation of H = J'*J
     * @param [in] r the residual for the current x
     * @param [out] s the step to take
     * @return TR_STATUS 
     */
    template<typename T, std::integral IDX>
    TR_STATUS gltrSubproblem(
        MATH::TENSOR::Matrix<T, IDX> &jac,
        MATH::TENSOR::Matrix<T, IDX> &M,
        MATH::TENSOR::Matrix<T, IDX> &Minv, // TODO:: extract m and minv into a preconditioner class that extends matrix
        T delta,
        MATH::TENSOR::Vector<T, IDX> &r,
        MATH::TENSOR::Vector<T, IDX> &s
    ){
        constexpr double TOLERANCE = 1e-8;
        using namespace MATH::TENSOR;
        // might be able to avoid storing all the iterates
        IDX n = jac.getN();
        std::vector<T> alphak{};
        std::vector<T> gammak{};
        std::vector<T> betak{};
        SymTridiagMat<T, IDX> Tk;
        Vector<T, IDX> v(n);
        Vector<T, IDX> p(n);
        Vector<T, IDX> temp1(n);
        Vector<T, IDX> h(n);
        std::vector<Vector<T, IDX>> Q{};
        bool INTERIOR = true;
        TR_STATUS status = TR_STATUS::INTERIOR;
        // initial values
        T lambda = 0.0;
        Vector<T, IDX> g(r.size());
        jac.mult(r, g); // g = J'*r
        s.zeroFill();
        Minv.mult(g, v);
        gammak.push_back(sqrt(v.dot(g)));
        for(IDX i = 0; i < n; i++) p[i] = -v[i];

        for(IDX k = 0; k < (IDX) (subproblemmax * jac.getN()); k++){ // main iteration
            Vector<T, IDX> Hp(n);
            applyH<T, IDX>(jac, p, Hp); // the M norm of the residual
            alphak.push_back(g.dot(v) / p.dot(Hp));

            // store the first lanczos vector for ease of use
            T sqrtgv = sqrt(g.dot(v));
            Q.push_back(Vector<T, IDX>(n)); // allocate memory for lanczos vector
            for(IDX i = 0; i < n; i++) Q[k][i] = v[i] / sqrtgv;

            if(k == 0){ // first iteration should get optimized out
                Tk.addRow(1);
            } else {
                Tk.addRow(
                    1.0 / alphak[k] + betak[k-1] / alphak[k-1], // main diagonal
                    sqrt(betak[k-1] / abs(alphak[k-1]))
                );
            }
            for(IDX i = 0; i < n; i++) temp1[i] = s[i] + alphak[k] * p[i];
            if(INTERIOR && (alphak[k] <= 0) || std::sqrt(M.matInnerProd(temp1)) >= delta){
                INTERIOR = false;
                status = TR_STATUS::EXTERIOR;
            }
            if(INTERIOR){
                for(IDX i = 0; i < n; i++) s[i] += alphak[k] * p[i];
            } else {
                // tridiagonal trust region subproblem   
                hNewtonTRBdy<T, IDX>(Tk, delta, gammak[0], lambda, h);
            }
            T gvold = g.dot(v);
            for(IDX i = 0; i < n; i++){
                g[i] += alphak[k] * Hp[i];
                Minv.mult(g, v);
            }
            betak.push_back(g.dot(v) / gvold);
            for(IDX i = 0; i < n; i++) p[i] = betak[k] * p[i] - v[i];
            gammak.push_back(sqrt(betak[k]) / abs(alphak[k])); //TODO: probably not necessary to store them all
            if(INTERIOR){ // convergence checks
                if(std::sqrt(Minv.matInnerProd(g)) <= TOLERANCE){ //TODO: double check what test for convergence means
                    break;
                } else if(abs(gammak[k+1] * h[k+1]) <= TOLERANCE){
                    break;
                }
            } else {
                // recover sk = Qk*hk
                s.zeroFill();
                for(IDX j = 0; j <= k; j++){
                    // TODO: maybe try conditionally prestore these once we get into the recovering sk
                    for(IDX i = 0; i < n; i++) s[i] += Q[j][i] * h[j];
                    //TODO: do i have to generate the rest of the iterates until size of Q_k = n ???
                }
            }
            if(k == 0) Tk.getMaindiag()[0] /= alphak[0]; // again should get optimized out
        }
        return status;
    }

    template<typename T, std::integral IDX>
    void GLTR_TR<T, IDX>::solve(CostFunction<T, IDX> &cost, MATH::TENSOR::Vector<T, IDX> &x){
        T rho, delta = delta0;
        // residual
        MATH::TENSOR::Vector<T, IDX> r = MATH::TENSOR::Vector<T, IDX>(cost.getM());
        // residual after taking the step
        MATH::TENSOR::Vector<T, IDX> rstep = MATH::TENSOR::Vector<T, IDX>(cost.getM());
        // the step (p in Nocedal, Wright)
        MATH::TENSOR::Vector<T, IDX> s = MATH::TENSOR::Vector<T, IDX>(cost.getN()); 

        // get the inital residual
        cost.eval(x.data, r.data);
        for(IDX k = 0; k < kmax; k++){ // outer loop
            

            // get the Jacobian (Dense for now)
            MATH::TENSOR::Matrix<T, IDX> jac{cost.getM(), cost.getN()};
            cost.jacobianDense(x.data, jac);

            // Using Identity preconditioner for now
            MATH::TENSOR::Matrix<T, IDX> M(jac.getN(), jac.getN());
            M.identityFill();

            // obtain step from gltr subproblem
            TR_STATUS status = gltrSubproblem<T, IDX>(jac, M, M, delta, rstep, s);

            // evaluate ratio (rho)
            MATH::TENSOR::Vector<T, IDX> xstep(cost.getN());
            for(IDX i = 0; i < cost.getN(); i++) xstep[i] = x[i];
            xstep += s;
            cost.eval(xstep.data, rstep.data);
            T ared = SQUARED(r.norm()) - SQUARED(rstep.norm());
            MATH::TENSOR::Vector<T, IDX> rpred(cost.getM());
            jac.mult(s, rpred);
            rpred += r;
            T pred = r.norm() - rpred.norm();
            rho = ared / pred;

            // Nocedal, Wright values used when not a parameter
            if(rho < 0.25){
                delta *= 0.25;
            } else {
                if(rho > 0.75 && status != TR_STATUS::INTERIOR){
                    delta = std::min(2 * delta, deltahat);
                } // else delta remains the same
            }
            //if(ared < 0){ // only take the step if the objective reduces
                if(rho > eta) {
                    x = xstep; // if within acceptable region, take the step
                    r = rstep;
                }
            //} else {
            //    delta *= 0.5;
            //}

            std::cout << "NL iteration " << k << " delta: " << delta << "\n";

        } // end outer loop
    }
}