/**
 * @file gltr.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Generalized Lanczos Trust region
 * 
 * reference:
 * M Gould, N. I., Lucidi, S., Roma, M., Toint, P. L., & Optim, S. J. (n.d.).
 * SOLVING THE TRUST-REGION SUBPROBLEM USING THE LANCZOS METHOD *.
 * Retrieved October 24, 2021, from http://www.siam.org/journals/siopt/9-2/32273.html
 * @version 0.1
 * @date 2021-12-17
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#pragma once
#include "optimization.hpp"
#include <Numtool/tensor.hpp>

namespace OPTIMIZATION {

    template<typename T, std::integral IDX>
    class GLTR_TR {
        private:
        /// overall bound on step size
        T deltahat = 100; 
        /// initial TR radius
        T delta0 = .1;
        /// ratio threshold for accepting step
        T eta = 0.125;
        /// maximum number of iterations
        IDX kmax = 50;

        public:
        void testFactorize();

        /**
         * @brief Solve the normal equations with gltr
         * @param [in] cost the cost function
         * @param [in, out] x the initial guess, gets set to the final solution
         */
        void solve(CostFunction<T, IDX> &cost, MATH::TENSOR::Vector<T, IDX> &x);

        void setKmax(IDX arg){ kmax = arg; }

        void setDelta0(T arg) {delta0 = arg;}
    };

    template class GLTR_TR<double, int>;

}