/**
 * @file TrustRegion.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Generalized Trust Region approach for nonlinear least squares
 * @version 0.1
 * @date 2022-01-03
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#pragma once
#include "optimization.hpp"
#include <Numtool/tensor.hpp>
#include <Numtool/MathUtils.hpp>
#include <Numtool/tensorUtils.hpp>

namespace OPTIMIZATION {
    namespace TR_OPTIONS {
        enum LinearSolver {
            CG
        };

        enum ConvergenceTest {
            ABSOLUTE_TOLERANCE,
            CURVATURE_BASED,
            CURVATURE_BASED_CS
        };

        enum SubproblemStatus {
            INTERIOR,
            BOUNDARY,
            EXTERIOR
        };
    }


    /**
     * @brief The Subproblem Solver
     * Solves the trust region subproblem
     * 
     * @tparam T The floating point type
     * @tparam IDX the index type
     * @tparam LSolver the linear solver (CG by default)
     * @tparam L the linear operator for the hessian approximation (dense row major matrix by default) 
     */
    template<
        typename T,
        typename IDX,
        TR_OPTIONS::LinearSolver LSolver = TR_OPTIONS::LinearSolver::CG,
        typename L = MATH::TENSOR::Matrix<T, IDX>
    >
    class Subproblem {

        public:
        /**
         * @brief Solve the Trust region subproblem
         * 
         * @param [in] H The approximate hessian
         * @param [in] g The residual of the 1d problem g = -J^T r
         * @param [in] delta the trust region radius
         * @param [out] p the step that solves the trust region subproblem
         * @param [out] pred the predicted reduction for this model
         * @return TR_OPTIONS::SubproblemStatus the location of the step wrt the trust region
         */
        virtual TR_OPTIONS::SubproblemStatus solve(
            L &H,
            MATH::TENSOR::Vector<T, IDX> &g,
            T delta,
            MATH::TENSOR::Vector<T, IDX> &p,
            T &pred
        ) = 0;

    };

    template<
        typename T,
        typename IDX,
        TR_OPTIONS::LinearSolver LSolver = TR_OPTIONS::LinearSolver::CG,
        typename L = MATH::TENSOR::Matrix<T, IDX>
    >
    class Steihaug : public Subproblem<T, IDX, LSolver, L> {
        private:
        T epsilon = 1e-8;
        MATH::TENSOR::PRECON::Preconditioner<T, IDX> *M;

        public:

        Steihaug(MATH::TENSOR::PRECON::Preconditioner<T, IDX> &Marg) : M(&Marg){}

        TR_OPTIONS::SubproblemStatus solve(
            L &H,
            MATH::TENSOR::Vector<T, IDX> &g,
            T delta,
            MATH::TENSOR::Vector<T, IDX> &p,
            T &pred            
        ) requires MATH::TENSOR::InnerProdSpace_LinearOperator<L, T>{
            using namespace MATH::TENSOR;
            Vector<T, IDX> r(p.size());
            Vector<T, IDX> rtilde(p.size());
            Vector<T, IDX> d(p.size());
            Vector<T, IDX> pstep(p.size());
            Vector<T, IDX> temp(p.size());
            TR_OPTIONS::SubproblemStatus status = TR_OPTIONS::SubproblemStatus::INTERIOR;

            IDX kmax = p.size() + 1;

            // initial setup
            p.zeroFill();
            r = g;
            for(IDX i = 0; i < r.size(); i++) r[i] = -r[i]; // r = -g
            M->invmult(r.data, rtilde.data);
            d = rtilde;
            
            // main loop
            for(IDX k = 0; k < kmax; k++){
                T r_dot_rtilde_prev = r.dot(rtilde); // save the dot product of r and rtilde for step 5

                // step 2 in (Steihaug)
                T gamma = H.matInnerProd(d.data);
                if(gamma <= 0){
                    // compute tau s.t ||p + tau * d||_C = Delta
                    T quadratic_c = M->matInnerProd(p.data, p.data) - SQUARED(delta);
                    T quadratic_b = M->matInnerProd(d.data, p.data);
                    T quadratic_a = M->matInnerProd(d.data, d.data);
                    T tau1, tau2;
                    MATH::quadraticSolve(quadratic_a, quadratic_b, quadratic_c, tau1, tau2);
                    // since a, b, are positive by definition and c negative by definition
                    // tau2 will be the positive one
                    MATH::TENSOR::addScaled<T, IDX>(p.data, tau2, d.data, p.size());
                    // went to the boundary so finish iteration
                    status = TR_OPTIONS::SubproblemStatus::BOUNDARY;
                    break; 
                } else {
                    // step 3 in (Steihaug)
                    T alpha = r.dot(rtilde) / gamma;
                    pstep = p;
                    MATH::TENSOR::addScaled<T, IDX>(pstep.data, alpha, d.data, p.size());
                    if(std::sqrt(M->matInnerProd(pstep.data, pstep.data)) < delta){
                        // step 4 in (Steihaug)
                        H.mult(d.data, temp.data);
                        MATH::TENSOR::addScaled<T, IDX>(r.data, -alpha, temp.data, r.size());
                        if(
                            std::sqrt(M->matInnerProd(r.data, r.data)) / 
                            std::sqrt(M->matInnerProd(g.data, g.data))
                            <= epsilon
                        ){
                            p = pstep;
                            // finished at the interior
                            status = TR_OPTIONS::SubproblemStatus::INTERIOR;
                            break;
                        } else {
                            M->invmult(r.data, rtilde.data);
                            T beta = r.dot(rtilde) / r_dot_rtilde_prev;
                            if(k == kmax - 1){ // if on the last iteration just accept prev iterate
                                p = pstep;
                                break;
                            }
                            for(IDX i = 0; i < d.size(); i++) d[i] = rtilde[i] + beta * d[i];
                        }
                    } else {
                        // compute tau s.t ||p + tau * d||_C = Delta
                        T quadratic_c = M->matInnerProd(p.data, p.data) - SQUARED(delta);
                        T quadratic_b = M->matInnerProd(d.data, p.data);
                        T quadratic_a = M->matInnerProd(d.data, d.data);
                        T tau1, tau2;
                        // note if delta is very small and p=0 then this can lead to tau1 = -0 tau2 = -nan
                        MATH::quadraticSolve(quadratic_a, quadratic_b, quadratic_c, tau1, tau2);
                        // since a, b, are positive by definition and c negative by definition
                        // tau2 will be the positive one
                        MATH::TENSOR::addScaled<T, IDX>(p.data, tau2, d.data, p.size());
                        // went to the boundary so finish iteration
                        status =  TR_OPTIONS::SubproblemStatus::BOUNDARY; 
                        break;
                    }
                }
            }
            // more numerically stable version (Conn, Gould, Toint 2000 Section 17.4)
            H.mult(p.data, temp.data);
            pred = -g.dot(p) - 0.5 * p.dot(temp); // Conn et al. use opposite sign convention for pred

            return status;
        }
    };

    /**
     * @brief Gauss-Newton approximation of the hessian
     * H = J^T J
     * @tparam T the floating point type
     * @tparam IDX the idnex type
     */
    template<typename T, typename IDX>
    class GNHessianApprox {
        private:
            MATH::TENSOR::Matrix<T, IDX> *jac;
        public:
        GNHessianApprox(MATH::TENSOR::Matrix<T, IDX> *jac) : jac(jac){}

        void mult(const T *x, T *out) const {
            return jac->ATAx(x, out);
        }

        T matInnerProd(const T *x) const{
            MATH::TENSOR::Vector<T, IDX> temp(jac->getN());
            jac->ATAx(x, temp.data);
            return MATH::TENSOR::innerprod<T, IDX>(temp.data, x, jac->getN());
        }

        IDX getN() const {return jac->getN(); }
        IDX getM() const {return jac->getM(); }

    };

    /**
     * @brief Trust region based solver for nonlinear least squares
     * 
     * References:
     * Conn, A. R., Gould, N. I. M., & Toint, P. L. (2000). Trust Region Methods. Siam. https://doi-org.prox.lib.ncsu.edu/10.1137/1.9780898719857
     * 
     * @tparam T The floating point type
     * @tparam IDX the index type
     * @tparam LSolver the linear solver
     * @tparam test the convergence test
     */
    template<
        typename T,
        typename IDX,
        TR_OPTIONS::LinearSolver LSolver = TR_OPTIONS::LinearSolver::CG,
        TR_OPTIONS::ConvergenceTest test = TR_OPTIONS::ConvergenceTest::CURVATURE_BASED
    >
    class TrustRegion {
    private:
        // ------------------------
        // - Trust Region Options -
        // ------------------------

        /// overall bound on step size
        T deltahat = 100; 
        /// initial TR radius (gets multiplied by initial residual)
        T delta0mult = .1;
        // minimum trust region radius
        T deltamin = 1e-6;
        /// ratio threshold for accepting step
        T eta1 = 0.05;
        /// Ratio threshold for updating radius (see Conn, Gould, Toint 2000)
        T eta2 = 0.9;

        // constants (Conn, Gould, Toint 2000 Section 17.1)
        T alpha1 = 2.5;
        T alpha2 = 0.25;

        /// maximum number of iterations
        IDX kmax = 3000;

        /// The Solver for the trust region subproblem
        Subproblem<T, IDX, LSolver, GNHessianApprox<T, IDX>> *subproblem;


        /// Convergence tolerance
        T epsilon = 1e-8;

        

        // -------------------------
        // - Linear Solver Options -
        // -------------------------
        IDX Linear_epsilon = 1e-8; /// tolerance for iterative linear solvers

    public:

        TrustRegion(Subproblem<T, IDX, LSolver, GNHessianApprox<T, IDX>> *subproblem) :
        subproblem(subproblem) {}

        /**
         * @brief Solve the nonlinear optimization problem
         * @param [in] cost the cost function
         * @param [in, out] x the initial guess, gets set to the final solution
         * @param [out] residuals the residuals for each nonlinear iteration
         */
        void solve(
            CostFunction<T, IDX> &cost,
            MATH::TENSOR::Vector<T, IDX> &x,
            std::vector<T> &residuals
        ) {
            using namespace MATH::TENSOR;
            Vector<T, IDX> xstep(cost.getN());
            Vector<T, IDX> r(cost.getM());
            Vector<T, IDX> rstep(cost.getM());
            Vector<T, IDX> g(cost.getN());
            Vector<T, IDX> p(cost.getN());
            Vector<T, IDX> temp(cost.getN()); // scratch space
            Matrix<T, IDX> jac(cost.getM(), cost.getN());

            
            cost.eval(x.data, r.data);
            T r0norm = r.norm();
            T delta = delta0mult * r0norm;
            for(IDX k = 0; k < kmax; k++){
                // TODO: Try other hessian models
                // Dense matrix J^T J model of hessian 
                cost.jacobianDense(x.data, jac);
                GNHessianApprox<T, IDX> H{&jac};
                jac.TransposeMultiply(r, g);

                // test for convergence
                if constexpr (test == TR_OPTIONS::ConvergenceTest::CURVATURE_BASED){
                    // curvature based test 
                    residuals.push_back(g.norm());
                    if(g.norm() < epsilon) break;
                } else if constexpr (test == TR_OPTIONS::ConvergenceTest::ABSOLUTE_TOLERANCE){
                    T rnorm = r.norm();
                    residuals.push_back(rnorm);
                    if(r.norm() <= epsilon * r0norm) break;

                } else if constexpr (test == TR_OPTIONS::ConvergenceTest::CURVATURE_BASED_CS){
                    // TODO: need power method iteration for 2-norm of jac
                    // (Conn et al section 17.4.3)
                }

                
                T pred;
                subproblem->solve(H, g, delta, p, pred);

                xstep = x;
                xstep += p;
                cost.eval(xstep.data, rstep.data);

                T ared = SQUARED( r.norm() ) - SQUARED(rstep.norm());
                T ratio = ared / pred;
                
                if(ratio >= eta1){
                    r = rstep;
                    x = xstep;                    
                }

                if(ratio >= eta2){
                    delta = std::max(delta, alpha1 * p.norm());
                } else if(ratio < eta1){
                    delta = alpha2 * p.norm();
                    if(delta < deltamin){
                        // accept the step anyways and set delta to the min
                        // cross fingers and hope for the best
                        delta = deltamin;
                        x = xstep;
                        r = rstep;
                    }
                } // else delta remains the same
                
            }

        }

    };
}

