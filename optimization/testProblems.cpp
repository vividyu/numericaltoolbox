#include <cmath>
#include <Numtool/MathUtils.hpp>
#include "testProblems.hpp"

namespace OPTIMIZATION {

    template<typename T, typename IDX_TYPE>
    void Rosenbrock<T, IDX_TYPE>::eval(T *x, T *f){
        for(IDX_TYPE i = 0; i < cf::getM(); i++){
            IDX_TYPE idx1 = i % cf::getN();
            IDX_TYPE idx2 = std::abs(cf::n - i);
            f[i] = SQUARED(x[idx1]) + b * SQUARED((x[idx2] - SQUARED(x[idx1])));
        }
    }

    // template<typename T, typename IDX_TYPE>
    // void Rosenbrock<T, IDX_TYPE>::jacobianSparse(
    //     const T *x, MATH::TENSOR::SPARSE::SparseMatrix<T, IDX_TYPE> &jac
    // ){
    //     for(IDX_TYPE i = 0; i < cf::getM(); i++){
    //         IDX_TYPE idx1 = i % cf::getN();
    //         IDX_TYPE idx2 = std::abs(cf::n - i);
    //         T x1 = x[idx1];
    //         T x2 = x[idx2];
    //         jac.insert(i, idx1, 2 * x1 - 4 * b * x1 * (x2 - x1 * x1));
    //         jac.insert(i, idx2, 2 * b * (x2 - x1 * x1));
    //     }
    // }

    template<typename T, typename IDX>
    void Test1<T, IDX>::eval(T *x, T *f){
        f[0] = x[0] + x[1] - 2;
        f[1] = x[0] * x[2] + x[1] * x[3];
        f[2] = x[0] * x[2] * x[2] + x[1] * x[3] * x[3] - 2.0 / 3.0;
        f[3] = x[0] * CUBED(x[2]) + x[1] * CUBED(x[3]);
    }

//    template<typename T, typename IDX>
    //void Test1<T, IDX>::jacobianDense(T *x, MATH::TENSOR::Matrix<T, IDX> &jac){
        //jac[0][0] = 1;
        //jac[0][1] = 1;
        //jac[0][2] = 0;
        //jac[0][3] = 0;

        //jac[1][0] = x[2];
        //jac[1][1] = x[3];
        //jac[1][2] = x[0];
        //jac[1][3] = x[1];

        //jac[2][0] = SQUARED(x[2]);
        //jac[2][1] = SQUARED(x[3]);
        //jac[2][2] = 2 * x[2] * x[0];
        //jac[2][3] = 2 * x[3] * x[1];
        
        //jac[3][0] = CUBED(x[2]);
        //jac[3][1] = CUBED(x[3]);
        //jac[3][2] = 3 * SQUARED(x[2]) * x[0];
        //jac[3][3] = 3 * SQUARED(x[3]) * x[1];

    //}


}