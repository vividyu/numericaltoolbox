/**
 * @file Sparse.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief 
 * @version 0.1
 * @date 2021-05-26
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#pragma once
#include "MathTypes.hpp"
#include "MathUtils.hpp"
#include <algorithm>
#include <list>
#include <functional>
#warning "Deprecated"
namespace MATH {
    namespace TENSOR {
        /**
         * @brief Sparse Representations
         * 
         */
        namespace SPARSE::DEPRECATED {

            /**
             * @brief A sparse Matrix entry
             * @tparam T the type of data
             */
            template<class T>
            class SparseEntry {
            public:
                // Only Allow Numeric Types
                static_assert(std::is_arithmetic<T>::value, "Must be an Arithmetic type");

                int idx; // the indices of the value
                T value; // the value

                /**
                 * @brief Construct a new Sparse Entry object
                 * 
                 * @param idx_ the index
                 * @param value_ the value
                 */
                SparseEntry(int idx_, T value_): idx(idx_), value(value_){}

                /**
                 * @brief Construct a new Sparse Entry object
                 * 
                 * @param other Entry to copy from
                 */
                SparseEntry(const SparseEntry<T> &other){
                    idx = other.idx;
                    value = other.value;
                }

                bool operator < (const SparseEntry<T> & other) const{
                    return other.idx < idx;
                }

                bool operator > (const SparseEntry<T> & other) const {
                    return other.idx > idx;
                }
                // INTEGER COMPARISONS
                bool operator < (const int & other) const{
                    return other < idx;
                }
                bool operator > (const int & other) const{
                    return other > idx;
                }
            };

            template<class T>
            bool operator <(const int &lhs, const SparseEntry<T> &rhs){
                return lhs < rhs.idx;
            }


            /**
             * @brief A vector of sparse entries
             * 
             * @tparam T The type of data
             */
            template<class T>
            class SparseVec {
                private:


                // Only Allow Numeric Types
                static_assert(std::is_arithmetic<T>::value, "Must be an Arithmetic type");
                public:
                std::vector<SparseEntry<T>> entries; /// the data entries with index

                /**
                 * @brief Index into the vector must call sort() before use
                 * O(log nrEntries) uses binary search
                 * If the Entry at the index does not exist, creates a new one
                 * @param j the index
                 * @return T& the data at the given index
                 */
                T &operator[](const int j){
                    if(entries.size() == 0){
                    entries.push_back(SparseEntry<T>(j, 0));
                    return entries[0].value;
                    }
                    auto it = std::upper_bound(entries.begin(), entries.end(), j);
                    if(it == entries.begin()){
                    // protect from undefined behavior
                    it = entries.insert(it, SparseEntry<T>(j, 0));
                    return it->value;
                    }
                    if((it-1)->idx != j){
                    it = entries.insert(it, SparseEntry<T>(j, 0));
                    return it->value;
                    }
                    return (it-1)->value;
                }

                /**
                 * @brief Get the data at the index without modifying the vector
                 * 
                 * @param j the index
                 * @return const T& the data at the given index
                 */
                const T &operator()(const int j) const{
                    if(entries.size() == 0){
                    return ZERO<T>;
                    }
                    auto it = std::upper_bound(entries.begin(), entries.end(), j);
                    if(it == entries.begin()){
                    return ZERO<T>;
                    }
                    if((it-1)->idx != j){
                        return ZERO<T>;
                    }
                    return (it-1)->value;
                }

                /**
                 * @brief Gets the dot product of the two vectors
                 * 
                 * @param other The other sparse vector
                 * @return T The dot product
                 */
                T operator *(const SparseVec<T> &other){
                    T dotprod = 0;
                    auto it2 = other.entries.begin();
                    for(auto it1 = entries.begin(); it1 != entries.end(); it1++){
                    if(it2 == other.entries.end()){
                        return dotprod;
                    }
                    while(it2->idx < it1->idx){
                        it2++;
                    }
                    if(it2->idx == it1->idx){
                        dotprod += it2->value * it1->value;
                        it2++;
                    }
                    }
                    return dotprod;
                }

                /**
                 * @brief Multiply by a scalar
                 * 
                 * @param other the scalar
                 */
                void operator *=(const double other){
                    for(auto it = entries.begin(); it != entries.end(); it++){
                    it->value *= other;
                    }
                }

                /**
                 * @brief Adds a scalar times a vector to this vector
                 * 
                 * @param other the other vector
                 * @param alpha The scalar to multiply by
                 */
                void scalarxVecAdd(const SparseVec<T> &other, const double alpha){
                    auto it2 = other.entries.begin();
                    for(auto it1 = entries.begin(); it1 != entries.end(); it1++){
                    if(it2 == other.entries.end())
                        break;
                    while(it2->idx < it1->idx){
                        if(!isTiny<T>(it2->value))
                            it1 = entries.insert(it1, SparseEntry<T>(it2->idx, it2->value * alpha));
                        it2++;
                    }
                    if(it2->idx == it1->idx){
                        it1->value += it2->value * alpha;
                        it2++;
                    }
                    }
                    for(; it2 != other.entries.end(); it2++){
                    if(!isTiny<T>(it2->value))
                        entries.push_back(SparseEntry<T>(it2->idx, it2->value * alpha));
                    }  
                }

                /**
                 * @brief Add another vector to this one
                 * 
                 * @param other The other vector
                 */
                void operator +=(const SparseVec<T> &other){
                    scalarxVecAdd(other, 1);
                }

                /**
                 * @brief Subtract another vector from this one
                 * 
                 * @param other the other vector
                 */
                void operator -=(const SparseVec<T> &other){
                    scalarxVecAdd(other, -1);
                }

                
            };


            /**
             * @brief Represents a Matrix as an Adjacency List using SparseEntry to represent an edge
             * a very general use sparse matrix format
             * @tparam T The data type of the items stored in the matrix
             */
            template<class T>
            class GraphSparseMatrix{
                // Only Allow Numeric Types
                static_assert(std::is_arithmetic<T>::value, "Must be an Arithmetic type");
                public:
                std::vector<SparseVec<T>> rows;

                /**
                 * @brief Construct a new Graph Sparse Matrix object
                 * 
                 * @param n The number of rows
                 */
                GraphSparseMatrix(int n){
                    rows = std::vector<SparseVec<T>>(n, SparseVec<T>());
                }

                /**
                 * @brief Construct a new Graph Sparse Matrix object from a dense matrix
                 * 
                 * @param A the dense matrix
                 */
                GraphSparseMatrix(std::vector<std::vector<T>> &A){
                    rows = std::vector<SparseVec<T>>(A.size(), SparseVec<T>());
                    for(int i = 0; i < A.size(); i++){
                    for(int j = 0; j < A[i].size(); j++){
                        if(!isTiny<T>(A[i][j])){
                            rows[i].entries.push_back(SparseEntry<T>(j, A[i][j]));
                        }
                    }
                    }
                }

                SparseVec<T> &operator[](const int i){
                    return rows[i];
                }

                const T &operator()(const int i, const int j) const {
                    return rows[i](j);
                }
            };


            /**
             * @brief Matrix vector product
             * 
             * @tparam T The data type
             * @param A The matrix
             * @param x The vector
             * @param result The product (uninitialized vector)
             */
            template<class T>
            void multiply(GraphSparseMatrix<T> &A, SparseVec<T> &x, std::vector<T> &result){
                for(auto it = A.rows.begin(); it != A.rows.end(); it++){
                    result.push_back((*it) * x);
                }
            }

            /**
             * @brief Matrix vector product
             * 
             * @tparam T The data type
             * @param A The matrix
             * @param x The vector
             * @param result The product initialized array
             */
            template<class T>
            void multiply(GraphSparseMatrix<T> &A, SparseVec<T> &x, T *result){
                int idx = 0;
                for(auto it = A.rows.begin(); it != A.rows.end(); it++, idx++){
                    T val = (*it) * x;
                    if(!isTiny<T>(val))
                    result[idx] = (*it) * x;
                }
            }

            /**
             * @brief Matrix vector product
             * When a sparse result is expected (uses isZero for determination)
             * @tparam T The data type
             * @param A The matrix
             * @param x The vector
             * @param result The product (uninitialized vector)
             */
            template<class T>
            void multiply(GraphSparseMatrix<T> &A, SparseVec<T> &x, SparseVec<T> &result){
                int idx = 0;
                for(auto it = A.rows.begin(); it != A.rows.end(); it++, idx++){
                    T val = (*it) * x;
                    if(!isTiny<T>(val))
                    result.entries.push_back(SparseEntry<T>(idx, val));
                }
            }

            /**
             * @brief Compressed Row Storage
             * Contiguous Storage for nonuniform 2d arrays
             * Could use Sparse Entries to create a sparse matrix representation
             * @tparam T The data type
             */
            template<class T>
            class CRS {
                public:
                std::size_t n; // the total number of rows
                /**
                 * @brief The location information for the data
                 * For item i:
                 * loc[i] = pointer to the start of the data
                 * loc[i+1] = pointer to one past the end of the data
                 */
                T** loc;

                /**
                 * @brief The data 
                 */
                T* data;

                /**
                 * @brief Construct a new CRS object default constructor (no data)
                 * 
                 */
                CRS(){
                    n = 0;
                    data = NULL;
                    loc = NULL;
                }

                protected:
                CRS(std::size_t totalData, std::size_t n) : n(n), data{new T[totalData]}, loc{new T*[n + 1]} {}
                public:

                /**
                 * @brief Construct a new CRS object
                 * 
                 * @param sizes The number of entries for each row
                 * @param totalData the total number of entries
                 */
                CRS(std::vector<int> &sizes, int totalData) : 
                n(sizes.size()), data{new T[totalData]}, loc{new T*[sizes.size() + 1]}{
                    loc[0] = data;
                    int i = 1;
                    for(auto s = sizes.begin(); s != sizes.end(); s++, i++){
                    loc[i] = loc[i-1] + *s;
                    }
                }

                CRS &operator=(const CRS &other){
                    
                    if(&other != this){
                    if(other.loc == NULL){
                    if(data != NULL)
                        delete[] data;
                    if(loc != NULL)
                        delete[] data;
                    n = 0;
                    data = NULL;
                    loc = NULL;
                    return *this;
                    }
                    n = other.n;
                    delete[] data;
                    delete[] loc;
                    loc = new T*[n + 1];
                    std::size_t totalData = other.loc[n] - other.loc[0];
                    data = new T[totalData];
                    for(int i = 0; i < totalData; i++){
                        data[i] = other.data[i];
                    }
                    loc[0] = data;
                    for(int i = 1; i <= n; i++){
                        // recalculate the locations
                        std::ptrdiff_t dist = other.loc[i] - other.loc[i - 1];
                        loc[i] = loc[i - 1] + dist;
                    }
                    }
                    return *this;
                }

                ~CRS(){
                    delete[] data;
                    delete[] loc;
                }

                /**
                 * @brief Gets the number of entries in the row
                 * 
                 * @param j the row
                 * @return int the number of entries in row j
                 */
                std::ptrdiff_t rowSize(std::size_t j){
                    return (loc[j+1] - loc[j]);
                }

                /**
                 * @brief Get the start pointer for a row
                 * 
                 * @param j the row
                 * @return T* the start pointer
                 */
                T* start(std::size_t j){
                    return loc[j];
                }

                /**
                 * @brief Get the end pointer for a row
                 * 
                 * @param j the row
                 * @return T* 1 past the last entry in that row
                 */
                T* end(std::size_t j){
                    return loc[j+1];
                }

                /**
                 * @brief Get the array for a row
                 * 
                 * @param j the row
                 * @return T* the array for this row, use rowsize() for bounds
                 */
                T* operator[](std::size_t j){
                    return start(j);
                }
            };

            // forward decleration
            template<typename T>
            class SymListMat;
            template<typename T>
            class ListMat;

            /**
             * @brief Compressed row storage matrix
             * Space Usage:
             * 4 * O(n)
             * stores data, pointer array for data, columns, pointer array for columns
             * 
             * Efficiency:
             * access: O(1)
             * index: O(n)
             * 
             * @tparam T the floating point type
             */
            template<typename T>
            class CRSMat : public CRS<T> {
                public:
                std::ptrdiff_t *colidx; /// owned, indices of columms, lines up with pointer differences of loc array
                std::ptrdiff_t **loc2; /// locations of the row starts for the column indices for efficency

                CRSMat() : CRS<T>() {}

                /**
                 * @brief Construct a new CRSMat object without initializing the column indices
                 * 
                 * column indices must be filled manually, it is constructed with the correct size
                 * 
                 * @param sizes The number of entries for each row
                 * @param totalData the total number of entries
                 */
                CRSMat(std::vector<int> &sizes, int totalData)
                    : CRS<T>(sizes, totalData), colidx{new std::ptrdiff_t[sizes.size()]},
                    loc2{new std::ptrdiff_t*[sizes.size() + 1]} 
                {
                    loc2[0] = colidx;
                    int i = 1;
                    for(auto s = sizes.begin(); s != sizes.end(); s++, i++){
                        loc2[i] = loc2[i-1] + *s;
                    }
                }

                /**
                 * @brief Construct a new CRSMat object with the column indices
                 * this takes ownership of the column indices
                 * 
                 * @param sizes The number of entries for each row
                 * @param totalData the total number of entries
                 * @param colidx the column indices (takes ownership)
                 */
                CRSMat(std::vector<int> &sizes, int totalData, std::ptrdiff_t *colidx)
                : CRS<T>(sizes, totalData), colidx(colidx), loc2{new std::ptrdiff_t*[sizes.size() + 1]}
                {
                    loc2[0] = colidx;
                    int i = 1;
                    for(auto s = sizes.begin(); s != sizes.end(); s++, i++){
                        loc2[i] = loc2[i-1] + *s;
                    }
                }

                CRSMat(ListMat<T> &listmat) : CRS<T>{listmat.totalData(), listmat.size()}, 
                    colidx{new std::ptrdiff_t[listmat.totalData()]}, loc2{new std::ptrdiff_t*[listmat.size()+1]}
                {
                    CRS<T>::loc[0] = CRS<T>::data;
                    loc2[0] = colidx;
                    int i = 0;
                    for(std::ptrdiff_t row = 0; row < listmat.size(); row++){
                        for(auto it = listmat[row].begin(); it != listmat[row].end(); it++, i++){
                            CRS<T>::data[i] = it->value;
                            colidx[i] = it->idx;
                        }
                        CRS<T>::loc[row + 1] = &CRS<T>::data[i];
                        loc2[row + 1] = &colidx[i];
                    }
                }

                /**
                 * @brief Destroy the CRSMat object
                 */
                ~CRSMat(){
                    delete[] colidx;
                    delete[] loc2;
                }

                /**
                 * @brief Get this times its transpose
                 * @return a pointer to a symmetric matrix with the result
                 *         this must be deleted manually
                 */
                SymListMat<T> *AAt(){
                    SymListMat<T> *result = new SymListMat<T>(CRS<T>::n);
                    for(std::ptrdiff_t rowi = 0; rowi < CRS<T>::n; rowi++){
                        // do the i*i efficiently
                        T diagval = 0;
                        for(auto it = CRS<T>::start(rowi); it != CRS<T>::end(rowi); it++)
                            diagval += SQUARED(*it);
                        result->addtoRow(rowi, SparseEntry<T>(rowi, diagval));
                        for(std::ptrdiff_t rowj = rowi + 1; rowj < CRS<T>::n; rowj++){
                            T val = 0;
                            T *it2 = CRS<T>::start(rowj);
                            std::ptrdiff_t *it2col = loc2[rowj];
                            std::ptrdiff_t *it1col = loc2[rowi];
                            for(T *it1 = CRS<T>::start(rowi); it1 != CRS<T>::end(rowi); it1++, it1col++){
                                if(it2 == CRS<T>::end(rowj)) break;
                                while(*it2col < *it1col){
                                    it2++;
                                    it2col++;
                                }
                                if(*it2col == *it1col){
                                    val += (*it1) * (*it2);
                                    it2++;
                                    it2col++;
                                }
                            }
                            if(!isTiny<T>(val)){
                                result->addtoRow(rowi, SparseEntry<T>(rowj, val));
                            }
                        }
                    }
                    return result;
                }

            };

            /**
             * @brief A sparse vector in list form
             * 
             * @tparam T the floating point type
             */
            template<typename T>
            class ListVector {
                private:
                std::list<SparseEntry<T>> data;
                public:
                void push_back(SparseEntry<T> &value){data.push_back(value);}

                const T &operator()(std::size_t j) const {
                    auto it = data.begin();
                    while(it->idx < j){
                        it++;
                    }
                    if(it->idx == j){
                        return it->value;
                    } else {
                        return MATH::ZERO<T>;
                    }
                }

                T &operator[](int j) {
                    if(data.size() == 0){
                        data.push_back({j, MATH::ZERO<T>});
                        return (*data.begin()).value;
                    } else {
                        auto it = data.begin();
                        while(it->idx < j && it != data.end()){
                            it++;
                        }
                        if(it->idx == j){
                            return it->value;
                        } else {
                            return (*data.insert(it, {j, MATH::ZERO<T>})).value;
                        }
                    }
                }


                typename std::list<SparseEntry<T>>::iterator begin(){return data.begin();}
                typename std::list<SparseEntry<T>>::iterator end(){return data.end();}

                std::size_t size(){
                    return data.size();
                }

            };

            /**
             * @brief A symmetrix sparse matrix in list form
             * 
             * @tparam T the floating point type
             */
            template<typename T>
            class SymListMat {
                private:
                std::vector<ListVector<T>> data;
                public:

                SymListMat() : data(){}

                SymListMat(std::size_t nrows) : data(nrows, ListVector<T>()) {};

                /**
                 * @brief Add a new row to the matrix
                 */
                void addRow(){
                    data.push_back(std::list<SparseEntry<T>>());
                }

                /**
                 * @brief push an element to the back of the list for that row
                 * does not preserve ordering
                 * @param row the row
                 * @param value the value to add
                 */
                void addtoRow(std::ptrdiff_t row, SparseEntry<T> &&value){data[row].push_back(value);}

                /**
                 * @brief Safely index into the matrix but do not alter
                 * This will properly handle symmetry (j > i)
                 * The matrix must be sorted
                 * @param i the row
                 * @param j the column
                 * @return T& the value at i,j or zero otherwise
                 */
                const T &operator()(std::ptrdiff_t i, std::ptrdiff_t j) const {
                    if(i <= j){
                        return data[i](j);
                    } else {
                        return data[j](i);
                    }
                }

                /**
                 * @brief Safely index into the matrix but do not alter
                 * This will properly handle symmetry (j > i)
                 * The matrix must be sorted
                 * @param i the row
                 * @param j the column
                 * @return T& the value at i,j or zero otherwise
                 */
                const T &getValue(std::ptrdiff_t i, std::ptrdiff_t j) const {
                    if(i <= j){
                        return data[i](j);
                    } else {
                        return data[j](i);
                    }
                }

                ListVector<T> &operator[](std::ptrdiff_t i){
                    return data[i];
                }

                /**
                 * @brief Sort the entries so that they are in order of column index
                 */
                void sort(){
                    for(auto it = data.begin(); it != data.end(); it++){
                        std::sort(it->begin(), it->end());
                    }
                }
            };

            /**
             * @brief A sparse matrix in list form
             * 
             * @tparam T the floating point type
             */
            template<typename T>
            class ListMat {
                private:
                std::vector<ListVector<T>> data;
                public:

                ListMat() : data(){}

                ListMat(std::size_t nrows) : data(nrows, ListVector<T>()) {};

                /**
                 * @brief Add a new row to the matrix
                 */
                void addRow(){
                    data.push_back(std::list<SparseEntry<T>>());
                }

                /**
                 * @brief push an element to the back of the list for that row
                 * does not preserve ordering
                 * @param row the row
                 * @param value the value to add
                 */
                void addtoRow(std::ptrdiff_t row, SparseEntry<T> &value){data[row].push_back(value);}

                /**
                 * @brief Safely index into the matrix but do not alter
                 * This will properly handle symmetry (j > i)
                 * The matrix must be sorted
                 * @param i the row
                 * @param j the column
                 * @return T& the value at i,j or zero otherwise
                 */
                const T &operator()(std::ptrdiff_t i, std::ptrdiff_t j) const {
                    return data[i](j);
                }

                ListVector<T> &operator[](std::ptrdiff_t i){
                    return data[i];
                }

                /**
                 * @brief Sort the entries so that they are in order of column index
                 */
                void sort(){
                    for(auto it = data.begin(); it != data.end(); it++){
                        std::sort(it->begin(), it->end());
                    }
                }

                /**
                 * @brief The number of rows
                 * 
                 * @return std::size_t the number of rows
                 */
                std::size_t size(){
                    return data.size();
                }

                /**
                 * @brief get the total amount of data
                 * 
                 * @return std::size_t the total amount of data stored in this
                 */
                std::size_t totalData(){
                    std::size_t count = 0;
                    for(auto it = data.begin(); it!= data.end(); it++) count+= it->size();
                    return count;
                }
            };
        }
    }
}