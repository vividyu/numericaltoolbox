#include "SparseTypes.hpp"
#include "CCS.hpp"
#include "ListMat.hpp"

#ifdef USE_EIGEN
#include <Eigen/Sparse>
#endif

namespace MATH::TENSOR::SPARSE {

    /**
     * @brief A general purpose sparse matrix that adapts to need
     * loses efficiency due to checking what type it is
     * @tparam T the floating point type
     */
    template<typename T, typename IDX_TYPE>
    class SparseMatrix {
        private:
        CCS<T, IDX_TYPE> ccs;
        ListMat<T, IDX_TYPE> listmat;

        public:
        ORDERING order;
        SPARSE_TYPE sptype;

        /**
         * @brief Construct a new Sparse Matrix object
         * 
         * @param order the ordering (default: COLUMN_MAJOR)
         * @param sptype the sparse type (default LIST_STORAGE)
         */
        SparseMatrix(IDX_TYPE ncols, ORDERING order = COLUMN_MAJOR_ORDER, SPARSE_TYPE sptype = LIST_STORAGE);

        /**
         * @brief Insert into the ListMat
         * 
         * @param i the Major index
         * @param j the Minor index
         * @param val the value to insert at (i, j)
         */
        void insert(IDX_TYPE i, IDX_TYPE j, T val);

        /**
         * @brief Compress the list matrix and switch to ccs
         * 
         */
        void compress();

        // -------------------
        // - Eigen Interface -
        // -------------------
        #ifdef USE_EIGEN
        void toEigenMat(Eigen::SparseMatrix<T, 0, IDX_TYPE> &eigenmat);
        #endif

    };

}