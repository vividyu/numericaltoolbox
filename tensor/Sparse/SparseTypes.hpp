/**
 * @file SparseTypes.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Sparse Matrix Types
 * @version 0.1
 * @date 2021-07-15
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#pragma once
#include "matrix.hpp"
#include "TensorEnums.h"

namespace MATH::TENSOR{

    /**
     * @brief namespace for sparse tensor definitions and operations
     * 
     * Sparse means that the storage pattern does not represent all of the numerically zero entries
     */
    namespace SPARSE{

        /**
         * @brief A sparse Matrix entry
         * @tparam T the type of data
         * @tparam IDX_TYPE the type for indices
         */
        template<class T, typename IDX_TYPE=int>
        class SparseEntry {
        public:
            // Only Allow Numeric Types
            static_assert(std::is_arithmetic<T>::value, "Must be an Arithmetic type");

            IDX_TYPE idx; // the indices of the value
            T value; // the value

            /**
             * @brief Construct a new Sparse Entry object
             * 
             * @param idx_ the index
             * @param value_ the value
             */
            SparseEntry(IDX_TYPE idx_, T value_): idx(idx_), value(value_){}

            /**
             * @brief Construct a new Sparse Entry object
             * 
             * @param other Entry to copy from
             */
            SparseEntry(const SparseEntry<T> &other){
                idx = other.idx;
                value = other.value;
            }

            bool operator < (const SparseEntry<T> & other) const{
                return other.idx < idx;
            }

            bool operator > (const SparseEntry<T> & other) const {
                return other.idx > idx;
            }
            // INTEGER COMPARISONS
            bool operator < (const IDX_TYPE & other) const{
                return other < idx;
            }
            bool operator > (const IDX_TYPE & other) const{
                return other > idx;
            }
        };

        template<class T, typename IDX_TYPE=int>
        bool operator <(const IDX_TYPE &lhs, const SparseEntry<T> &rhs){
            return lhs < rhs.idx;
        }

        /**
         * @brief The way the matrix is stored in memory
         * 
         */
        enum SPARSE_TYPE {
            /// compressed array storage stores the data as a contiguous array
            /// and then stores element column numbers and indices for row starts 
            ///(or row numbers and indices for column starts in the case of column major ordering)
            COMPRESSED_ARRAY_STORAGE,

            /// ListMat
            LIST_STORAGE

        };

        /**
         * @brief A Sparse Matrix Interface
         * Common implementations that sparse matrix types should have
         * 
         * @tparam T the floating point type
         */
        template<typename T>
        class SparseMatrixInterface {

            // ---------------------
            // - Optimized methods -
            // ---------------------

            /**
             * @brief Sparse Matrix times dense vector multiply
             * assumes both vectors allocated with the same number of rows
             * Does not zero initialize result
             * @param vec The vector to be multiplied
             * @param result the result of the inner product of this matrix with vec
             */
            virtual
            void smxdv(T *vec, T *result) = 0;

        };
    }
}