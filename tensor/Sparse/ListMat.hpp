
#pragma once
#include "SparseTypes.hpp"
#include <vector>
#include <algorithm>
#include <list>
namespace MATH::TENSOR::SPARSE {
        /**
     * @brief A sparse vector in list form
     * 
     * @tparam T the floating point type
     * @tparam IDX_T the index type
     */
    template<typename T, typename IDX_T>
    class ListVector {
        private:
        std::list<SparseEntry<T, IDX_T>> data;
        public:

        /**
         * @brief Add an entry to the end of this vector
         * 
         * @param value the entry to add
         */
        void push_back(SparseEntry<T, IDX_T> &value){data.push_back(value);}

        /**
         * @brief Index into the vector
         * 
         * @param j the index
         * @return const T& a const reference to the value at that index (zero if not in sparsity pattern)
         */
        const T &operator()(std::size_t j) const;

        /**
         * @brief Index into the vector
         * 
         * @param j the index
         * @return T& a reference to the value at that index
         * If the entry doesn't exist, generates a new entry at that index with the value 0
         */
        T &operator[](int j);

        /**
         * @brief Get an iterator to the start of this vector
         * 
         * @return std::list<SparseEntry<T, IDX_T>>::iterator the first element of the vector
         */
        typename std::list<SparseEntry<T, IDX_T>>::iterator begin(){ return data.begin(); }

        /**
         * @brief get an iterator pointing one past the end of this vector
         * 
         * @return std::list<SparseEntry<T, IDX_T>>::iterator one past the end (list<T>.end)
         */
        typename std::list<SparseEntry<T, IDX_T>>::iterator end(){ return data.end(); }

        /**
         * @brief Get the number of entries in this vector
         * 
         * @return std::size_t 
         */
        std::size_t size(){ return data.size(); }

    };

    /**
     * @brief A symmetrix sparse matrix in list form
     * 
     * @tparam T the floating point type
     * @tparam IDX_T the index type
     * @tparam ORDERING the ordering of the matrix
     */
    template<typename T, typename IDX_T = int, ORDERING ordering = ORDERING::ROW_MAJOR_ORDER>
    class SymListMat {
        private:
        std::vector<ListVector<T, IDX_T>> data;
        public:

        SymListMat() : data(){}

        SymListMat(std::size_t nrows) : data(nrows, ListVector<T, IDX_T>()) {};

        /**
         * @brief Add a new vector to the matrix
         */
        void addVector(){
            data.push_back(std::list<SparseEntry<T, IDX_T>>());
        }

        /**
         * @brief push an element to the back of the list for that row/column
         * does not preserve ordering
         * @param idx the row for row major ordering or column for column major ordering
         * @param value the value to add
         */
        void addtoVec(IDX_T idx, SparseEntry<T, IDX_T> &&value){data[idx].push_back(value);}

        /**
         * @brief Safely index into the matrix but do not alter
         * This will properly handle symmetry (j > i)
         * The matrix must be sorted
         * @param i the vector index (row for row major order, column for column major order)
         * @param j the index inside the vector (column for row major order)
         * @return T& the value at i,j or zero otherwise
         */
        inline const T &getValue(IDX_T i, IDX_T j) const {
            if(i <= j){
                return data[i](j);
            } else {
                return data[j](i);
            }
        }

        /**
         * @brief Safely index into the matrix but do not alter
         * This will properly handle symmetry (j > i)
         * The matrix must be sorted
         * @param i the row
         * @param j the column
         * @return T& the value at i,j or zero otherwise
         */
        const T &operator()(IDX_T i, IDX_T j) const {
            return getValue(i, j);
        }

        /**
         * @brief get the ith vector
         * 
         * @param i the index
         * @return ListVector<T, IDX_T>& the vector
         */
        ListVector<T, IDX_T> &operator[](IDX_T i){
            return data[i];
        }

        /**
         * @brief Sort the entries so that they are in order of column index
         */
        void sort(){
            for(auto it = data.begin(); it != data.end(); it++){
                std::sort(it->begin(), it->end());
            }
        }
    };

    namespace LISTMATUTILS{
        template<typename T, typename IDX_T = int>
        class IndexProxy;

        template<typename T, typename IDX_T, ORDERING ordering>
        struct rowtype{ typedef int type;}; // TODO:: come up with a better default

        template<typename T, typename IDX_T>
        struct rowtype<T, IDX_T, ORDERING::ROW_MAJOR_ORDER>{
            typedef ListVector<T, IDX_T> type;
        };

        template<typename T, typename IDX_T>
        struct rowtype<T, IDX_T, ORDERING::COLUMN_MAJOR_ORDER>{
            typedef IndexProxy<T, IDX_T> type;
        };
    }

    

    /**
     * @brief A sparse matrix in list form
     * 
     * @tparam T the floating point type
     * @tparam IDX_T the index type
     * @tparam ordering the ordering of the matrix (defines major and minor dimensions)
     */
    template<typename T, typename IDX_T = int, ORDERING ordering = ORDERING::ROW_MAJOR_ORDER>
    class ListMat {
        private:
        std::vector<ListVector<T, IDX_T>> data;
        public:

        ListMat() : data(){}

        ListMat(std::size_t nrows) : data(nrows, ListVector<T, IDX_T>()) {};

        /**
         * @brief Add a new Vector to the major dimension of the matrix
         */
        void addVector(){
            data.push_back(std::list<SparseEntry<T, IDX_T>>());
        }

        /**
         * @brief Get the idxth major dimension vector from the list
         * 
         * @param idx the major dimension index
         * @return ListVector<T, IDX_T>& the vector at major dimension idx
         */
        ListVector<T, IDX_T> &getVector(IDX_T idx){
            return data[idx];
        }

        /**
         * @brief push an element to the back of the list for that vector
         * does not preserve ordering
         * @param maj_idx the major dimension index (row for row major order)
         * @param value the value to add
         */
        void addtoVector(IDX_T maj_idx, SparseEntry<T, IDX_T> &value){data[maj_idx].push_back(value);}

        /**
         * @brief Safely index into the matrix but do not alter
         * The matrix must be sorted
         * @param i the row
         * @param j the column
         * @return T& the value at i,j or zero otherwise
         */
        const T &operator()(IDX_T i, IDX_T j) const {
            if constexpr (ordering == ORDERING::ROW_MAJOR_ORDER)
                return data[i](j);
            else
                return data[j](i);
        }

        /**
         * @brief Index into the matrix
         * 
         * @param i the row
         * @return LISTMATUTILS::rowtype<T, IDX_T, ordering>::type& the row vector or a proxy object that allows indexing into the column
         * for row major order and column major order respectively
         */
        typename LISTMATUTILS::rowtype<T, IDX_T, ordering>::type &operator[](IDX_T i){
            if constexpr (ordering == ORDERING::ROW_MAJOR_ORDER)
                return data[i];
            else
                return LISTMATUTILS::IndexProxy<T, IDX_T>(this, i);
        }

        /**
         * @brief Sort the entries so that they are in order of column index
         */
        void sort(){
            for(auto it = data.begin(); it != data.end(); it++){
                std::sort(it->begin(), it->end());
            }
        }

        /**
         * @brief The size of the major dimension
         * number of rows for row major order
         * number of columns for column major order
         * 
         * @return std::size_t the size of the major dimension
         */
        IDX_T size(){
            return data.size();
        }

        /**
         * @brief get the total amount of data
         * 
         * @return std::size_t the total amount of data stored in this
         */
        std::size_t totalData(){
            std::size_t count = 0;
            for(auto it = data.begin(); it!= data.end(); it++) count+= it->size();
            return count;
        }
    };

    namespace LSITMATUTILS{
        /**
         * @brief Proxy for indexing into Column major ListMat
         * 
         * @tparam T 
         * @tparam IDX_T 
         */
        template<typename T, typename IDX_T = int>
        class IndexProxy {
            public:
            ListMat<T, IDX_T, ORDERING::COLUMN_MAJOR_ORDER> *listmat; // the matrix to index into
            IDX_T i; // the i index

            IndexProxy(ListMat<T, IDX_T, ORDERING::COLUMN_MAJOR_ORDER> *listmat, IDX_T i) : 
            listmat(listmat), i(i) {}

            T &operator[](IDX_T j){
                return listmat.data[j][i];
            }
        };
    }

    // compiler type declerations
    template class ListVector<float, int>;
    template class ListVector<double, int>;
    template class ListVector<long double, int>;

    template class ListVector<float, std::ptrdiff_t>;
    template class ListVector<double, std::ptrdiff_t>;
    template class ListVector<long double, std::ptrdiff_t>;
}