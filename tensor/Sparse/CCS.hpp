/**
 * @file CCS.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Compressed Column Storage
 * @version 0.1
 * @date 2021-07-15
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#pragma once
#include "SparseTypes.hpp"
#include "ListMat.hpp"

#ifdef USE_EIGEN
#include <Eigen/Sparse>
#endif

namespace MATH::TENSOR::SPARSE{

    /**
     * @brief Compressed column Storage
     * 
     * @tparam T the floating point type
     * @tparam IDX_TYPE the integral type for indices
     */
    template<typename T, typename IDX_TYPE=std::ptrdiff_t>
    class CCS : public SparseMatrixInterface<T>{
        private:
        bool dataowned = true;

        public:
        IDX_TYPE ncols;        /// the number of columns
        IDX_TYPE ndata;        /// the total number of nonzero data entries
        T *data;               /// the data
        IDX_TYPE *rowidxs;     /// the matrix indices for the row of each data entry
        IDX_TYPE *colstart;    /// the offset for each column (first column is 0) and one off the end


        /**
         * @brief Construct a new CCS object from a dense matrix
         * eliminates all entries less than tol
         * @param densematrix the dense matrix
         * @param tol the tolerance for numerically zero entries
         */
        CCS(Matrix<T> &densematrix, T tol);

        /**
         * @brief Construct a new CCS object from a list matrix
         * 
         * @param listmat the list matrix to copy
         */
        CCS(ListMat<T, IDX_TYPE, ORDERING::COLUMN_MAJOR_ORDER> &listmat) :
        ncols(listmat.size()), ndata(listmat.totalData()), data(new T[ndata]), 
        rowidxs(new IDX_TYPE[ndata]), colstart(new IDX_TYPE[ncols + 1])
        {
            IDX_TYPE pos = 0;
            for(int j = 0; j < ncols; j++){
                colstart[j] = pos;
                for(auto it = listmat.getVector(j).begin(); it != listmat.getVector(j).end(); it++){
                    data[pos] = it->value;
                    rowidxs[pos] = it->idx;
                    pos++;
                }
            }
            colstart[ncols] = ndata;
        }

        ~CCS();

        // -----------
        // - UTILITY -
        // -----------
        /**
         * @brief Resize the data arrays to be able to hold more entries
         * 
         * @param newsize the new size (truncates if it is smaller than the current size)
         */
        void resize(IDX_TYPE newsize);

        // ---------------------
        // - Optimized methods -
        // ---------------------

        void smxdv(T *vec, T *result);

        // -------------------
        // - Eigen Interface -
        // -------------------
        #ifdef USE_EIGEN
        /**
         * @brief Construct a new CCS object From an Eigen matrix
         * 
         * @tparam copy whether to copy the matrix or not
         * @param eigenmat the eigen matrix, must be in compressed format
         */
        template<bool copy>
        CCS(Eigen::SparseMatrix<T, 0, IDX_TYPE> &eigenmat);

        /**
         * @brief Copies the contents of this matrix to an Eigen matrix
         * and compresses afterwards
         * @param eigenmat an Empty Eigen Matrix
         */
        void toEigenMat(Eigen::SparseMatrix<T, 0, IDX_TYPE> &eigenmat);
        #endif
    };

    // forward declerations
    template class CCS<float, int>;
    template class CCS<double, int>;
    template class CCS<long double, int>;
    template class CCS<float, std::ptrdiff_t>;
    template class CCS<double, std::ptrdiff_t>;
    template class CCS<long double, std::ptrdiff_t>;
}