/**
 * @file CRS.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Compressed Row Storage
 * @version 0.1
 * @date 2021-08-24
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#pragma once
#include "SparseTypes.hpp"

namespace MATH::TENSOR::SPARSE {

    /**
     * @brief A compressed container
     * contiguous storage for nonuniform 2d arrays
     * could use sparse entries to create a sparse matrix representation
     * 
     * @tparam T the data type stored in the container
     */
    template<class T>
    class CRSContainer {
        public:
        std::size_t n; // the total number of rows
        /**
         * @brief The location information for the data
         * For item i:
         * loc[i] = pointer to the start of the data
         * loc[i+1] = pointer to one past the end of the data
         */
        T** loc;

        /**
         * @brief The data 
         */
        T* data;

        /**
         * @brief Construct a new CRS object default constructor (no data)
         * 
         */
        CRSContainer(){
            n = 0;
            data = NULL;
            loc = NULL;
        }

        protected:
        CRSContainer(std::size_t totalData, std::size_t n) : n(n), data{new T[totalData]}, loc{new T*[n + 1]} {}
        public:

        /**
         * @brief Construct a new CRS object
         * 
         * @param sizes The number of entries for each row
         * @param totalData the total number of entries
         */
        CRSContainer(std::vector<int> &sizes, int totalData) : 
        n(sizes.size()), data{new T[totalData]}, loc{new T*[sizes.size() + 1]}{
            loc[0] = data;
            int i = 1;
            for(auto s = sizes.begin(); s != sizes.end(); s++, i++){
            loc[i] = loc[i-1] + *s;
            }
        }

        CRSContainer &operator=(const CRSContainer<T> &other){
            
            if(&other != this){
            if(other.loc == NULL){
            if(data != NULL)
                delete[] data;
            if(loc != NULL)
                delete[] data;
            n = 0;
            data = NULL;
            loc = NULL;
            return *this;
            }
            n = other.n;
            delete[] data;
            delete[] loc;
            loc = new T*[n + 1];
            std::size_t totalData = other.loc[n] - other.loc[0];
            data = new T[totalData];
            for(int i = 0; i < totalData; i++){
                data[i] = other.data[i];
            }
            loc[0] = data;
            for(int i = 1; i <= n; i++){
                // recalculate the locations
                std::ptrdiff_t dist = other.loc[i] - other.loc[i - 1];
                loc[i] = loc[i - 1] + dist;
            }
            }
            return *this;
        }

        ~CRSContainer(){
            delete[] data;
            delete[] loc;
        }

        /**
         * @brief Gets the number of entries in the row
         * 
         * @param j the row
         * @return int the number of entries in row j
         */
        std::ptrdiff_t rowSize(std::size_t j){
            return (loc[j+1] - loc[j]);
        }

        /**
         * @brief Get the start pointer for a row
         * 
         * @param j the row
         * @return T* the start pointer
         */
        T* start(std::size_t j){
            return loc[j];
        }

        /**
         * @brief Get the end pointer for a row
         * 
         * @param j the row
         * @return T* 1 past the last entry in that row
         */
        T* end(std::size_t j){
            return loc[j+1];
        }

        /**
         * @brief Get the array for a row
         * 
         * @param j the row
         * @return T* the array for this row, use rowsize() for bounds
         */
        T* operator[](std::size_t j){
            return start(j);
        }
    };
}