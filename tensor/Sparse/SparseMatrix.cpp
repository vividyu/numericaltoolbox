#include "SparseMatrix.hpp"

namespace MATH::TENSOR::SPARSE{

    template<typename T, typename IDX_TYPE>
    SparseMatrix<T, IDX_TYPE>::SparseMatrix(IDX_TYPE ncols, ORDERING order, SPARSE_TYPE sptype) :
    order(order), sptype(sptype), ccs{}, listmat{ncols}
    {}

    template<typename T, typename IDX_TYPE>
    void SparseMatrix<T, IDX_TYPE>::insert(IDX_TYPE i, IDX_TYPE j, T val){
        if(order == ORDERING::ROW_MAJOR_ORDER)
            listmat.addVector(i, SparseEntry<T, IDX_TYPE>(j, val));
        else
            listmat.addtoVector(j, SparseEntry<T, IDX_TYPE>(i, val));
    }

    template<typename T, typename IDX_TYPE>
    void SparseMatrix<T, IDX_TYPE>::compress(){
        if(order == ORDERING::COLUMN_MAJOR_ORDER){
            sptype = SPARSE_TYPE::COMPRESSED_ARRAY_STORAGE;
            ccs = CCS<T, IDX_TYPE>(listmat);
            listmat = ListMat<T, IDX_TYPE>(); // clear list matrix
        } else {
            //TODO:: finish
        }
    }

    #ifdef USE_EIGEN
    template<typename T, typename IDX_TYPE>
    void SparseMatrix<T, IDX_TYPE>::toEigenMat(Eigen::SparseMatrix<T, 0, IDX_TYPE> &eigenmat){
        if(order == ORDERING::ROW_MAJOR_ORDER){
            for(int i = 0; i < listmat.size(); i++){
                for(auto it = listmat.getVector(i).begin(); it != listmat.getVector(i).end(); it++){
                    eigenmat.insert(i, it->idx) = it->value;
                }
            }
        } else {
            for(int j = 0; j < listmat.size(); j++){
                for(auto it = listmat.getVector(j).begin(); it != listmat.getVector(j).end(); it++){
                    eigenmat.insert(it->idx, j) = it->value;
                }
            }
        }
    }
    #endif
}