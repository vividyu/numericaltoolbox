
#include "sp_optimization.hpp"
#include "orthogonal.hpp"
#include <cmath>
#include <cstring>
#include <iostream>
#include <iomanip>

namespace MATH::TENSOR::SPARSE {

    /**
     * @brief Get the idx for a upper triangular Matrix
     * 
     * @param n the dimension size
     * @param i the row
     * @param j the column
     * @return SZ_INT uses sum of integers formula to find the index 
     */
    template<typename SZ_INT, typename IDX_INT>
    inline SZ_INT triRidx(SZ_INT n, SZ_INT i, SZ_INT j){
        return  (i) * (2 * n - i - 1) / 2 + j;
    }

    // template<typename T>
    // void printR(SZ_INT n, T *R){
    //     int idx = 0;
    //     for(int i = 0; i < n; i++){
    //         for(int j = 0; j < i; j++){
    //             std::cout << std::setw(10) << std::fixed << 0.0 << " ";
    //         }
    //         for(int j = i; j < n; j++){
    //             std::cout << std::setw(10) << std::fixed << R[idx++] << " ";
    //         }
    //         std::cout << "\n";
    //     }
    // }

    /**
     * @brief Compute the Levenberg Marquardt step from a QR factorization of the Jacobian
     * For column major compressed column storage (CCS)
     * 
     * @tparam T the floating point type
     * @param m the number of rows in the matrix
     * @param n the number of columns in the matrix
     * @param R the right trapezoidal part of the QR decomposition of the Jacobian in CCS
     * @param Ridx the row indices for the entries in R
     * @param Rcolstart the pointers to the start of each column in R
     * @param D The diagonal of the jacobian (this is multiplied by the sqrt(lambda) in the ellipsoidal LM algorithm)
     *          Use the Identity for classical LM
     * @param lambda the trust region radius
     * @param Qtf the result of Q transpose times f compute this before using this method
     * @param p [out] the step
     * @param sdiag [out] the diagonal of S from p^T* (AA^T + DD) * p = S^T S
     */
    template<typename T, typename SZ_INT, typename IDX_INT>
    inline void colmaj_lm_rdiag_step(
        SZ_INT m,
        SZ_INT n,
        T *R,
        IDX_INT *Ridx,
        IDX_INT *Rcolstart,
        const T *D,
        T lambda,
        const T* Qtf,
        T *p,
        T *sdiag
    ){
        // givens rotation variables
        T c, s, r;
        T temp;
        T sqrtlambda = std::sqrt(lambda);

        // ----------------------------------------
        // - Givens Rotations of augmented matrix -
        // ----------------------------------------
        // TODO: CSR for r because sparsity is retained during givens
        // work array copy over sparse R  TODO: determine whether storing square matrix is noticably faster vs triangular storage scheme
        T *Rwork = new T[n * (n + 1) / 2](); // dense triangular matrix (row-wise)
        for(SZ_INT j = 0; j < n; j++){
            for(IDX_INT idx = Rcolstart[j]; idx != Rcolstart[j + 1]; idx++){
                SZ_INT i = Ridx[idx];
                Rwork[triRidx<SZ_INT, IDX_INT>(n, i, j)] = R[idx];
            }
        }
        T *rhswork = new T[n]; // work array for QTf
        memcpy(rhswork, Qtf, n * sizeof(T));
        
        T *wrow = new T[n]; // work array for the row
        // main loop O(n^3) ...
        for(SZ_INT i = 0; i < n; i++){

            T qtf_npi = 0; //Q^T f index n + i
            // loop over the rows from the bottom of the matrix
            wrow[i] = sqrtlambda * D[i];
            for(int l = i + 1; l < n; l++) wrow[l] = 0;
            for(SZ_INT j = i; j < n; j++){
                // loop over the columns from the diagonal to the end
                

                // compute the givens rotation to eliminate the entry
                givens<T>(Rwork[triRidx<SZ_INT, IDX_INT>(n, j, j)], wrow[j], &c, &s, &r);
                // apply transformation to the diagonal of R
                Rwork[triRidx<SZ_INT, IDX_INT>(n, j, j)] = c * Rwork[triRidx<SZ_INT, IDX_INT>(n, j, j)] + s * wrow[j];
                // wrow[j] => would compute to 0

                // TODO: accumulate into qtf
                temp = c * rhswork[j] + s * qtf_npi;
                qtf_npi = -s * rhswork[j] + c * qtf_npi;
                rhswork[j] = temp;

                // accumulate effects of transformation accross the row
                for(SZ_INT l = j + 1; l < n; l++){
                    // rotate the column l by the givens rotation
                    // |  c   s  | | R[j][l] |    
                    // |   ...   | |   ...   | =  new vector
                    // | -s   c  | | wrow[l] |    
                    temp = c * Rwork[triRidx<SZ_INT, IDX_INT>(n, j, l)] + s * wrow[l];
                    wrow[l] = -s * Rwork[triRidx<SZ_INT, IDX_INT>(n, j, l)] + c * wrow[l];
                    Rwork[triRidx<SZ_INT, IDX_INT>(n, j, l)] = temp;
                }
            } // end loop over columns
            sdiag[i] = Rwork[triRidx<SZ_INT, SZ_INT>(n, i, i)];
        } // end loop over rows

        T sum;
        // solve p = R^-1 u
        for(int i = n - 1; i >= 0; i--){
            sum = 0;
            for(int j = i + 1; j < n; j++){
                sum += p[j] * Rwork[triRidx<SZ_INT, IDX_INT>(n, i, j)];
            }
            p[i] = (rhswork[i] - sum) / Rwork[triRidx<SZ_INT, IDX_INT>(n, i, i)];
        }

        // cleanup
        delete[] Rwork;
        delete[] rhswork;
        
    }

    // compiler declarations
    template void colmaj_lm_rdiag_step<double, long, long>(long m, long n, double *R, long *Ridx,
        long *Rcolstart, const double *D, double lambda, const double* Qtf, double *p, double *sdiag);
    template void colmaj_lm_rdiag_step<double, std::size_t, std::size_t>(std::size_t m, std::size_t n, double *R, std::size_t *Ridx,
        std::size_t *Rcolstart, const double *D, double lambda, const double* Qtf, double *p, double *sdiag);

}