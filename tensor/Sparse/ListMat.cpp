#include "ListMat.hpp"

namespace MATH::TENSOR::SPARSE {
    template<typename T, typename IDX_T>
    const T &ListVector<T, IDX_T>::operator()(std::size_t j) const {
        auto it = data.begin();
        while(it->idx < j){
            it++;
        }
        if(it->idx == j){
            return it->value;
        } else {
            return MATH::ZERO<T>;
        }
    }

    template<typename T, typename IDX_T>
    T &ListVector<T, IDX_T>::operator[](int j){
    if(data.size() == 0){
            data.push_back({j, MATH::ZERO<T>});
            return (*data.begin()).value;
        } else {
            auto it = data.begin();
            while(it->idx < j && it != data.end()){
                it++;
            }
            if(it->idx == j){
                return it->value;
            } else {
                return (*data.insert(it, {j, MATH::ZERO<T>})).value;
            }
        }
    }

}