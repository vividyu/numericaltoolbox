/**
 * @file CCS.cpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Implementation for Compressed Column Storage
 * @version 0.1
 * @date 2021-07-15
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "CCS.hpp"
#include <cmath>
#include "memory.hpp"
#include <algorithm>
#include <cstring>

using namespace MEMORY;

namespace MATH::TENSOR::SPARSE {

    template<typename T, typename IDX_TYPE>
    inline CCS<T, IDX_TYPE>::CCS(Matrix<T> &densematrix, T tol): ncols(densematrix.n){
        IDX_TYPE count = 0;
        // count the number of nonzero entries
        for(T *it = densematrix.data; it != densematrix.data + densematrix.m * densematrix.n; it++){
            if(std::abs(*it) > tol) count++;
        }
        // initialize
        ndata = count;
        data = tmalloc<T>(count);
        rowidxs = tmalloc<IDX_TYPE>(count);
        colstart = tmalloc<IDX_TYPE>(densematrix.n + 1);

        IDX_TYPE idx = 0;
        T *dataptr = data;
        IDX_TYPE *rowidxsptr = rowidxs;
        for(IDX_TYPE j = 0; j < densematrix.n; j++){
            colstart[j] = idx;
            for(IDX_TYPE i = 0; i < densematrix.m; i++){
                if(std::abs(densematrix[i][j]) > tol){
                    *dataptr = densematrix[i][j];
                    *rowidxsptr = i;
                    dataptr++;
                    rowidxsptr++;
                    idx++;
                }
            }
        }
        colstart[densematrix.n] = count;
    }

    template<typename T, typename IDX_TYPE>
    inline void CCS<T, IDX_TYPE>::resize(IDX_TYPE newsize){
        bool ok1, ok2, ok3;
        trealloc<T>(data, newsize, &ok1);
        trealloc<IDX_TYPE>(rowidxs, newsize, &ok2);
        if(ok1 && ok2){
            ndata = newsize;
        } else if(newsize < ndata){
            ndata = newsize;
        }
    }

    template<typename T, typename IDX_TYPE>
    inline CCS<T, IDX_TYPE>::~CCS(){
        if(dataowned){
            tfree(data);
            tfree(rowidxs);
            tfree(colstart);
        }
    }

    template<typename T, typename IDX_TYPE>
    inline void CCS<T, IDX_TYPE>::smxdv(T *vec, T *result){
        T *datait = data;
        for(IDX_TYPE j = 0; j < ncols; j++){
            for(IDX_TYPE *rowidx = rowidxs + colstart[j]; rowidx != rowidxs + colstart[j+1]; rowidx++)
                result[*rowidx] += vec[j] * *datait++;
        }
    }

    // -------------------
    // - Eigen Interface -
    // -------------------
    #ifdef USE_EIGEN
    template<typename T, typename IDX_TYPE>
    template<bool copy>
    CCS<T, IDX_TYPE>::CCS(Eigen::SparseMatrix<T, 0, IDX_TYPE> &eigenmat) : 
    ncols(eigenmat.outerSize()), ndata(eigenmat.nonZeros())
    {
        if constexpr (copy){
            dataowned = false;
            data = new T[ndata];
            rowidxs = new IDX_TYPE[ndata];
            colstart = new IDX_TYPE[ncols + 1];
            memcpy(data, eigenmat.valuePtr(), ndata * sizeof(T));
            memcpy(colstart, eigenmat.innerIndexPtr(), ndata * sizeof(IDX_TYPE));
            memcpy(rowidxs, eigenmat.outerIndexPtr(), (ncols + 1) * sizeof(IDX_TYPE));
        } else {
            data = eigenmat.valuePtr;
            colstart = eigenmat.innerIndexPtr;
            rowidxs = eigenmat.outerIndexPtr;
        }
    }

    template<typename T, typename IDX_TYPE>
    void CCS<T, IDX_TYPE>::toEigenMat(Eigen::SparseMatrix<T, 0, IDX_TYPE> &eigenmat){
        for(int c = 0; c < ncols; c++){
            for(IDX_TYPE ptr = colstart[c]; ptr < colstart[c + 1]; ptr++){
                eigenmat.insert(rowidxs[ptr], c) = data[ptr];
            }
        }
    }
    #endif

}