/**
 * @file orthogonal.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief 
 * @version 0.1
 * @date 2021-07-23
 * 
 * Utilities and Orthogonal operations
 * 
 * E.g givens rotations and householder reflections
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#pragma once
#include "TensorEnums.h"

namespace MATH::TENSOR{

    /**
     * @brief Compute a Givens rotation for a vector [ f ; g ]
     * 
     * A givens rotation is the matrix:
     * G = 
     * |  c  s |
     * | -s  c |
     * 
     * That rotates the vector s.t G * [f ; g] = [r; 0]
     * 
     * This is an orthogonal rotation
     * @tparam T the floating point type
     * @param f the first component of the vector
     * @param g the second component of the vector
     * @param c [out] the cosine of the rotation (c in the matrix)
     * @param s [out] the sine of the rotation (s in the matrix)
     * @param r [out] the r which is the top componenet of the resulting vector
     * 
     * Continuous real plane rotation based on Edward Anderson
     * http://www.netlib.org/lapack/lawnspdf/lawn150.pdf
     */
    template<typename T>
    void givens(T f, T g, T *c, T *s, T *r);

}