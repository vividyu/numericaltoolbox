/**
 * @file RandomizedSVD.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Randomized Singular Value Decomposition
 * @version 0.1
 * @date 2021-11-24
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#pragma once
#include "TensorEnums.h"
#include "tensorUtils.hpp"
#include <random>
#include "matrix.hpp"
#include <thread>
#include <utility>
#include <vector>
#include <iostream>
#include <barrier>

namespace MATH::TENSOR{
    /**
     * @brief Namespace for Randomized Methods
     * 
     */
    namespace RANDOMIZED{

        /**
         * @brief Generate a random vector with a normal distribution
         * 
         * @tparam IDX the index type
         * @tparam V the vector type
         * @param v the vector to fill (must have correct size)
         */
        template<typename IDX, typename T, typename V>
        inline void randn(V &v, std::default_random_engine &re, std::normal_distribution<T> &dist) requires VectorConcept<V>{   
            for(IDX i = 0; i < v.size(); i++) v[i] = dist(re);
        }

        /**
         * @brief For evenly distributed processors find the next index for that processor
         * greater than i
         * 
         * @tparam IDX the index type
         * @param p the number of processors
         * @param id the processor id
         * @param i the i bound
         * @return IDX the next index
         */
        template<typename IDX>
        inline IDX leastEntryAfteri(IDX p, IDX id, IDX i){
            IDX base = p * (i / p);
            IDX dist = i - base;
            if(id > base){
                return base + id;
            } else {
                return base + p + id;
            }
        }

        template<typename T, typename IDX, class V, class L>
        void randSVD(L &A, IDX k, IDX numthreads) requires LinearOperator2<L, V>{
            Matrix<T, IDX, COLUMN_MAJOR_ORDER> B(A.getM(), k);
            std::vector<std::thread> threads;

            // barrier setup
            auto sync_complete = []() noexcept {
                //std::cout << "sync\n";
            };
            auto sync_point = std::make_shared<std::barrier<std::function<void()> > >(numthreads, sync_complete);

            // worker function definition
            auto threadfunc = [sync_point](L &A, IDX k, Matrix<T, IDX, COLUMN_MAJOR_ORDER> &B, IDX id, IDX numthreads) {
                std::default_random_engine re{};
                re.seed(std::random_device{}());
                std::normal_distribution<T> dist{0, 1};
                // get random vector and fill B with normalized vectors
                for(IDX col = id; col < k; col += numthreads){
                    V w(A.getM());
                    randn<IDX, T, V>(w, re, dist);
                    V y(A.getM());
                    A.mult(w, y);
                    T ynorm = y.norm();
                    for(int i = 0; i < B.getM(); i++){
                        B[i][col] = y[i] / ynorm;
                    }
                }

                sync_point->arrive_and_wait();
                // Orthogonalization
                constexpr int n_orthogs = 2;
                for(int reortho = 0; reortho < n_orthogs; reortho++){
                    for(IDX j = 0; j < k; j++){
                        IDX colcount = 0;
                        for(IDX col = leastEntryAfteri<IDX>(numthreads, id, j); col < k; col += numthreads){
                            // Gram-schmidt
                            T projscale = innerprod<T, IDX>(B.ptr[j], B.ptr[col], B.getM()) /
                                        innerprod<T, IDX>(B.ptr[j], B.ptr[j], B.getM()); 
                            for(IDX i = 0; i < B.getM(); i++){
                                B[i][col] -= projscale * B[i][j];
                            }
                        }
                        sync_point->arrive_and_wait();
                    }
                }

                // Normalization
                constexpr T epsilon = 1e-20;
                for(IDX col = id; col < k; col += numthreads){
                    T colnorm = norm<T, IDX, 2>(B.ptr[col], B.getM());
                    if(colnorm > epsilon){
                        for(IDX i = 0; i < B.getM(); i++){
                            B[i][col] /= colnorm;
                        }
                    } else { // TODO: pull out into reverse loop and resize matrix first because last columsn will be the zero ones
                            for(IDX i = 0; i < B.getM(); i++){
                            B[i][col] = 0; 
                        }
                    }
                    std::cout << norm<T, IDX, 2>(B.ptr[col], B.getM());
                }

            };

            // thread creation
            for(IDX t = 0; t < numthreads; t++) {
                threads.push_back(
                    std::thread(
                        threadfunc,
                        std::ref(A),
                        k,
                        std::ref(B),
                        t,
                        numthreads
                    )
                );
            }
            // get random vectors w
            // y_i - Aw_i

            for(auto &th : threads) th.join(); // join all the threads
            std::cout << "\n";
            //B.prettyPrint(std::cout);
            // Q factor of Y
        }

    }
}