/**
 * @file directSolvers.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Direct Linear Solvers and Factorizations
 * @version 0.1
 * @date 2021-09-11
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <Numtool/matrix/matrix.hpp>
#include <concepts>

namespace MATH::TENSOR{

    /**
     * @brief Get the Pivot Permutation object by partial pivoting
     * 
     * @tparam T the floating point type
     * @tparam IDX the index type
     * @param mat the matrix
     * @param pi the pivoting permutation
     */
    template<typename T, typename IDX=std::size_t>
    PermutationMatrix<IDX> getPivotPermutation(Matrix<T, IDX> &mat) {
        T max;
        IDX maxidx;
        PermutationMatrix<IDX> pi{mat.m};
        for(int j = 0; j < mat.n; j++){
            max = std::abs(mat[j][j]);
            maxidx = j;
            for(int i = j + 1; i < mat.m; i++){
                if(std::abs(mat[pi[i]][j]) > max){
                    max = std::abs(mat[pi[i]][j]);
                    maxidx = i;
                }
            }
            pi.swap(maxidx, j);
        }
        return pi;
    }

    /**
     * @brief Decomposes the matrix A into F using a given permutation matrix
     * 
     * @tparam T The flaoting point type
     * @tparam IDX the index type
     * @param A [in/out] The matrix to decompose result is stored in A
     * @param pi the permutation matrix
     */
    template<typename T, typename IDX=std::size_t>
    void LUDecompose(Matrix<T, IDX> &A, PermutationMatrix<IDX> &pi){
        // naive algorithm
        pi.apply(A);
        for(IDX j = 0; j < A.n; j++){
            for(IDX i = j + 1; i < A.m; i++){
                A[i][j] /= A[j][j]; // multipliers
                for(IDX k = j + 1; k < A.n; k++){
                    A[i][k] -=  A[i][j] * A[j][k];
                }
            }
        }
    }

    /**
     * @brief forward substitution
     * 
     * @tparam T - floating point
     * @tparam IDX - index type
     * @param A - LU decomposed matrix
     * @param pi - permutation matrix
     * @param b - RHS vector
     * @param y - solution of Ly=b
     */
    template<typename T, typename IDX=std::size_t>
    void FWDSub(Matrix<T, IDX> &A, PermutationMatrix<IDX> &pi, T*b, T*y){
        for(IDX i=0;i<A.m;i++){
            y[i] = b[pi[i]];
            for(IDX j=0;j<i;j++){
                y[i]-=A[i][j] * y[j];
            }
        }
    }

    template<typename T, typename IDX=std::size_t>
    void backsub(Matrix<T, IDX> &A, T*y, T*x) requires std::integral<IDX>{
        for(IDX i=A.m-1; i != (IDX) -1; i--){ // SAFE FOR UNSIGNED INTS (STOPS AT 0)
            for(IDX k=i+1; k<A.n; k++){
                y[i] -= A[i][k] * y[k];
            }
            x[i] = y[i]/A[i][i];
        }
    }
}
