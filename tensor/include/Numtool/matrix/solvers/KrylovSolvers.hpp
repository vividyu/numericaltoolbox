#pragma once
#include <Numtool/TensorConcepts.hpp>
#include <Numtool/tensorUtils.hpp>
#include <Numtool/matrix/matrix.hpp>
#include <stdio.h>
#include <iostream>
#include <vector>
namespace MATH::TENSOR {

    /**
     * @brief Krylov Subspace Methods
     * 
     */
    namespace KRYLOV{

        template<typename T, typename IDX, typename L>
        void bicgstab(L &A, T *x, const T *b, T epsilon, IDX kmax) requires LinearOperator<L, T>{
            // sizes
            IDX m = A.getM();
            // real values
            T alpha = 1, omega = 1, beta, tau;
            T errtol = epsilon * norm<T, IDX, 2>(b, A.getM());

            // Vectors
            Vector<T, IDX> r(A.getM());
            Vector<T, IDX> rhat(A.getM());
            Vector<T, IDX> p(A.getM()), v(A.getM()), t(A.getM()), s(A.getN());

            std::vector<T> rho;
            rho.reserve(kmax + 1);
            rho.push_back(1.0);

            // r = b - Ax
            A.mult(x, r.data);
            for(int i = 0; i < A.getM(); i++) r[i] = b[i] - r[i];

            // initialize vectors
            rhat = r;
            p.zeroFill();
            v.zeroFill();

            rho.push_back(r.dot(rhat));
            for(IDX k = 0; k < kmax && r.norm() > errtol; k++){
                if(omega == 0){
                    ERR::throwWarning("bicgstab breakdown");
                    return;
                }
                // calculate beta                
                beta = (rho[k + 1] / rho[k]) * (alpha / omega);

                // update p = r + beta * (p - omega * v)
                p *= beta;
                p.addScaled(r, 1.0);
                p.addScaled(v, -beta * omega);

                // v = Ap
                A.mult(p.data, v.data);

                // alpha = rho_k / (r0hat^T v)
                if( (tau = rhat.dot(v)) == 0 ){
                    ERR::throwWarning("bicgstab breakdown");
                    return;
                }
                alpha = rho[k+1] / tau; 

                // calculate s and t
                s = r;
                s.addScaled(v, -alpha);
                A.mult(s.data, t.data);

                // update omega and rho
                if( (tau = t.dot(t)) == 0){
                    ERR::throwWarning("bicgstab breakdown");
                    return;
                }
                omega = t.dot(s) / tau;
                rho.push_back(-omega * rhat.dot(t));

                // update x
                for(int i = 0; i < A.getN(); i++)
                    x[i] += alpha * p[i] + omega * s[i];

                r = s;
                r.addScaled(t, -omega);
            }


        }


        /**
         * @brief Compute a Givens rotation for a vector [ f ; g ]
         * 
         * A givens rotation is the matrix:
         * G = 
         * |  c  s |
         * | -s  c |
         * 
         * That rotates the vector s.t G * [f ; g] = [r; 0]
         * 
         * This is an orthogonal rotation
         * @tparam T the floating point type
         * @param f the first component of the vector
         * @param g the second component of the vector
         * @param c [out] the cosine of the rotation (c in the matrix)
         * @param s [out] the sine of the rotation (s in the matrix)
         * @param r [out] the r which is the top componenet of the resulting vector
         * 
         * Continuous real plane rotation based on Edward Anderson
         * http://www.netlib.org/lapack/lawnspdf/lawn150.pdf
         */
        template<typename T>
        inline void givens(T f, T g, T *c, T *s, T *r) {
            if(g == 0) {
                *c = copysign(1, f);
                *s = 0;
                *r = abs(f);
            } else if (f == 0){
                *c = 0;
                *s = copysign(1, g);
                *r = abs(g);
            } else if (abs(f) > abs(g)) {
                T t = g / f;
                T u = copysign(1, f) * sqrt(1 + t * t);
                *c = 1 / u;
                *s = t * *c;
                *r = f * u;
            } else {
                T t = f / g;
                T u = copysign(1, g) * sqrt(1 + t * t);
                *s = 1/ u;
                *c = t * *s;
                *r = g * u;
            }
        }

        template<typename T, typename IDX, typename L>
        void arnoldi(
            L &A,
            std::vector<Vector<T, IDX>> &V,
            std::vector<Vector<T, IDX>> &H
        ) requires LinearOperator<L, T> {
            Vector<T, IDX> v_k1(A.getM()); // create vector
            IDX k = V.size() - 1;
            Vector<T, IDX> h_k(k);
            A.mult(V[k].data, v_k1.data);
            // Modified Gram-Schmidt
            for(int j = 0; j <= k; j++){
                h_k[j] = v_k1.dot(V[j]);
                v_k1.addScaled(V[j], -h_k[j]);
            }
            h_k[k+1] = v_k1.norm();
            V.push_back(v_k1);
            H.push_back(h_k);
        }

        template<typename T, typename IDX, typename L>
        void gmres(L &A, T *x, const T *b, T epsilon, IDX kmax) requires LinearOperator<L, T>{
            Vector<T, IDX> r(A.getM());
            Vector<T, IDX> w(A.getM());
            Vector<T, IDX> y(A.getM());
            Vector<T, IDX> x_k(A.getN());
            std::vector<Vector<T, IDX>> V;
            std::vector<Vector<T, IDX>> H;
            T rho, beta;
            IDX n = A.getN();
            IDX m = A.getM();
            // r = b - Ax
            A.mult(x, r.data);
            for(int i = 0; i < A.getM(); i++) r[i] = b[i] - r[i];
            //v1 = r/norm(r)
            Vector<T, IDX> v1(r);
            rho = r.norm();
            v1 *= (1.0 / rho);
            V.push_back(v1);
            beta = rho;

            Vector<T, IDX> g(kmax + 1);
            g.zeroFill();
            g[0] = rho;
            for(IDX k = 0; k < kmax && rho > epsilon * norm<T, IDX, 2>(b, m); k++){
                arnoldi(A, V, H);

                // TODO: test for orthogonality and reorthogonalize if necessary
                if(H[k][k+1] < epsilon){
                    // finish by fact that 0 vector is orthogonal to all other vectors
                    break;
                }
                V[k+1] *= 1.0 / H[k][k+1]; // divide by norm

                // Apply Givens Rotation to H
                T c, s;
                givens<T>(H[k][k], H[k][k+1], &c, &s, &H[k][k]);
                H[k][k+1] = 0;
                // Apply Givens Rotation to g
                g[k+1] = -s * g[k]; // g[k+1] should be zero
                g[k] = c * g[k];
                // update rho
                rho = std::abs(g[k+1]);

                // solve upper triangular
                for(IDX i = 0; i <= k; i++) w[i] = g[i];
                for(IDX i = k; i != (IDX) -1; i--){
                    for(IDX j = i + 1; j <= k; j++){
                        w[i] -= H[j][i] * w[j]; // H indexing is column first
                    }
                    y[i] = w[i]/H[i][i];
                }

                // Compute x_k = x0 + V * y
                for(int i = 0; i < A.getN(); i++) x_k[i] = x[i];
                for(int j = 0; j <= k; j++){
                    for(int i = 0; i < A.getM(); i++){
                        x_k[i] += V[j][i] * y[j];
                    }
                }
            }

            // copy x_k to x as the result
            for(int i = 0; i < A.getN(); i++) x[i] = x_k[i];
        }

        /**
         * @brief Conjugate Gradient Descent
         * Based on C. T. Kelley Iterative methods
         * 
         * @tparam T The floating point type
         * @tparam IDX the index type
         * @tparam L The linear operator type
         * @param A The linear operator
         * @param x the result vector (initialize with initial guess)
         * @param b the rhs vector
         * @param epsilon the convergence tolerance
         * @param kmax the maximum number of iterations
         */
        template<typename T, typename IDX, typename L>
        void cg(L &A, T *x, const T *b, T epsilon, IDX kmax) requires LinearOperator<L, T>{
            T rhokm1, rhok, beta, alpha;
            IDX n = A.getN();
            T bnorm = norm<T, IDX, 2>(b, n);
            T *r = new T[A.getM()];
            T *p =  new T[n];
            T *Ap = new T[n];
            // get initial residual
            A.mult(x, r);
            for(IDX i = 0; i < n; i++) r[i] = b[i] - r[i];

            // initial iteration
            rhok = innerprod<T, IDX>(r, r, A.getM());
            if(sqrt(rhok) <= epsilon * bnorm) return;
            for(IDX i = 0; i < n; i++) p[i] = r[i];
            A.mult(p, Ap);
            alpha = rhok / innerprod<T, IDX>(p, Ap, n);
            addScaled<T, IDX>(x, alpha, p, n);
            addScaled<T, IDX>(r, -alpha, Ap, n);
            rhokm1 = rhok;
            rhok = innerprod<T, IDX>(r, r, A.getM());

            // subsequent iterations
            IDX k = 1;
            while(sqrt(rhokm1) > epsilon * bnorm){
                if(k >= kmax){
                    std::cout << "Reached maximum iterations in CG descent\n";
                    return;
                }
                beta = rhok / rhokm1;
                for(IDX i = 0; i < n; i++) p[i] = r[i] + beta * p[i];
                A.mult(p, Ap);
                alpha = rhok / innerprod<T, IDX>(p, Ap, n);
                addScaled<T, IDX>(x, alpha, p, n);
                addScaled<T, IDX>(r, -alpha, Ap, n);
                rhokm1 = rhok;
                rhok = innerprod<T, IDX>(r, r, A.getM()); // 2norm squared
                k++;
            }

            delete[] r;
            delete[] p;
            delete[] Ap;
        }
    }
}