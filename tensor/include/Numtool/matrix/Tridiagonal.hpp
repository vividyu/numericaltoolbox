/**
 * @file Tridiagonal.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Tridiagonal Matrix Implementation
 * @version 0.1
 * @date 2021-11-13
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#pragma once
#include <vector>
#include <Numtool/TensorConcepts.hpp>
#include <concepts>
#include <Numtool/Errors.h>
#include <iostream>
#include <iomanip>

namespace MATH::TENSOR{
    

    template<typename T, std::integral IDX>
    class TridiagMat {
        public:
        std::vector<T> upperdiag;
        std::vector<T> maindiag;
        std::vector<T> lowerdiag;

        /**
         * @brief Construct a empty Tridiag object
         * 
         */
        TridiagMat() : upperdiag{}, maindiag{}, lowerdiag{} {};

        /**
         * @brief Construct a new Tridiag matrix with given constants for the whole diagonals
         * 
         * @param a initialize all lower diagonal entries to a
         * @param b initialize all main diagonal entries to b
         * @param c initialize all upper diagonal entries to c
         * @param n size of the main diagonal 
         */
        TridiagMat(T a, T b, T c, IDX n);

        /**
         * @brief Uses Thomas's algorithm to solve Ax=b
         * Alters the matrix to prestore the forward elimination step
         * @param b the b vector (is also altered as part of the elimination step)
         */
        void ThomasForward(IndexableVector auto &b);

        /**
         * @brief Call after using ThomasForward
         * solves x
         * @param x preallocated vector of size n 
         * @param b forward eliminated b vector
         */
        void ThomasSolve(IndexableVector auto &x, IndexableVector auto &b);

        /**
         * @brief Uses Thomas's algorithm to solve Ax = b without altering the matrix or b vector
         * 
         * @param x the solution (preallocated to size n)
         * @param b the RHS vector
         */
        void constThomasSolve(IndexableVector auto &x, const IndexableVector auto &b) const;

        /**
         * @brief computes A * x
         * 
         * @param x the x vector (size n)
         * @param result the result of the matrix vector multiplication (preallocated size n)
         */
        void matvec(const IndexableVector auto &x, IndexableVector auto &result);

        /**
         * @brief Add a row to the matrix
         * 
         * @param a The subdiagonal entry
         * @param b The main diagonal entry
         * @param c The entry above the diagonal
         */
        void addRow(T a, T b, T c);

        /**
         * @brief 
         * 
         * @param offset the offset from the main diagonal [-1, 0 or 1]
         * @param row which row of the overall matrix to index
         */
        T &quickindex(IDX offset, IDX row){
            switch(offset){
                case -1:
                    return lowerdiag[row - 1];
                case 0:
                    return maindiag[row];
                case 1:
                    return upperdiag[row];
            }
            return maindiag[-1]; // TODO: create some sort of optional reference and exception
        }

        /**
         * @brief Get the size of the matrix
         * 
         * @return IDX the length of the main diagonal
         */
        IDX size(){ return maindiag.size(); }
    };

    template<typename T, std::integral IDX>
    void TridiagMat<T, IDX>::addRow(T a, T b, T c){
        lowerdiag.push_back(a);
        maindiag.push_back(b);
        upperdiag.push_back(c);
    }

    template<typename T, std::integral IDX>
    TridiagMat<T, IDX>::TridiagMat(T a, T b, T c, IDX n) :
    upperdiag(n - 1, c), maindiag(n, b), lowerdiag(n-1, a)
    {}

    template<typename T, std::integral IDX>
    void TridiagMat<T, IDX>::ThomasForward(IndexableVector auto &b){
        // Forward
        for(IDX i = 1; i < maindiag.size(); i++){
            T m = lowerdiag[i - 1] / maindiag[i-1];
            maindiag[i] -= m*upperdiag[i-1];
            b[i] -= m*b[i-1];
        }
    }

    template<typename T, std::integral IDX>
    void TridiagMat<T, IDX>::ThomasSolve(IndexableVector auto &x, IndexableVector auto &b){
        IDX n = maindiag.size();
        x[n-1] = b[n-1] / maindiag[n-1];
        for(IDX i = n-2; i >= 0; i--){
            x[i] = (b[i] - upperdiag[i] * x[i+1]) / maindiag[i];
        }
    }

    template<typename T, std::integral IDX>
    void TridiagMat<T, IDX>::constThomasSolve(IndexableVector auto &x, const IndexableVector auto &b) const {
        IDX n = maindiag.size();
        double *maindiagtmp = new double[n];
        double *btmp = new double[n];
        // Forward
        btmp[0] = b[0];
        maindiagtmp[0] = maindiag[0];
        for(IDX i = 1; i < n; i++){
            T m = lowerdiag[i - 1] / maindiagtmp[i-1];
            maindiagtmp[i] = maindiag[i] - m*upperdiag[i-1];
            btmp[i] = b[i] - m*btmp[i-1];
        }
        // reverse
        x[n-1] = btmp[n-1] / maindiagtmp[n-1];
        for(IDX i = n-2; i >= 0; i--){
            x[i] = (btmp[i] - upperdiag[i] * x[i+1]) / maindiagtmp[i];
        }
        delete[] maindiagtmp;
        delete[] btmp;
    }

    template<typename T, std::integral IDX>
    void TridiagMat<T, IDX>::matvec(const IndexableVector auto &x, IndexableVector auto &result){
        IDX n = maindiag.size();
        result[0] = maindiag[0] * x[0] + upperdiag[0] * x[1];
        for(IDX row = 1; row < n-1; row++){
            result[row] = lowerdiag[row-1] * x[row - 1] +
                          maindiag[row] * x[row] + 
                          upperdiag[row] * x[row + 1];
        }
        result[n-1] = lowerdiag[n-2] * x[n-2] + maindiag[n-1] * x[n-1];
    }

    // #################################################################################

    // -------------------------
    // - Symmetric Tridiagonal -
    // -------------------------

    template<typename T, std::integral IDX>
    class SymTridiagMat {
        private:
        std::vector<T> maindiag;
        std::vector<T> offdiag;

        public:

        // ----------------
        // - Constructors -
        // ----------------

        /**
         * @brief Default Constructor constructs an empty matrix
         */
        SymTridiagMat() : maindiag{}, offdiag{} {}

        // -------------------
        // - Getters/Setters -
        // -------------------
        /**
         * @brief Get the vector corresponding to the main diagonal by reference
         * 
         * @return std::vector<T>& reference to the main diagonal
         */
        std::vector<T> &getMaindiag(){ return maindiag; }

        /**
         * @brief Get the vector corresponding to the off diagonal entries by reference
         * 
         * @return std::vector<T>& reference to the off diagonal
         */
        std::vector<T> &getOffdiag(){ return offdiag;}

        // ---------------
        // - Information -
        // ---------------
        /**
         * @brief Get the number of rows
         * 
         * @return IDX the number of rows
         */
        IDX getM(){return maindiag.size();}

        IDX getM() const {return maindiag.size();}

        /**
         * @brief Get the number of columns
         * 
         * @return IDX the number of columns
         */
        IDX getN(){return maindiag.size();}

        IDX getN() const {return maindiag.size();}

        // -----------------------------------
        // - Element Access and Manipulation -
        // -----------------------------------

        /**
         * @brief Add a row to the matrix
         * 
         * Note:
         * Use the 1 argument version for the first row
         * 
         * @param main the entry to add along the main diagonal
         * @param off  the entry to add along the off diagonal
         */
        void addRow(T main, T off){
            maindiag.push_back(main);
            offdiag.push_back(off);
        }

        /**
         * @brief Add the first entry to the matrix
         * 
         * @param main the entry to add along the main diagonal
         */
        void addRow(T main){
            if(maindiag.size() != 0) ERR::throwWarning("This should only be used for the first entry to the matrix.");
            maindiag.push_back(main);
        }

        // ------
        // - IO -
        // ------
        friend std::ostream &operator<<(std::ostream &output, const SymTridiagMat<T, IDX> &tri){
            static constexpr int OUTWIDTH = 10;
            output << std::setprecision(6);
            output <<  std::setw(OUTWIDTH) << tri.maindiag[0];
            output << " " << std::setw(OUTWIDTH) << tri.offdiag[0];
            for(int j = 2; j < tri.getN(); j++) output << " " << std::setw(OUTWIDTH) << 0.0;
            output << "\n";
            for(int i = 1; i < tri.getM() - 1; i++){
                for(int j = 0; j < i - 1; j++) output << std::setw(OUTWIDTH) << 0.0 << " ";
                output << std::setw(OUTWIDTH) << tri.offdiag[i - 1] << " ";
                output << std::setw(OUTWIDTH) << tri.maindiag[i] << " ";
                output << std::setw(OUTWIDTH) << tri.offdiag[i];
                for(int j = i+2; j < tri.getN(); j++) output << " " << std::setw(OUTWIDTH) << 0.0;
                output << "\n";
            }
            for(int j = 0; j < tri.getN() - 2; j++){
                output << std::setw(OUTWIDTH) << 0.0 << " ";
            }
            output << std::setw(OUTWIDTH) << tri.offdiag[tri.getN() - 2] << " ";
            output << std::setw(OUTWIDTH) << tri.maindiag[tri.getM() - 1] << "\n";
            return output;
        }

    };

}