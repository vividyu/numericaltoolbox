/**
 * @file Preconditioners.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Preconditioners
 * @version 0.1
 * @date 2022-01-03
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#pragma once
#include <Numtool/tensorUtils.hpp>

namespace MATH::TENSOR::PRECON {
    template<typename T, typename IDX>
    class Preconditioner {
        public:
        virtual void mult(const T *x, T * out) const = 0;

        virtual void invmult(const T *x, T * out) const = 0;

        virtual IDX getM() const = 0;

        virtual IDX getN() const = 0;

        /**
         * @brief Gets the matrix inner product
         * < a, Cb >
         * 
         * @param a the first vector
         * @param b the second vector
         * @return T the inner product
         */
        virtual T matInnerProd(const T *a, const T *b) const = 0;
    };

    template<typename T, typename IDX>
    class IdentityPreconditioner : public Preconditioner<T, IDX>{
        private:
        IDX n;

        public:
        IdentityPreconditioner(IDX n) : n(n) {}

        void mult(const T *x, T *out) const { for(IDX i = 0; i < n; i++) out[i] = x[i]; }

        void invmult(const T *x, T *out) const { for(IDX i = 0; i < n; i++) out[i] = x[i]; }

        IDX getM() const { return n; }

        IDX getN() const { return n; }

        T matInnerProd(const T *a, const T *b) const{
            return innerprod<T, IDX>(a, b, getN());
        }
    };
}