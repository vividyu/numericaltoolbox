/**
 * @file TensorEnums.h
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief 
 * @version 0.1
 * @date 2021-09-24
 * Tensor Enumerations
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef TENSOR_ENUMS_H
#define TENSOR_ENUMS_H
namespace MATH {
    namespace TENSOR {
        /**
         * @brief The ordering for a matrix
         * Row major order (C style) ordering means that within a row, data members are adjacent in memory
         * Column major order (Fortran style) means that within a column data members are adjacent
         * Indexing operations will always be Matrix_{row, column} the ordering just changes which order of iteration is most efficient 
         */
        enum ORDERING {
            ROW_MAJOR_ORDER = 0,
            COLUMN_MAJOR_ORDER = 1
        };

        enum MATRIX_TYPE {
            SPARSE_M,
            DENSE_M
        };
    }
}

#endif