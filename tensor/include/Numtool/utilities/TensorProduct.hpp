/**
 * @file TensorProduct.hpp
 * @author your name (you@domain.com)
 * @brief Tensor products
 * @version 0.1
 * @date 2022-04-18
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once
#include <concepts>
#include <ostream>
#include <tuple>
#include <Numtool/Errors.h>
#include <Numtool/CompGeo.hpp>
#include <Numtool/TMPDefs.hpp>
namespace MATH::TENSOR_PROD {

    #if __cplusplus > 201703L

    template<typename Polyfunc, typename T, int k>
    concept Function1Variate = requires(
        Polyfunc &func,
        T xi
    ){
        { func.template eval<0>(xi) } -> std::convertible_to<T>;
        // TODO:: use std::integer_sequence and pack expansion to ensure all in between functions are available
        { func.template eval<k>(xi) } -> std::convertible_to<T>;

        { func.template derivative<0>(xi) } -> std::convertible_to<T>;
        // TODO:: use std::integer_sequence and pack expansion to ensure all in between functions are available
        { func.template derivative<k>(xi) } -> std::convertible_to<T>;

        { func.template derivative2nd<0>(xi) } -> std::convertible_to<T>;
        // TODO:: use std::integer_sequence and pack expansion to ensure all in between functions are available
        { func.template derivative2nd<k>(xi) } -> std::convertible_to<T>;
    };

    template<typename Polyfunc, typename T, int k>
    concept Function1VariateRuntime = requires(
        Polyfunc &func,
        T xi
    ){
        { func.template eval(0, xi) } -> std::convertible_to<T>;
        // TODO:: use std::integer_sequence and pack expansion to ensure all in between functions are available
        { func.template eval(k, xi) } -> std::convertible_to<T>;

        { func.template derivative(0, xi) } -> std::convertible_to<T>;
        // TODO:: use std::integer_sequence and pack expansion to ensure all in between functions are available
        { func.template derivative(k, xi) } -> std::convertible_to<T>;

        // Ensure up to kth derivatives are available for each func
        {func.jthDerivative(0, 0, xi)} ->std::convertible_to<T>;
        {func.jthDerivative(0, k, xi)} ->std::convertible_to<T>;
        {func.jthDerivative(k, 0, xi)} ->std::convertible_to<T>;
        {func.jthDerivative(k, k, xi)} ->std::convertible_to<T>;

        // { func.template derivative2nd<0>(xi) } -> std::convertible_to<T>;
        // // TODO:: use std::integer_sequence and pack expansion to ensure all in between functions are available
        // { func.template derivative2nd<k>(xi) } -> std::convertible_to<T>;
    };

    #endif

    template<int a, int b>
    constexpr int pow_const(){
        if constexpr (b == 0) return 1;
        else return pow_const<a, b-1>() * a;
    }

    template<int... indices>
    struct MultiIndex{};

    template<int ifirst, int... indices>
    struct MultiIndex<ifirst, indices...> : MultiIndex<indices...> {
        constexpr int value(){ return ifirst; }
    };

    template<int ndim>
    struct MultiIndexAlt{
        int indices[ndim] = {0};

        inline int get(int idim){ return indices[idim]; }

        bool increment(int imax){
            int idim = ndim - 1;
            while(idim >= 0){
                indices[idim]++;
                if(indices[idim] > imax){
                    indices[idim] = 0;
                    idim--;
                } else {
                    return true;
                }
            }
            return false; // increment unsuccessful
        }

        inline int &operator[] (int idim){ return indices[idim]; }
        

        void print(std::ostream &out){
            out << "MultiIndex: {" << indices[0];
            for(int i = 1; i < ndim; i++) out << ", " << indices[i];
            out << "}\n";
        }
    };

    /**
     * @brief Multi Index where a position can be locked to generate
     * all the multi indices with the given position unchanging
     * 
     * @tparam ndim number of dimensions
     * @tparam idxmax maximum index value + 1
     *          e.g for P1 idxmax = 2 b/c fcn can be p0 or p1
     */
    template<int ndim, int idxlim>
    struct LockedMultiIndex {
        int idxs[ndim] = {0};
        int lockedDim = 0;

        LockedMultiIndex() = default;

        LockedMultiIndex(int lockedDim) : lockedDim(lockedDim) {}

        /**
         * @brief get the equivalent 1d array for the multi index
         * 
         * @return int the index in a 1d array
         */
        int globalIdx(){
            int mult = 1;
            int ret = 0;
            for(int idim = ndim - 1; idim >= 0; idim--){
                ret += idxs[idim] * mult;
                mult *= idxlim;
            }
            return ret;
        }

        /**
         * @brief increment the multi-index
         * 
         * @return true if the increment results in a valid multi index
         * @return false otherwise, this means to stop incrementing
         */
        bool increment(){
            int idim = ndim - 1;
            if(lockedDim == idim) idim--;
            while(idim >= 0){
                idxs[idim]++;
                if(idxs[idim] >= idxlim){
                    idxs[idim] = 0;
                    if(lockedDim == idim - 1) idim -= 2;
                    else idim--;
                } else {
                    return true;
                }
            }
            return false; // increment unsuccessful
        }
    };

    template<int ndim>
    struct DimensionalIndices{
        typedef MATH::TMP::sized_tuple<ndim, int>::tuple_type type;
    };

    template<> struct DimensionalIndices<1> { typedef std::tuple<int> type; };
    template<> struct DimensionalIndices<2> { typedef std::tuple<int, int> type; };
    template<> struct DimensionalIndices<3> { typedef std::tuple<int, int, int> type; };
    template<> struct DimensionalIndices<4> { typedef std::tuple<int, int, int, int> type; };
    template<typename T, int ndim>
    class MultidimDerivativeAccessor {
        /**
         * @brief Get the Derivative for given indices
         * 
         * @param iderivatives the order of the derivative in each dimension
         *      Derivatives are accesed in order x, y, z, ...
         *      i.e for 3D a tuple with values 3, 4, and 2 would
         *      return f_xxxyyyyzz (3 x derivatives, 4 y derivatives, and 2 z derivatives)
         *      NOTE: this means that access will be z-fastest
         * @return T the value of the given derivative
         */
        virtual T getDerivative(DimensionalIndices<ndim>::type iderivatives) = 0;
    };

    /**
     * @brief Multidimensional derivative storage
     * 
     * @tparam T the floating point type
     * @tparam ndim the number of dimensions
     * @tparam nfunc the number of basis functions
     */
    template<typename T, int ndim, int nfunc>
    class MultidimDerivatives final : public MultidimDerivativeAccessor<T, ndim>{

        private:
        int stride;
        int calculateStorage(int max_derivative_order){
            return integer_pow(max_derivative_order + 1, ndim);
        }

        template<int idim>
        inline int strideIndex(DimensionalIndices<ndim>::type iderivatives, int stridemult){
            if constexpr (idim < ndim){
                return std::get<idim>(iderivatives) * stridemult 
                    + strideIndex<idim + 1>(iderivatives, stridemult * stride);  
            } else {
                return stridemult * std::get<ndim>(iderivatives);
            }
        }

        public:
        T *data;
        MultidimDerivatives(int max_derivative_order)
        : stride(max_derivative_order + 1),
          data{new T[calculateStorage(max_derivative_order)]}
        {}

        /**
         * @brief Get the Derivative for given indices
         * 
         * @param iderivatives the order of the derivative in each dimension
         *      Derivatives are accesed in order x, y, z, ...
         *      i.e for 3D a tuple with values 3, 4, and 2 would
         *      return f_xxxyyyyzz (3 x derivatives, 4 y derivatives, and 2 z derivatives)
         *      NOTE: this means that access will be z-fastest
         * @return T the value of the given derivative
         */
        T getDerivative(decltype(DimensionalIndices<ndim>::type) iderivatives) override {
            return data[strideIndex<0>(iderivatives, 1)];
        }

        T &derivRef(decltype(DimensionalIndices<ndim>::type) iderivatives) {
            return data[strideIndex<0>(iderivatives, 1)];
        }

        ~MultidimDerivatives(){delete[] data;}
    };

    /**
     * @brief Full Tensor product
     * the multi-index set consists of all multi-indices with the l2 norm <= k
     * 
     * @tparam T the numeric type
     * @tparam Polyfunc the polynomial function
     * @tparam ndim the dimensionality
     * @tparam k the degree of the polynomial
     */
    template<typename T, typename Polyfunc, int ndim, int k>
    class LinfTensorProduct{
        private:
        Polyfunc func;

        #if __cplusplus > 201703L
        static_assert(Function1VariateRuntime<Polyfunc, T, k>);
        #endif

        public:

        static constexpr int cardinality(){
            return pow_const<k + 1, ndim>();
        }

        void getProduct(const T *x, T *result){
            MultiIndexAlt<ndim> mindex;
            result[0] = func.eval(0, x[0]);
            for(int idim = 1; idim < ndim; idim++) 
                result[0] *= func.eval(0, x[idim]);

            int iresult = 1;
            while(mindex.increment(k)){
                result[iresult] = func.eval(mindex[0], x[0]);
                for(int idim = 1; idim < ndim; idim++)
                    result[iresult] *= func.eval(mindex[idim], x[idim]);
                iresult++;
            }
        }

        void getGradient(const T*x, T **dBidxj){
            MultiIndexAlt<ndim> mindex;
            dBidxj[0][0] = func.derivative(0, x[0]);
            for(int idim = 1; idim < ndim; idim++){
                dBidxj[0][0] *= func.eval(0, x[idim]);
            }

            for(int j = 1; j < ndim; ++j){
                dBidxj[0][j] = func.eval(0, x[0]);
                for(int idim = 1; idim < ndim; idim++){
                    if(idim == j)
                        dBidxj[0][j] *= func.derivative(0, x[idim]);
                    else 
                        dBidxj[0][j] *= func.eval(0, x[idim]);
                }
            }

            int iresult = 1;
            while(mindex.increment(k)){
                dBidxj[iresult][0] = func.derivative(mindex[0], x[0]);
                for(int idim = 1; idim < ndim; idim++){
                    dBidxj[iresult][0] *= func.eval(mindex[idim], x[idim]);
                }

                for(int j = 1; j < ndim; ++j){
                    dBidxj[iresult][j] = func.eval(mindex[0], x[0]);
                    for(int idim = 1; idim < ndim; idim++){
                        if(idim == j)
                            dBidxj[iresult][j] *= func.derivative(mindex[idim], x[idim]);
                        else 
                            dBidxj[iresult][j] *= func.eval(mindex[idim], x[idim]);
                    }
                }
                iresult++;
            }
        }

        private:
        template<int idim, typename idx1, typename idx2, typename... Args_type>
        inline T arb_derivative_helper(T *xi, idx1 ibasis, idx2 iderivative, Args_type... args){
            static_assert(std::same_as<idx1, int>);
            static_assert(std::same_as<idx2, int>);
            if constexpr (sizeof...(args) > 0){
                return arb_derivative_helper<idim + 1, Args_type...>(xi, args...) * func.jthDerivative(ibasis, iderivative, xi[idim]);
            } else {
                return func.jthDerivative(ibasis, iderivative, xi[idim]);
            }
        }


        public:
        /**
         * @brief Evaluate an arbitrary cross derivative in the tensor product
         * 
         * @param xi the location in the reference domain
         * @param fcn_and_derivative_nrs alternating function indices and derivative orders to specify what to evaluate
         * @return the value of that arbitrary derivative
         */
        template<typename... Args_type>
        T evalArbitraryDerivative(T *xi, Args_type... fcn_and_derivative_nrs){
            static_assert(sizeof...(fcn_and_derivative_nrs) == 2 * ndim);
            return arb_derivative_helper<0>(xi, fcn_and_derivative_nrs...);
        }

        private:
        void getGradientOperator(
            T *xi,
            T *coeffs,
            COMP_GEO::TRANSFORMATIONS::Jacobian<T, ndim> &jac,
            T grad[cardinality()][ndim]
        ){
            T gref[cardinality()][ndim]; // reference domain
            T phis[k];
            int gidxs[ndim];
            for(int idim = 0; idim < ndim; idim++){
                T gradprod1d[k] = {0};
                // create the locked multi-index and find the global indices
                LockedMultiIndex<ndim, k + 1> mindex{idim};

                do {
                    for(int ibasis = 0; ibasis < k; ibasis++){
                        mindex.idxs[idim] = ibasis;
                        gidxs[ibasis] = mindex.globalIdx();
                        phis[ibasis] = coeffs[gidxs[ibasis]];
                    } 

                    // get the derivative operator d_ik applied to coefficients phi_i
                    // multiplying these new coefficents by the basis function evaluations
                    // will get the gradient in the reference domain
                    derivativeOperator(xi[idim], phis, gradprod1d);

                    // form the nD derivatives from the 1D derivative operator
                    for(int ibasis = 0; ibasis < k; ibasis++){
                        T contrib = gradprod1d[ibasis];
                        for(int idim2 = 0; idim2 < ndim; idim2++){
                            if(idim2 != idim){
                                contrib *= func.eval(ibasis, xi[idim2]);
                            }
                        }
                        gref[gidxs[ibasis]][idim] += contrib;
                    }
                } while (mindex.increment());
            }
            // apply the jacobian
            for(int b = 0; b < k; b++) for(int d = 0; d < ndim; d++) grad[b][d] = 0;
            for(int kbasis = 0; kbasis < k; kbasis++){
                for(int ldim = 0; ldim < ndim; ldim++){
                    for(int jdim = 0; jdim < ndim; jdim++){
                        grad[kbasis][ldim] += jac[ldim][jdim] * gref[kbasis][jdim];
                    }
                }
            }
        }

        template<int idim = 0>
        inline void fillGradAccessor(
            MultidimDerivatives<T, ndim, cardinality()> &derivs,
            T fcnEvals[cardinality()],
            T gradOperator[cardinality()][ndim]
        ){
            using namespace MATH::TMP;
            T gradx_i = 0;
            for(int ibasis = 0; ibasis < k; ibasis++){
                gradx_i += gradOperator[ibasis][idim] * fcnEvals[ibasis];
            }
            typename sized_tuple<ndim, int>::tuple_type indices{};
            std::get<idim>(indices) = 1;

            derivs.derivRef(indices) = gradx_i;
            if constexpr(idim < ndim - 1){
                fillGradAccessor<idim + 1>(derivs, fcnEvals, gradOperator);
            }
        }

        public:
        MultidimDerivativeAccessor<T, ndim> *createDerivativeAccessor(
            T *xi,
            T *coeffs,
            COMP_GEO::TRANSFORMATIONS::Jacobian<T, ndim> &jac,
            int max_derivative_order
        ) {
            using namespace MATH::TMP;
            jac.formAdjandDet();
            MultidimDerivatives<T, ndim, cardinality()> *derivatives = 
                new MultidimDerivatives<T, ndim, cardinality()>(max_derivative_order);
            
            // calculate the solution
            T result[cardinality()];
            getProduct(xi, result);
            T sum = 0;
            for(int ibasis = 0; ibasis < cardinality(); ibasis++) 
                sum += coeffs[ibasis] * result[ibasis];
            derivatives->data[0] = sum;

            // get the gradient operator
            T gradOperator[cardinality()][ndim];
            getGradientOperator(xi, coeffs, jac, gradOperator);

            // calculate the gradient and add to accessor
            fillGradAccessor(*derivatives, result, gradOperator);

            for(int ideriv = 0; ideriv < max_derivative_order; ideriv++){
                // TODO
            }

        }
    };

    /**
     * @brief Tensor product where the product is at most k-degree
     * the multi-index set consists of all multi-indices with the l1 norm <= k
     * 
     * @tparam T the numeric type
     * @tparam Polyfunc the polynomial function
     * @tparam ndim the dimensionality 
     * @tparam k the degree of the polynomial
     */
    template<typename T, typename Polyfunc, int ndim, int k>
    class OrderLimitedTensorProduct{
        private:
        Polyfunc func;

        #if __cplusplus > 201703L
        static_assert(Function1Variate<Polyfunc, T, k>);
        #endif

        template<int d, int p>
        inline constexpr int sizeQ(){
            return MATH::binomial<p + d - 1, d - 1>();
        }
        
        template<int d, int j, int p>
        inline void Qpd(const T *xi, T *result){
            constexpr int idim = d - 1; // array index for xi
            // base case: d = 1
            // multiply by polynomial order p
            if constexpr (d == 1){
                result[0] = func.template eval<p>(xi[idim]);
            } else {
                // drill down 
                // start the new j from 0
                // p - j because we want overall order to be p
                Qpd<d - 1, 0, p - j>(xi, result);
                // block multiply
                T qj = func.template eval<j>(xi[idim]); // keep to single function evaluation
                for(int l = 0; l < sizeQ<d - 1, p - j>(); l++) result[l] *= qj;

                // move to next "block" of polynomial order j + 1
                if constexpr (j < p){
                    Qpd<d, j + 1, p>(xi, result + sizeQ<d - 1, p - j>());
                }
            }
        }

        template<int d, int i, int p>
        inline void gradQpdOld(const T *xi, T **grad){ // BASED ON PRODUCT RULE, LESS EFFICIENT
            constexpr int idim = d - 1; // array index for xi
            // base case : d = 1
            // value is delta^j_1 dQ^i/d\xi (x_1)
            if constexpr(d == 1){
                // only the gradient wrt 1st variable is nonzero for this
                grad[0][idim] = func.template derivative<p>(xi[idim]); 
                for(int j = 1; j < ndim; j++) grad[0][j] = 0.0;
            } else {
                // Product Rule
                // second term: drill down
                gradQpd<d - 1, 0, p - i>(xi, &grad[0]);
                // block multiply through by q^i(x_d)
                T qid = func.template eval<i>(xi[idim]);
                for(int l = 0; l < sizeQ<d-1, p-i>(); l++){
                    for(int j = 0; j < ndim; j++) grad[l][j] *= qid;
                }

                // kronkeker delta over loop
                constexpr int j = idim;
                T basisEval[sizeQ<d - 1, p - i>()];
                Qpd<d-1, 0, p-i>(xi, basisEval);
                // add the first term of the product rule
                T dbidxj = func.template derivative<i>(xi[j]);
                for(int l = 0; l < sizeQ<d-1, p-i>(); l++)
                    grad[l][j] = grad[l][j] + dbidxj * basisEval[l];

                // move to the next "block"
                if constexpr (i < p){
                    gradQpd<d, i + 1, p>(xi, &grad[sizeQ<d - 1, p - i>()]);
                }
            }
        }

        template<int d, int i, int p>
        inline void gradQpd(const T *xi, T **grad){
            constexpr int idim = d - 1; // array index for xi
            // base case : d = 1
            // value is delta^j_1 dQ^i/d\xi (x_1)
            if constexpr(d == 1){
                // only the gradient wrt 1st variable is nonzero for this
                grad[0][idim] = func.template derivative<p>(xi[idim]); 
                for(int j = 1; j < ndim; j++) grad[0][j] = func.template eval<p>(xi[idim]);
            } else {
                // drill down first
                gradQpd<d - 1, 0, p - i>(xi, &grad[0]);

                // case 1: j = d
                constexpr int j = idim;
                T basisEval[sizeQ<d - 1, p - i>()];
                Qpd<d-1, 0, p-i>(xi, basisEval);
                // add the first term of the product rule
                T dbidxj = func.template derivative<i>(xi[j]);
                for(int l = 0; l < sizeQ<d-1, p-i>(); l++)
                    grad[l][j] = grad[l][j] * dbidxj;

                // case 2: j != d
                // block multiply through by q^i(x_d)
                T qid = func.template eval<i>(xi[idim]);
                for(int l = 0; l < sizeQ<d-1, p-i>(); l++){
                    for(int j2 = 0; j2 < idim; j2++) grad[l][j2] *= qid;
                    for(int j2 = idim + 1; j2 < ndim; j2++) grad[l][j2] *= qid;
                }

                // move to the next "block"
                if constexpr (i < p){
                    gradQpd<d, i + 1, p>(xi, &grad[sizeQ<d - 1, p - i>()]);
                }
            }
        }

        template<int d, int i, int p>
        inline void hessDiagQpd(const T *xi, T **grad){
            constexpr int idim = d - 1; // array index for xi
            // base case : d = 1
            // value is delta^j_1 dQ^i/d\xi (x_1)
            if constexpr(d == 1){
                // only the gradient wrt 1st variable is nonzero for this
                grad[0][idim] = func.template derivative2nd<p>(xi[idim]); 
                for(int j = 1; j < ndim; j++) grad[0][j] = func.template eval<p>(xi[idim]);
            } else {
                // drill down first
                hessDiagQpd<d - 1, 0, p - i>(xi, &grad[0]);

                // case 1: j = d
                constexpr int j = idim;
                T basisEval[sizeQ<d - 1, p - i>()];
                Qpd<d-1, 0, p-i>(xi, basisEval);
                // add the first term of the product rule
                T dbidxj = func.template derivative2nd<i>(xi[j]);
                for(int l = 0; l < sizeQ<d-1, p-i>(); l++)
                    grad[l][j] = grad[l][j] * dbidxj;

                // case 2: j != d
                // block multiply through by q^i(x_d)
                T qid = func.template eval<i>(xi[idim]);
                for(int l = 0; l < sizeQ<d-1, p-i>(); l++){
                    for(int j2 = 0; j2 < idim; j2++) grad[l][j2] *= qid;
                    for(int j2 = idim + 1; j2 < ndim; j2++) grad[l][j2] *= qid;
                }

                // move to the next "block"
                if constexpr (i < p){
                    hessDiagQpd<d, i + 1, p>(xi, &grad[sizeQ<d - 1, p - i>()]);
                }
            }
        }

        template<int i>
        inline void product(const T *xi, T *result){
            Qpd<ndim, 0, i>(xi, result);
            if constexpr (i < k){
                product<i + 1>( xi, &result[sizeQ<ndim, i>()] );
            }
        }

        template<int i>
        inline void gradproduct(const T *xi, T **result){
            gradQpd<ndim, 0, i>(xi, result);
            if constexpr (i < k){
                gradproduct<i + 1>( xi, &result[sizeQ<ndim, i>()] );
            }
        }

        template<int i>
        inline void hessDiagproduct(const T *xi, T **result){
            hessDiagQpd<ndim, 0, i>(xi, result);
            if constexpr (i < k){
                hessDiagproduct<i + 1>( xi, &result[sizeQ<ndim, i>()] );
            }
        }

        public:
        inline constexpr int cardinality(){ return MATH::binomial<k + ndim, ndim>(); }

        static inline constexpr int cardinalityStatic(){
            return MATH::binomial<k + ndim, ndim>();
        }

        /**
         * @brief Get the tensor product evaluated into an array
         * 
         * @param [in] xi the abscisse
         * @param [out] result the evaluated tensor product, size = cardinality()
         */
        void getProduct(const T *xi, T *result){
            product<0>(xi, result);
        }

        void getGradient(const T *xi, T **grad){
            gradproduct<0>(xi, grad);
        }

        void hessianDiagonal(const T *xi, T **HessDiag){
            hessDiagproduct<0>(xi, HessDiag);
        }

    };
}
