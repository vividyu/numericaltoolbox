/**
 * @file tensor.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Tensor and Matrix operations library
 * @version 0.1
 * @date 2022-05-04
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#pragma once
// Tensor Definitions
#include <Numtool/tensor/tensorclass.hpp>
// Matrix Definitions
#include <Numtool/matrix/matrix.hpp>
#include <Numtool/matrix/Tridiagonal.hpp>
#include <Numtool/matrix/Preconditioners.hpp>
// Matrix Solvers
#include <Numtool/matrix/solvers/directSolvers.hpp>
#include <Numtool/matrix/solvers/KrylovSolvers.hpp>