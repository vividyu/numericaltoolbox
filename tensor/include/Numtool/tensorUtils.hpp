/**
 * @file tensorUtils.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Utility Functions for Tensor and Matrix operations
 * @version 0.1
 * @date 2021-11-01
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#pragma once
#include <cmath>
#include <Numtool/TensorConcepts.hpp>
#include <Numtool/MathUtils.hpp>
#include <Numtool/utilities/TensorProduct.hpp>

namespace MATH::TENSOR {
    template<typename T, typename IDX, int P>
    inline T norm(const T *u, IDX n){
        T result = 0;
        for(IDX i = 0; i < n; i++){
            if constexpr (P == 2){
                // optimized
                result += SQUARED(u[i]);
            } else {
                result += pow(u[i], P);
            }
        }
        if constexpr (P == 2){
            result = sqrt(result);
        } else {
            result = pow(result, 1.0 / P);
        }
        return result;
    }

    template<typename T, typename V, typename IDX, int P>
    inline T norm(const V &u, IDX n) requires IndexableVector<V> {
        T result = 0;
        for(IDX i = 0; i < n; i++){
            if constexpr (P == 2){
                // optimized
                result += SQUARED(u[i]);
            } else {
                result += pow(u[i], P);
            }
        }
        if constexpr (P == 2){
            result = sqrt(result);
        } else {
            result = pow(result, 1.0 / P);
        }
        return result;
    }

    template<typename T, typename IDX>
    inline T innerprod(const T *u1, const T *u2, IDX n){
        T result = 0;
        for(IDX i = 0; i < n; i++) result += u1[i] * u2[i];
        return result;
    }

    template<typename T, typename IDX>
    inline void addScaled(T *u1, T alpha,  const T *u2, IDX n){
        for(IDX i = 0; i < n; i++) u1[i] += alpha * u2[i];
    }
}