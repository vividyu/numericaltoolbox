#pragma once

#include <concepts>
namespace MATH
{
    namespace TENSOR
    {
        /**
         * @brief A vector that just requires indexing
         * 
         * @tparam V the vector type
         */
        template<typename V>
        concept IndexableVector = requires(V &v) {
            {v[0]} -> std::convertible_to<double &>;
        };

        /**
         * @brief A vector on an inner product space
         * 
         * @tparam V the vector type
         */
        template<typename V>
        concept VectorConcept = 
            std::default_initializable<V> &&    // can be default initialized
            IndexableVector<V> &&               // can be indexed
            //std::constructible_from<V, int> &&  // can be constructed from a size (size might not be int)
            requires (V &v){
                {v.size()} -> std::convertible_to<int>; // can get the current size
                {v.norm()} -> std::convertible_to<double>; // has a norm
            };

        template<typename L, typename V>
        concept LinearOperator2 =
            VectorConcept<V> &&
            requires(const L &A, const V &x, V &out){
                {A.mult(x, out)};
                {A.getM()} -> std::convertible_to<int>;
                {A.getN()} -> std::convertible_to<int>;
            };

        /**
         * @brief A linear operator
         * has size information
         * Can operate on a vector
         * 
         * @tparam L the linear operator type
         * @tparam FLT the floating point type
         */
        template<typename L, typename FLT>
        concept LinearOperator = requires(const L &a, const FLT *x, FLT *out){
            {a.mult(x, out)};
            {a.getM()} -> std::convertible_to<int>;
            {a.getN()} -> std::convertible_to<int>;
        };

        template<typename L, typename FLT>
        concept InnerProdSpace_LinearOperator = requires(const L &a, const FLT *x, FLT *out){
            {a.mult(x, out)};
            {a.matInnerProd(x)} -> std::convertible_to<double>;
            {a.getM()} -> std::convertible_to<int>;
            {a.getN()} -> std::convertible_to<int>;
        };
    } // namespace TENSOR
} // namespace MATH