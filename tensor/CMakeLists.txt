cmake_minimum_required(VERSION 3.14)

project(nt_tensor VERSION 1.0.0 LANGUAGES CXX)

add_library(nt_tensor INTERFACE)
target_link_libraries(nt_tensor INTERFACE nt_general)
target_link_libraries(nt_tensor INTERFACE nt_util)
target_link_libraries(nt_tensor INTERFACE nt_memory)
target_link_libraries(nt_tensor INTERFACE nt_comp_geo)
target_link_libraries(nt_tensor INTERFACE RAJA)
target_link_libraries(nt_tensor INTERFACE umpire)
#target_link_libraries(nt_tensor INTERFACE BLAS::BLAS)
#target_compile_options(nt_tensor INTERFACE ${BLAS_LINKER_LAGS})
#target_include_directories(nt_tensor INTERFACE ${BLAS_INCLUDE_DIRS})
target_include_directories(nt_tensor 
    INTERFACE $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)

target_compile_features(nt_general INTERFACE cxx_std_20)
