/**
 * @file orthogonal.cpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Orthogonal operations
 * @version 0.1
 * @date 2021-07-23
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "orthogonal.hpp"
#include <ctgmath>

namespace MATH::TENSOR {

    template<typename T>
    inline void givens(T f, T g, T *c, T *s, T *r) {
        if(g == 0) {
            *c = copysign(1, f);
            *s = 0;
            *r = abs(f);
        } else if (f == 0){
            *c = 0;
            *s = copysign(1, g);
            *r = abs(g);
        } else if (abs(f) > abs(g)) {
            T t = g / f;
            T u = copysign(1, f) * sqrt(1 + t * t);
            *c = 1 / u;
            *s = t * *c;
            *r = f * u;
        } else {
            T t = f / g;
            T u = copysign(1, g) * sqrt(1 + t * t);
            *s = 1/ u;
            *c = t * *s;
            *r = g * u;
        }
    }

    // specialization declerations
    template void givens<float>(float, float, float *, float *, float *);
    template void givens<double>(double, double, double *, double *, double *);
    template void givens<long double>(long double, long double, long double *, long double *, long double *);

}