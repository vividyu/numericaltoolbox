/**
 * @file Legendre.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief 
 * @version 0.1
 * @date 2022-01-15
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#pragma once
#include <Numtool/MathUtils.hpp>

/**
 * @brief Numerically important polynomials and polynomial utilities
 * i.e Legendre, Lagrange, Chebyshev
 * 
 */
namespace MATH::POLYNOMIAL{
    /**
     * @brief Evaluation of a legendre polynomial in 1D templated by order
     * 
     * @tparam T the floating point type
     * @tparam Pn the polynomial order
     * @param xi the location in the reference domain on [-1, 1]
     * @return T the Pnth legendre polynomial evaluated at xi
     */
    template<typename T, int Pn>
    T legendre1d(T xi);

    template<typename T, int Pn>
    T dlegendre1d(T xi);

    template<>
    inline double legendre1d<double, 0>(double xi){ return 1; };

    template<>
    inline double legendre1d<double, 1>(double xi){ return xi; };

    template<>
    inline double legendre1d<double, 2>(double xi){
        return 3.0 / 2.0 * SQUARED(xi) - MATH::ONE_HALF<double>;
    };
    
    template<>
    inline double legendre1d<double, 3>(double xi){
        return 5.0 / 2.0 * CUBED(xi) - 3.0 / 2.0 * xi;
    };

    template<>
    inline double legendre1d<double, 4>(double xi){
        double x2 = SQUARED(xi);
        return 35.0 / 8.0 * x2 * x2 - 30.0 / 8.0 * x2 + 3.0 / 8.0;
    };


    // generalized to n dimensions but unoptimized
    // https://en.wikipedia.org/wiki/Legendre_polynomials#Recurrence_relations
    template<typename T, int Pn>
    inline T legendre1d(T xi){
        if constexpr (Pn == 0){
            return 1;
        }
        else if constexpr (Pn == 1){
            return xi;
        }
        else if constexpr (Pn == 2){
            return 3.0 / 2.0 * SQUARED(xi) - MATH::ONE_HALF<T>;
        } else {
            static_assert(Pn > 0);
            // recurrence relation is defined for P_{n+1} so find it easier to rewrite in this way
            constexpr int n = Pn - 1;
            return ((2 * n + 1) * xi * legendre1d<T, n>(xi) - n * legendre1d<T, n-1>(xi)) / ((T) (n + 1));
        }
    }

    // ---------------------------
    // - 1D Legendre Derivatives -
    // ---------------------------
    template<>
    inline double dlegendre1d<double, 0>(double xi){
        return 0;
    }

    template<>
    inline double dlegendre1d<double, 1>(double xi){
        return 1;
    }

    // generalized to n dimensions but unoptimized
    // https://en.wikipedia.org/wiki/Legendre_polynomials#Recurrence_relations
    template<typename T, int Pn>
    inline T dlegendre1d(T xi){
        if constexpr (Pn == 0){
            return 0;
        } else if constexpr (Pn == 1){
            return 1;
        } else if constexpr (Pn == 2){
            return 3.0 * xi;
        } else {
            // recurrence relation is defined for P_{n+1} so find it easier to rewrite in this way
            constexpr int n = Pn - 1;
            return (n + 1) * legendre1d<T, n>(xi) + xi * dlegendre1d<T, n>(xi);
        }
    }

    template<typename T, int Pn>
    inline T legendreD2(T xi){
        if constexpr(Pn <= 1){
            return 0;
        } else if constexpr(Pn == 2){
            return 3.0;
        } else if constexpr(Pn == 3){
            return 15.0 * xi;
        } else {
            constexpr int n = Pn - 1;
            return (n + 1) * dlegendre1d<T, n>(xi) + dlegendre1d<T, n>(xi) + xi * legendreD2<T, n>(xi); // TODO: double chek
        }
    }

    // -------------------------
    // - Kth Order Derivatives -
    // -------------------------
    // https://arxiv.org/pdf/1711.00925.pdf
    // gives general sum expansion
    template<typename T, int Pn, int k>
    inline T legendreDk(T xi){
        //TODO: may not be worth it
    }

    template<typename T>
    class Legendre1DFunc{
        public:
        template<int order>
        inline T eval(T xi){
            return legendre1d<T, order>(xi);
        }

        template<int order>
        inline T derivative(T xi){
            return dlegendre1d<T, order>(xi);
        }

        template<int order>
        inline T derivative2nd(T xi){
            return legendreD2<T, order>(xi);
        }
    };
}
