cmake_minimum_required(VERSION 3.14)

project(nt_polynomial VERSION 1.0.0 LANGUAGES CXX)

add_library(nt_polynomial INTERFACE)
target_include_directories(nt_polynomial 
    INTERFACE $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)
target_link_libraries(nt_polynomial INTERFACE nt_general)

target_sources(nt_polynomial INTERFACE include/Numtool/Polynomials.hpp)

target_compile_features(nt_polynomial INTERFACE cxx_std_17)
