add_library(nt_util INTERFACE)
target_include_directories(nt_util 
    INTERFACE $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)

target_compile_features(nt_util INTERFACE cxx_std_20)
