/**
 * @file Errors.h
 * @author your name (you@domain.com)
 * @brief Functions for logging errors
 * @version 0.1
 * @date 2021-04-01
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef ERRORS_H
#define ERRORS_H

#if __has_include(<source_location>)
#include <source_location>
#define srclocinc
#elif __has_include(<experimental/source_location>)
#include <experimental/source_location>
#define srclocincexp
#endif


#include <string_view>
#include <iostream>

namespace ERR {
    #ifdef srclocincexp
    static void throwError(const std::string_view message, const std::experimental::source_location &location = std::experimental::source_location::current()){
        std::cout << "Error: "
              << location.file_name() << "(line "
              << location.line() << ": col "
              << location.column() << ") `"
              << location.function_name() << "` "
              << message << '\n';
        exit(EXIT_FAILURE);
    }

    /**
     * @brief Print an error message but does not exit
     * 
     */
    static void throwWarning(const std::string_view message, const std::experimental::source_location &location = std::experimental::source_location::current()){
        std::cout << "Warning: "
              << location.file_name() << "(line "
              << location.line() << ": col "
              << location.column() << ") `"
              << location.function_name() << "` "
              << message << '\n';
    }
    #else

        #ifdef srclocinc
            static void throwError(const std::string_view message, const std::source_location &location = std::source_location::current()){
                std::cout << "Error: "
                    << location.file_name() << "(line "
                    << location.line() << ": col "
                    << location.column() << ") `"
                    << location.function_name() << "` "
                    << message << '\n';
                exit(EXIT_FAILURE);
            }

            /**
             * @brief Print an error message but does not exit
             * 
             */
            static void throwWarning(const std::string_view message, const std::source_location &location = std::source_location::current()){
                std::cout << "Warning: "
                    << location.file_name() << "(line "
                    << location.line() << ": col "
                    << location.column() << ") `"
                    << location.function_name() << "` "
                    << message << '\n';
            }
        #else 
        static void throwError(const std::string_view message){
            std::cout << message << "\n";
        }

        static void throwWarning(const std::string_view message){
            std::cout << message << "\n";
        }
        #endif
    #endif
}


#endif