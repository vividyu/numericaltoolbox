/**
 * @file MatrixTest.cpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Testing matrix subroutines
 * @version 0.1
 * @date 2022-01-03
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <Numtool/tensor.hpp>
#include <gtest/gtest.h>
using namespace MATH::TENSOR;
TEST(MatrixCopy, CopyToNew){
    Matrix<double, int> A{
            {3, 4, 1},
            {2, 2, 7},
            {15, 20, 1},
            {4, 1, 2}
    };

    Matrix<double, int> B(A);
    for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 3; ++j){
            ASSERT_EQ(B[i][j], A[i][j]);
        }
    }

    Matrix<double, int> C(0, 0);
    C = B;
    for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 3; ++j){
            ASSERT_EQ(C[i][j], A[i][j]);
        }
    }
}

TEST(MatrixUtils, ZeroFill){
    Matrix<double, int> A{
            {3, 4, 1},
            {2, 2, 7},
            {15, 20, 1},
            {4, 1, 2}
    };

    A.zeroFill();

    for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 3; ++j){
            ASSERT_EQ(0, A[i][j]);
        }
    }
}

TEST(MatrixOps, gemv){
    Matrix<double, int> A{
            {3, 4, 1},
            {2, 2, 7},
            {15, 20, 1},
            {4, 1, 2}
    };

    double x[3] = {1, 2, 0};
    double y[4] = {1, 2, 0, 1};

    A.gemv(2.0, x, 1.0, y);
    ASSERT_DOUBLE_EQ(23, y[0]);
    ASSERT_DOUBLE_EQ(14, y[1]);
    ASSERT_DOUBLE_EQ(110, y[2]);
    ASSERT_DOUBLE_EQ(13, y[3]);
}

TEST(MatrixOps, gemvT){
    Matrix<double, int> A{
            {3, 4, 1},
            {2, 2, 7},
            {15, 20, 1},
            {4, 1, 2}
    };

    double x[4] = {1, 2, 0, 1};
    double y[3] = {1, 2, 0};

    A.gemvT(2.0, x, 1.0, y);
    ASSERT_DOUBLE_EQ(23, y[0]);
    ASSERT_DOUBLE_EQ(20, y[1]);
    ASSERT_DOUBLE_EQ(34, y[2]);

    Matrix<double, int> A2{
        {1, 1, 0, 0},
        {0.95, 1, 1, 1},
        {0.9025, 1, 1.9, 2},
        {.85, 1.0, 2.7075, 3}
    };

    Vector<double, int> r(4);
    r[0] = 0;
    r[1] = 1.95;
    r[2] = 1.2236;
    r[3] = 1.8574;

    Vector<double, int> y2(4);
    y2[0] = 1;
    y2[1] = 2;
    y2[2] = 1;
    y2[3] = 0;

    A2.gemvT(1.0, r.data, 0.0, y2.data);
    //y2.zeroFill();
    //A2.TransposeMultiply(r, y2);
    ASSERT_NEAR(y2[0], 4.5356, 1e-3);
    ASSERT_NEAR(y2[1], 5.0310, 1e-3);
    ASSERT_NEAR(y2[2], 9.3037, 1e-3);
    ASSERT_NEAR(y2[3], 9.9693, 1e-3);
}

/* TEMPORARILY REMOVE BLAS
TEST(MatrixOps, gemm){
    Matrix<double, int> A{
            {3, 4, 1},
            {2, 2, 7},
            {15, 20, 1},
            {4, 1, 2}
    };

    Matrix<double, int> B{
        {3, 2},
        {1, 2},
        {2, 4}
    };

    Matrix<double, int> C(4, 2);
    C.zeroFill();
    A.gemm(CblasNoTrans, CblasNoTrans, 2.0, B, 1.5, C);
    ASSERT_DOUBLE_EQ(30, C[0][0]);
    ASSERT_DOUBLE_EQ(36, C[0][1]);

    ASSERT_DOUBLE_EQ(44, C[1][0]);
    ASSERT_DOUBLE_EQ(72, C[1][1]);

    ASSERT_DOUBLE_EQ(134, C[2][0]);
    ASSERT_DOUBLE_EQ(148, C[2][1]);

    ASSERT_DOUBLE_EQ(34, C[3][0]);
    ASSERT_DOUBLE_EQ(36, C[3][1]);
}
*/

TEST(MatrixMultiply, ATAx){
    using namespace MATH::TENSOR;
    Matrix<double, int> a{
            {3, 4, 1},
            {2, 2, 7},
            {15, 20, 1},
            {4, 1, 2}
    };

    double x[3] = {4, 6, 3};
    double out[3];
    a.ATAx(x, out);

    ASSERT_DOUBLE_EQ(out[0], 3056);
    ASSERT_DOUBLE_EQ(out[1], 3926);
    ASSERT_DOUBLE_EQ(out[2], 565);
}

TEST(MatrixOperation, AddScaled){
    using namespace MATH::TENSOR;
    Matrix<double, int> A{
            {3, 4, 1},
            {2, 2, 7},
            {15, 20, 1},
            {4, 1, 2}
    };

    Matrix<double, int> B{
            {1, 4, 1},
            {2, 2, 7},
            {1, 1, 1},
            {4, 1, 2}
    };

    A.addScaled(0.5, 2, B);

    ASSERT_DOUBLE_EQ(A[0][0], 3.0 / 2 + 1 * 2);
    ASSERT_DOUBLE_EQ(A[0][1], 4.0 / 2 + 4 * 2);
    ASSERT_DOUBLE_EQ(A[0][2], 1.0 / 2 + 1 * 2);

    ASSERT_DOUBLE_EQ(A[1][0], 2.0 / 2 + 2 * 2);
    ASSERT_DOUBLE_EQ(A[1][1], 2.0 / 2 + 2 * 2);
    ASSERT_DOUBLE_EQ(A[1][2], 7.0 / 2 + 7 * 2);

    ASSERT_DOUBLE_EQ(A[2][0], 15.0 / 2 + 1 * 2);
    ASSERT_DOUBLE_EQ(A[2][1], 20.0 / 2 + 1 * 2);
    ASSERT_DOUBLE_EQ(A[2][2], 1.0 / 2 + 1 * 2);

    ASSERT_DOUBLE_EQ(A[3][0], 4.0 / 2 + 4 * 2);
    ASSERT_DOUBLE_EQ(A[3][1], 1.0 / 2 + 1 * 2);
    ASSERT_DOUBLE_EQ(A[3][2], 2.0 / 2 + 2 * 2);
}