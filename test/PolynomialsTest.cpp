#include<Numtool/Polynomials.hpp>
#include<gtest/gtest.h>

TEST(TestPolynomials, TestLagrangeTemplate){
    using namespace POLYNOMIAL;
    // P0
    double val = lagrange1d<double, 0, 0>(-1);
    ASSERT_DOUBLE_EQ(1.0, val);
    val = lagrange1d<double, 0, 0>(0);
    ASSERT_DOUBLE_EQ(1.0, val);
    val = lagrange1d<double, 0, 0>(1);
    ASSERT_DOUBLE_EQ(1.0, val);

    // P1
    val = lagrange1d<double, 1, 0>(-1);
    ASSERT_DOUBLE_EQ(1.0, val);
    val = lagrange1d<double, 1, 0>(1);
    ASSERT_DOUBLE_EQ(0.0, val);

    val = lagrange1d<double, 1, 1>(-1);
    ASSERT_DOUBLE_EQ(0.0, val);
    val = lagrange1d<double, 1, 1>(1);
    ASSERT_DOUBLE_EQ(1.0, val);

    // P2
    val = lagrange1d<double, 2, 0>(-1);
    ASSERT_DOUBLE_EQ(1.0, val);
    val = lagrange1d<double, 2, 0>(0);
    ASSERT_DOUBLE_EQ(0.0, val);
    val = lagrange1d<double, 2, 0>(1);
    ASSERT_DOUBLE_EQ(0.0, val);

    val = lagrange1d<double, 2, 1>(-1);
    ASSERT_DOUBLE_EQ(0.0, val);
    val = lagrange1d<double, 2, 1>(0);
    ASSERT_DOUBLE_EQ(1.0, val);
    val = lagrange1d<double, 2, 1>(1);
    ASSERT_DOUBLE_EQ(0.0, val);

    val = lagrange1d<double, 2, 2>(-1);
    ASSERT_DOUBLE_EQ(0.0, val);
    val = lagrange1d<double, 2, 2>(0);
    ASSERT_DOUBLE_EQ(0.0, val);
    val = lagrange1d<double, 2, 2>(1);
    ASSERT_DOUBLE_EQ(1.0, val);

}


TEST(TestPolynomials, LagrangeDerivativesTest){
    using namespace POLYNOMIAL;
    // P0
    double val = dNlagrange1d<double, 0>(0, 1, 0.0);
    ASSERT_DOUBLE_EQ(0.0, val);
    val = dlagrange1d<double, 0>(0, 0);
    ASSERT_DOUBLE_EQ(0.0, val);

    val = dNlagrange1d<double, 0>(0, 1, 1.0);
    ASSERT_DOUBLE_EQ(0.0, val);

    // P1
    val = dNlagrange1d<double, 1>(0, 1, -1.0);
    ASSERT_DOUBLE_EQ(-0.5, val);
    val = dNlagrange1d<double, 1>(0, 1, 0.0);
    ASSERT_DOUBLE_EQ(-0.5, val);
    val = dNlagrange1d<double, 1>(0, 1, 1.0);
    ASSERT_DOUBLE_EQ(-0.5, val);
    val = dNlagrange1d<double, 1>(1, 1, -1.0);
    ASSERT_DOUBLE_EQ(0.5, val);
    val = dNlagrange1d<double, 1>(1, 1, 0.0);
    ASSERT_DOUBLE_EQ(0.5, val);
    val = dNlagrange1d<double, 1>(1, 1, 1.0);
    ASSERT_DOUBLE_EQ(0.5, val);
    val = dNlagrange1d<double, 1>(0, 2, 0.5);
    ASSERT_DOUBLE_EQ(0.0, val);

    // P2
    auto checkP2d1 = [](int ibasis, double x){
        double val = dNlagrange1d<double, 2>(ibasis, 1, x);
        if(ibasis == 0){
            double act = x - 0.5;
            ASSERT_DOUBLE_EQ(act, val);
        }
        if(ibasis == 1){
            double act = -2 * x;
            ASSERT_DOUBLE_EQ(act, val);
        }
        if(ibasis == 2){
            double act = x + 0.5;
            ASSERT_DOUBLE_EQ(act, val);
        }
    };
    checkP2d1(0, -1.0);
    checkP2d1(0, 0.0);
    checkP2d1(0, 0.5);
    checkP2d1(0, 1.0);
    checkP2d1(1, -1.0);
    checkP2d1(1, 0.0);
    checkP2d1(1, 0.5);
    checkP2d1(1, 1.0);
    checkP2d1(2, -1.0);
    checkP2d1(2, 0.0);
    checkP2d1(2, 0.5);
    checkP2d1(2, 1.0);

    auto checkP2d2 = [](int ibasis, double x){
        double val = dNlagrange1d<double, 2>(ibasis, 2, x);
        if(ibasis == 0){
            ASSERT_DOUBLE_EQ(1.0, val);
        } else if (ibasis == 1){
            ASSERT_DOUBLE_EQ(-2.0, val);
        } else {
            ASSERT_DOUBLE_EQ(1.0, val);
        }
    };

    checkP2d2(0, -1.0);
    checkP2d2(0, 0.0);
    checkP2d2(0, 0.5);
    checkP2d2(0, 1.0);
    checkP2d2(1, -1.0);
    checkP2d2(1, 0.0);
    checkP2d2(1, 0.5);
    checkP2d2(1, 1.0);
    checkP2d2(2, -1.0);
    checkP2d2(2, 0.0);
    checkP2d2(2, 0.5);
    checkP2d2(2, 1.0);

}
// TODO: Derivatives and runtime evaluations/derivatives