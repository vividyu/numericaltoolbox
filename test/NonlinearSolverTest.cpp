#include <Numtool/NonlinearSolvers.hpp>
#include <gtest/gtest.h>

using namespace MATH::NONLINEAR;
class MA580RESIDUAL final : public DenseResidualFunction<double, int, MATH::TENSOR::ORDERING::ROW_MAJOR_ORDER>{
    void calculateResidual(double *x, double *r){
        r[0] = x[0] + x[1] - 2;
        r[1] = x[0] * x[2] + x[1] * x[3];
        r[2] = x[0] * SQUARED(x[2]) + x[1] * SQUARED(x[3]) - 2.0 / 3.0;
        r[3] = x[0] * CUBED(x[2]) + x[1] * CUBED(x[3]);
    }
    int getN(){return 4;}
};

class QuadraticResidual final : public DenseResidualFunction<double, int, MATH::TENSOR::ROW_MAJOR_ORDER> {
    int neq = 10;
    public:
    void calculateResidual(double *x, double *r){
        for(int i = 0; i < neq; i++){
            r[i] = SQUARED(x[i]);
        }
    }
    int getN(){return neq;}
};

TEST(TestNonlinear, TestJacobian){
    MA580RESIDUAL resfunc{};
    double dvar = 0.25;
    double xtest[4] = {0, 0, 0, 0};
    MATH::TENSOR::Matrix<double, int, MATH::TENSOR::ORDERING::ROW_MAJOR_ORDER> jac(4, 4);
    for(int i = 0; i < 10; i++){
        xtest[0] = i * dvar;
        for(int j = 0; j < 10; j++){
            xtest[1] = j * dvar;
            for(int k = 0; k < 10; k++){
                xtest[2] = k * dvar;
                for(int l = 0; l < 10; l++){

                    //std::cout << "ijkl: " << i << ", " << j << ", " << k << ", " << l << "\n";
                    xtest[3] = l * dvar;
                    resfunc.fillJacobian(xtest, jac);
                    double x0 = xtest[0];
                    double x1 = xtest[1];
                    double x2 = xtest[2];
                    double x3 = xtest[3];
                    ASSERT_NEAR(1.0, jac[0][0], 1e-5);
                    ASSERT_NEAR(1.0, jac[0][1], 1e-5);
                    ASSERT_NEAR(0.0, jac[0][2], 1e-5);
                    ASSERT_NEAR(0.0, jac[0][3], 1e-5);

                    ASSERT_NEAR(x2, jac[1][0], 1e-5);
                    ASSERT_NEAR(x3, jac[1][1], 1e-5);
                    ASSERT_NEAR(x0, jac[1][2], 1e-5);
                    ASSERT_NEAR(x1, jac[1][3], 1e-5);

                    ASSERT_NEAR(SQUARED(x2), jac[2][0], 1e-5);
                    ASSERT_NEAR(SQUARED(x3), jac[2][1], 1e-5);
                    ASSERT_NEAR(2 * x0 * x2, jac[2][2], 1e-5);
                    ASSERT_NEAR(2 * x1 * x3, jac[2][3], 1e-5);

                    ASSERT_NEAR(CUBED(x2), jac[3][0], 1e-5);
                    ASSERT_NEAR(CUBED(x3), jac[3][1], 1e-5);
                    ASSERT_NEAR(3 * x0 * SQUARED(x2), jac[3][2], 1e-5);
                    ASSERT_NEAR(3 * x1 * SQUARED(x3), jac[3][3], 1e-5);
                }
            }
        }
    }
}

TEST(TestNonlinear, TestNewton){
    NewtonSolver<double, int, MATH::TENSOR::ORDERING::ROW_MAJOR_ORDER> nlsolver{};
    MA580RESIDUAL resfunc{};

    double x[4] = {1.0, 1.0, -1.0, 0.5};
    //double x[4] = {1.0, 1.0, -0.5774, 0.5774}; 
    std::vector<double> residuals{};
    SolverStatus<int> solstatus = nlsolver.performIterations(30, resfunc, x, residuals);

    std::cout << "# newton iterations: " << solstatus.niter << "\n";
    // std::cout << "residuals:\n";
    // for(int i = 0; i < solstatus.niter; i++){
    //     std::cout << residuals[i] << "\n";
    // }
    ASSERT_EQ(solstatus.convergenceStatus, STATUS::CONVERGENCE_STATUS::CONVERGED);
    ASSERT_EQ(solstatus.errorStatus, STATUS::ERROR_STATE::NO_ERROR);
    ASSERT_EQ(solstatus.niter + 1, residuals.size()); // initial residual doesn't count as an iteration
    ASSERT_NEAR(x[0], 1, 1e-7);
    ASSERT_NEAR(x[1], 1, 1e-7);
    ASSERT_NEAR(x[2], -0.5774, 1e-3);
    ASSERT_NEAR(x[3], 0.5774, 1e-3);

    // Quadratic Function
    double x2[10] = {0};
    for(int i = 0; i < 10; i++){
        x2[i] = (std::sqrt((double) i) / 10.0);
    }
    QuadraticResidual resfunc2{};
    std::vector<double> residuals2{};
    solstatus = nlsolver.performIterations(20, resfunc2, x, residuals2);
    std::cout << "residuals:\n";
    for(int i = 0; i < solstatus.niter; i++){
        std::cout << residuals2[i] << "\n";
    }

    for(int i = 0; i < 10; i++){
        ASSERT_NEAR(x[i], 0, 1e-4);
    }
   
}

TEST(TestNonlinear, TestNewtonMatrixFree){
    MatrixFreeNewtonSolver<double, int, MATH::TENSOR::ORDERING::ROW_MAJOR_ORDER> nlsolver{};
    MA580RESIDUAL resfunc{};

    double x[4] = {1.0, 1.0, -1.0, 0.5};
    //double x[4] = {1.0, 1.0, -0.5774, 0.5774}; 
    std::vector<double> residuals{};
    SolverStatus<int> solstatus = nlsolver.performIterations(30, resfunc, x, residuals);

    std::cout << "# newton iterations: " << solstatus.niter << "\n";
    // std::cout << "residuals:\n";
    // for(int i = 0; i < solstatus.niter; i++){
    //     std::cout << residuals[i] << "\n";
    // }
    ASSERT_EQ(solstatus.convergenceStatus, STATUS::CONVERGENCE_STATUS::CONVERGED);
    ASSERT_EQ(solstatus.errorStatus, STATUS::ERROR_STATE::NO_ERROR);
    ASSERT_EQ(solstatus.niter + 1, residuals.size()); // initial residual doesn't count as an iteration
    ASSERT_NEAR(x[0], 1, 1e-7);
    ASSERT_NEAR(x[1], 1, 1e-7);
    ASSERT_NEAR(x[2], -0.5774, 1e-3);
    ASSERT_NEAR(x[3], 0.5774, 1e-3);
}