
#include <Numtool/MathUtils.hpp>
#include <gtest/gtest.h>
#include <Numtool/TMP/multi_index.hpp>

TEST(TestPolySolvers, TestCubicSolver){
    using namespace MATH;
    double a = 4, b = 5, c = -6;
    double x1, x2, x3;

    cubicSolve<double>(a, b, c, x1, x2, x3);
    ASSERT_NEAR(x1, 0.716188658993105, 0.0000000000001);
    ASSERT_NEAR(x2, -2.35809432949655, 0.0000000000001);
    ASSERT_NEAR(x3, -2.35809432949655, 0.0000000000001);

    a = -6; b = -16; c = -1;
    cubicSolve<double>(a, b, c, x1, x2, x3);
    ASSERT_NEAR(x1, -1.94840994517981, 0.0000000001);
    ASSERT_NEAR(x2, 8.01246501604970, 0.0000000001);
    ASSERT_NEAR(x3, -0.0640550708698870, 0.0000000001);
}

TEST(TestTMP, TestCeil){
    constexpr int c1 = MATH::ceil(1.35);
    ASSERT_EQ(c1, 2);

    constexpr int c2 = MATH::ceil(4002.35);
    ASSERT_EQ(c2, 4003);
}

TEST(TestTMP, TestKroneker){
    constexpr int c1 = MATH::kronecker<0, 0>();
    ASSERT_EQ(c1, 1);

    constexpr int c2 = MATH::kronecker<0, 1>();
    ASSERT_EQ(c2, 0);

    constexpr int c3 = MATH::kronecker<14, 23>();
    ASSERT_EQ(c3, 0);

    constexpr int c4 = MATH::kronecker<25, 25>();
    ASSERT_EQ(c4, 1);
}

TEST(TestTMP, Testpower_T){
    constexpr int c1 = MATH::power_T<0,0>::value;
    ASSERT_EQ(c1 , 1);
    constexpr int c2 = MATH::power_T<0,1>::value;
    ASSERT_EQ(c2 , 0);
    constexpr int c3 = MATH::power_T<2,0>::value;
    ASSERT_EQ(c3 , 1);
    constexpr int c4 = MATH::power_T<3,4>::value;
    ASSERT_EQ(c4 , 81);
}

TEST(TestTMP, TestMinMax){
    constexpr int c1 = MATH::max_i_T<4, 5>();
    ASSERT_EQ(c1, 5);
    constexpr int c2 = MATH::max_i_T<5, 4>();
    ASSERT_EQ(c2, 5);

    constexpr int c3 = MATH::min_i_T<4, 5>();
    ASSERT_EQ(c3, 4);
    constexpr int c4 = MATH::min_i_T<5, 4>();
    ASSERT_EQ(c4, 4);
}

TEST(TestTMP, TestFactorial){
    constexpr int c1 = MATH::factorial<0>();
    ASSERT_EQ(c1, 1);
    constexpr int c2 = MATH::factorial<1>();
    ASSERT_EQ(c2, 1);
    constexpr int c3 = MATH::factorial<3>();
    ASSERT_EQ(c3, 6);
    constexpr int c4 = MATH::factorial<7>();
    ASSERT_EQ(c4, 5040);
}

TEST(TestTMP, TestConsecutiveSum){
    constexpr int c1 = MATH::consecutiveSum<5, 5>();
    ASSERT_EQ(c1, 5);
    constexpr int c2 = MATH::consecutiveSum<2, 5>();
    ASSERT_EQ(c2, 14);
    constexpr int c3 = MATH::consecutiveSum<14, 20>();
    ASSERT_EQ(c3, 119);
}

TEST(TestTMP, TestBinomial){
    constexpr int c1 = MATH::binomial<5,2>();
    ASSERT_EQ(c1, 10);
    constexpr int c2 = MATH::binomial<5,5>();
    ASSERT_EQ(c2, 1);
    constexpr int c3 = MATH::binomial<5,7>();
    ASSERT_EQ(c3, 0);
}

TEST(TestTMP, Testpow_T){
    double a = MATH::pow_T<double, 4>(2.0);
    ASSERT_DOUBLE_EQ( a , 16.0 );
}

TEST(test_midx, test_index_sum){
    using namespace MATH::TMP;

    multi_index<2, 3, 0> mindex{};
    ASSERT_EQ(index_sum(mindex), 5);

    ASSERT_EQ(size(mindex), 3);
    ASSERT_EQ(get<0>(mindex), 2);
}

TEST(test_midx, test_concat){
    using namespace MATH::TMP;
    multi_index<1, 2, 3> mindex1{};
    multi_index<4, 5, 6> mindex2{};

    auto mindex_concat = concat(mindex1, mindex2);
    ASSERT_EQ(get<0>(mindex_concat), 1);
    ASSERT_EQ(get<1>(mindex_concat), 2);
    ASSERT_EQ(get<2>(mindex_concat), 3);
    ASSERT_EQ(get<3>(mindex_concat), 4);
    ASSERT_EQ(get<4>(mindex_concat), 5);
    ASSERT_EQ(get<5>(mindex_concat), 6); 
}

TEST(test_midx, test_sum_limited_increment){
    using namespace MATH::TMP;
    multi_index<2> mindex1{};
    auto mindex1_inc = sum_limited_increment<3>(mindex1);
    ASSERT_EQ(3, get<0>(mindex1_inc));
    ASSERT_EQ(1, size(mindex1_inc));

    ASSERT_EQ(0, size(sum_limited_increment<3>(mindex1_inc)));

    multi_index<0, 0, 0> mindex2{};
    ASSERT_TRUE(equals(mindex2, multi_index<0, 0, 0>{}));

    auto inc1 = sum_limited_increment<3>(mindex2);
    ASSERT_TRUE(equals(inc1, multi_index<0, 0, 1>{}));
    
    auto inc2 = sum_limited_increment<3>(inc1);
    ASSERT_TRUE(equals(inc2, multi_index<0, 0, 2>{}));

    auto inc3 = sum_limited_increment<3>(inc2);
    ASSERT_TRUE(equals(inc3, multi_index<0, 0, 3>{}));

    auto inc4 = sum_limited_increment<3>(inc3);
    ASSERT_TRUE(equals(inc4, multi_index<0, 1, 0>{}));

    auto inc5 = sum_limited_increment<3>(inc4);
    ASSERT_TRUE(equals(inc5, multi_index<0, 1, 1>{}));
    
    auto inc6 = sum_limited_increment<3>(inc5);
    ASSERT_TRUE(equals(inc6, multi_index<0, 1, 2>{})); 

    auto inc7 = sum_limited_increment<3>(inc6);
    ASSERT_TRUE(equals(inc7, multi_index<0, 2, 0>{}));

    auto inc8 = sum_limited_increment<3>(inc7);
    ASSERT_TRUE(equals(inc8, multi_index<0, 2, 1>{}));

    auto inc9 = sum_limited_increment<3>(inc8);
    ASSERT_TRUE(equals(inc9, multi_index<0, 3, 0>{}));

    auto inc10 = sum_limited_increment<3>(inc9);
    ASSERT_TRUE(equals(inc10, multi_index<1, 0, 0>{}));

}