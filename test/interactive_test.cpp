#include <petscsys.h>
#include "optimization.hpp"
#include "GNPetsc.hpp"
#include <iostream>
using namespace OPTIMIZATION;
template<typename T, typename IDX>
class TestCost : public CostFunctionPetsc<T, IDX>{
    private:
    IDX neq;


    public:
    TestCost(IDX neq) : neq(neq) {}

    PetscErrorCode residual(Vec x, Vec f) override {
        PetscReal *xview, *fview;
        PetscInt xstart, xend;
        VecGetArray(x, &xview);
        VecGetArray(f, &fview);

        VecGetOwnershipRange(x, &xstart, &xend);
        for(PetscInt i = 0; i < xstart; i++) fview[i] = 0;
        for(PetscInt i = xstart; i < xend; i++){
            fview[i] = SQUARED(xview[i - xstart]);
        }

        VecRestoreArray(x, &xview);
        VecRestoreArray(f, &fview);
        return 0;
    }

    IDX getN() override { return neq; }
    IDX getM() override { return neq; }
    
};

static char help[] = "PETSc Numtool Interactive test";
int main(int argc, char **argv){

    CHKERRQ(PetscInitialize(&argc, &argv, (char *)0, help));

    int neq = 10;
    TestCost<double, int> costfcn{neq};

    Vec r, x, lambdaI;
    Mat J;
    VecCreateSeq(PETSC_COMM_SELF, neq, &r);
    VecCreateMPI(PETSC_COMM_WORLD, PETSC_DECIDE, neq, &x);
    VecCreateMPI(PETSC_COMM_WORLD, PETSC_DECIDE, neq, &lambdaI);
    MatCreateAIJ(PETSC_COMM_WORLD, 
        PETSC_DECIDE, PETSC_DECIDE, costfcn.getM(), costfcn.getN(),
        0, NULL, 0, NULL, &J
    );
    MatSetOption(J, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE);
    MatSetUp(J);

    PetscReal *xview;
    PetscInt xstart, xend;
    VecGetOwnershipRange(x, &xstart, &xend);
    VecGetArray(x, &xview);
    for(PetscInt i = xstart; i < xend; i++){
        xview[i-xstart] = std::sqrt((double) i) / ((double) neq);
    }
    VecRestoreArray(x, &xview);

    costfcn.formResidualAndJacobian(x, r, J);

    GaussNewtonPetsc<double, int> nl_solver{};
    VecSet(lambdaI, 0.0e-8);
    KSPSetType(nl_solver.ksp, KSPGMRES);
    PC pc;
    KSPGetPC(nl_solver.ksp, &pc);
    PCSetType(pc, PCJACOBI); 
    
    nl_solver.solve(x, lambdaI, costfcn, 20);

    PetscPrintf(PETSC_COMM_WORLD, "============ X ===============\n");
    VecView(x, PETSC_VIEWER_STDOUT_WORLD);

    PetscFinalize();
    return 0;
}