/**
 * @file RandomizedTest.cpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Test for Randomized Methods
 * @version 0.1
 * @date 2021-11-24
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include <gtest/gtest.h>
#include "RandomizedSVD.hpp"
#include "matrix.hpp"

using namespace MATH::TENSOR;

TEST(RANDMETHODS, RANDSVD){
    
    //  Matrix<double, int> A {3, 3, {
    //     {6, 4, 2},
    //     {4, 5, 3},
    //     {2, 3, 5}
    // }};
    // int k = 4;
    // int numthreads = 1;
    // RANDOMIZED::randSVD<double, int, Vector<double, int>, Matrix<double, int>>(A, k, numthreads);

    // Matrix<double, int> A1 {10000, 10000};
    // std::default_random_engine re{};
    // re.seed(std::random_device{}());
    // std::normal_distribution<> dist{0, 1};
    // for(int i = 0; i < 10000; i++){
    //     for(int j = 0; j < 10000; j++){
    //         A1[i][j] = dist(re);
    //     }
    // }
    // k = 50;
    // numthreads = 2; // breaks with > 1 thread rn
    // RANDOMIZED::randSVD<double, int, Vector<double, int>, Matrix<double, int>>(A1, k, numthreads);

}
