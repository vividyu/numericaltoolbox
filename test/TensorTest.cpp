#include <gtest/gtest.h>
#include <Numtool/tensor.hpp>

TEST(TestTensor, TestInit){
    using namespace MATH::TENSOR;
    Tensor<double, 3> T3{4, 5, 2};

    ASSERT_EQ(T3.totalSize(), 40);
    ASSERT_EQ(T3.dimSize(0), 4);
    ASSERT_EQ(T3.dimSize(1), 5);
    ASSERT_EQ(T3.dimSize(2), 2);
}

TEST(TestTensor, TestIndex){
    using namespace MATH::TENSOR;
    Tensor<double, 3> T3{4, 5, 2};

    T3[0][0][0] = 4;
    ASSERT_EQ(T3(0, 0, 0), 4);
    T3[3][4][1] = 16;
    ASSERT_EQ(T3(3, 4, 1), 16);
}

TEST(TestMatrix, TestInit){
    using namespace MATH::TENSOR;
    Matrix<double, int, ORDERING::ROW_MAJOR_ORDER> mat1(3, 4);
    ASSERT_EQ(mat1.getM(), 3);
    ASSERT_EQ(mat1.getN(), 4);

    Matrix<double, int, ROW_MAJOR_ORDER> mat2{{3.0, 4.0, 2.0}, {2.0, 3.1, 5.2}};
    ASSERT_EQ(mat2.getM(), 2);
    ASSERT_EQ(mat2.getN(), 3);
    ASSERT_EQ(mat2[0][0], 3.0);
    ASSERT_EQ(mat2[1][1], 3.1);
    ASSERT_EQ(mat2[1][2], 5.2);

    Matrix<double, int, COLUMN_MAJOR_ORDER> mat3{{3.0, 4.0, 2.0}, {2.0, 3.1, 5.2}};
    ASSERT_EQ(mat3.getM(), 2);
    ASSERT_EQ(mat3.getN(), 3);
    ASSERT_EQ(mat3[0][0], 3.0);
    ASSERT_EQ(mat3[1][1], 3.1);
    ASSERT_EQ(mat3[1][2], 5.2);
}