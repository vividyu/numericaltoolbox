#include "lerp.hpp"
#include "spline.hpp"
#include <fstream>

int main(){

    double xmin = 1.0;
    double dx = 0.5;

    double y1[2] = {1.0, 1.0};
    double y2[2] = {3.0, 3.0};
    double y3[2] = {2.0, 4.5};
    double y4[2] = {1.0, 5.0};
    double y5[2] = {0.5, 4.5};


    MATH::INTERP::Lerp1DES<double, int> lerp{2, xmin, dx};
    MATH::INTERP::Spline1DES<double, int> spline(2, xmin, dx);

    lerp.addData(y1);
    lerp.addData(y2);
    lerp.addData(y3);
    lerp.addData(y4);
    lerp.addData(y5);

    spline.addData(y1);
    spline.addData(y2);
    spline.addData(y3);
    spline.addData(y4);
    spline.addData(y5);
    spline.precompute();



    std::ofstream lerpout{"lerpout.dat"};
    std::ofstream splineout{"spline.dat"};
    double xinc = (lerp.maxInterp() - lerp.minInterp()) / 99;
    double x = xmin;
    for(int i = 0; i < 100; i++){
        double yi[2];
        lerp.interp(x, yi);
        lerpout << x << " " << yi[0] << " " << yi[1] << "\n";
        spline.interp(x, yi);
        splineout << x << " " << yi[0] << " " << yi[1] << "\n";
        x += xinc;
    }
}