#include <gtest/gtest.h>
#include <Numtool/tensor.hpp>
#include <string>
#include <vector>

using namespace MATH::TENSOR_PROD;

struct TestSymbol {
    std::string val = "";

    TestSymbol() = default;
    TestSymbol(const TestSymbol &other) = default;
    TestSymbol(TestSymbol &&other) = default;

    TestSymbol &operator=(const TestSymbol &other) = default;
    TestSymbol &operator=(TestSymbol &&other) = default;


    TestSymbol(std::string val) : val(val) {}

    TestSymbol &operator*=(const TestSymbol &other){
        val = "(" + val + "*" + other.val + ")";
        return *this;
    }

};

TestSymbol operator*(const TestSymbol &val1, const TestSymbol &val2){
    return TestSymbol{"(" + val1.val + "*" + val2.val + ")"};
}

struct TestFunction {
    TestSymbol eval(int Pn, TestSymbol xi){
        return TestSymbol{"fcn" + std::to_string(Pn) + "(" + xi.val + ")"};
    }

    TestSymbol derivative(int Pn, TestSymbol xi){
        return TestSymbol{"df" + std::to_string(Pn) + "(" + xi.val + ")"};
    }

    TestSymbol jthDerivative(int ibasis, int jderivative, TestSymbol xi){
        return TestSymbol{"d" + std::to_string(jderivative) + "f" + std::to_string(ibasis) + "(" + xi.val + ")"};
    }
}; 

TEST(test_tensor_prod, test_linf_tensor_prod_eval){
    TestSymbol a{"0"};
    TestFunction fcn;
    ASSERT_EQ("fcn1(0)", fcn.eval(1, a).val );
    LinfTensorProduct<TestSymbol, TestFunction, 2, 1> p1prod;
    std::vector<TestSymbol> evals(p1prod.cardinality());

    TestSymbol x{"x"};
    TestSymbol y{"y"};
    TestSymbol loc[2] = {TestSymbol{"x"}, TestSymbol{"y"}};

    p1prod.getProduct(loc, evals.data());
    ASSERT_EQ("(fcn0(x)*fcn0(y))", evals[0].val);
    ASSERT_EQ("(fcn0(x)*fcn1(y))", evals[1].val);
    ASSERT_EQ("(fcn1(x)*fcn0(y))", evals[2].val);
    ASSERT_EQ("(fcn1(x)*fcn1(y))", evals[3].val);

    static constexpr int p1card = 4;
    ASSERT_EQ(p1card, p1prod.cardinality());

    TestSymbol **grads = new TestSymbol *[2];
    for(int i = 0; i < p1card; i++){
        grads[i] = new TestSymbol[2];
    }
    p1prod.getGradient(loc, grads);
    ASSERT_EQ("(df0(x)*fcn0(y))", grads[0][0].val);
    ASSERT_EQ("(fcn0(x)*df0(y))", grads[0][1].val);
    ASSERT_EQ("(df0(x)*fcn1(y))", grads[1][0].val);
    ASSERT_EQ("(fcn0(x)*df1(y))", grads[1][1].val);
    ASSERT_EQ("(df1(x)*fcn0(y))", grads[2][0].val);
    ASSERT_EQ("(fcn1(x)*df0(y))", grads[2][1].val);
    ASSERT_EQ("(df1(x)*fcn1(y))", grads[3][0].val);
    ASSERT_EQ("(fcn1(x)*df1(y))", grads[3][1].val);


    for(int i = 0; i < p1card; i++){
        delete[] grads[i];
    }
    delete[] grads;
}

TEST(test_tensor_prod, test_linf_arbitrary_derivative){
    LinfTensorProduct<TestSymbol, TestFunction, 2, 1> p1prod;

    TestSymbol x{"x"};
    TestSymbol y{"y"};
    TestSymbol loc[2] = {TestSymbol{"x"}, TestSymbol{"y"}};

    ASSERT_EQ("(d0f0(y)*d0f0(x))", p1prod.evalArbitraryDerivative(loc, 0, 0, 0, 0).val);
    ASSERT_EQ("(d0f0(y)*d0f1(x))", p1prod.evalArbitraryDerivative(loc, 1, 0, 0, 0).val);
    ASSERT_EQ("(d4f6(y)*d2f1(x))", p1prod.evalArbitraryDerivative(loc, 1, 2, 6, 4).val);
    ASSERT_EQ("(d3f1(y)*d2f3(x))", p1prod.evalArbitraryDerivative(loc, 3, 2, 1, 3).val);
   
}

