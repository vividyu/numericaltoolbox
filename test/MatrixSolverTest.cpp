
#include <gtest/gtest.h>
#include <Numtool/tensor.hpp> 

#include <iostream>
using namespace MATH::TENSOR;
TEST(DirectSolve, LUDecompose){
    Matrix<double, int> A {{
        {3, 1, 2},
        {4, 2, 2},
        {5, 7, 1}
    }};

    PermutationMatrix<int> pi = getPivotPermutation(A);
    LUDecompose<double, int>(A, pi);
    A.prettyPrint(std::cout);

    double b[3]={5,3,9};
    double x[3];
    FWDSub<double, int>(A, pi, b, x);
    backsub<double, int>(A, x, x);
    ASSERT_DOUBLE_EQ(x[0],-6.5);
    ASSERT_DOUBLE_EQ(x[1],4.5);
    ASSERT_DOUBLE_EQ(x[2],10);
    //A.prettyPrint(std::cout);
    pi.apply<double>(A);
    A.prettyPrint(std::cout);
    // ASSERT_DOUBLE_EQ(A[0][0], 5);
    // ASSERT_DOUBLE_EQ(A[0][1], 7);
    // ASSERT_DOUBLE_EQ(A[0][2], 1);

    // ASSERT_DOUBLE_EQ(A[1][0], 0.8);
    // ASSERT_DOUBLE_EQ(A[1][1], -3.6);
    // ASSERT_DOUBLE_EQ(A[1][2], 1.2);

    // ASSERT_DOUBLE_EQ(A[2][0], 0.6);
    // ASSERT_DOUBLE_EQ(A[2][1], 0.88888888888888888889);
    // ASSERT_DOUBLE_EQ(A[2][2], 1.0 / 3.0);
}

TEST(IterativeSolve, cgSolve){
    Matrix<double, int> A {{
        {6, 4, 2},
        {4, 5, 3},
        {2, 3, 5}
    }};
    double b[3]={5,3,9};
    double x[3];
    MATH::TENSOR::KRYLOV::cg<double, int, Matrix<double, int>>(
        A, x, b, 0.0000001, 10
    );
    EXPECT_NEAR(x[0], 1.2727, .0001);
    EXPECT_NEAR(x[1], -1.8636, .0001);
    EXPECT_NEAR(x[2], 2.4091, .0001);

}

TEST(IterativeSolve, bicgstabSolve){
    Matrix<double, int> A {{
        {6, 4, 2},
        {4, 5, 3},
        {2, 3, 5}
    }};
    double b[3]={5,3,9};
    double x[3]={1,1,1};
    MATH::TENSOR::KRYLOV::bicgstab<double, int, Matrix<double, int>>(
        A, x, b, 0.0000001, 3
    );
    EXPECT_NEAR(x[0], 1.2727, .0001);
    EXPECT_NEAR(x[1], -1.8636, .0001);
    EXPECT_NEAR(x[2], 2.4091, .0001);

}

// TEST(IterativeSolve, gmresSolve){
    // Matrix<double, int> A {{
        // {6, 4, 2},
        // {4, 5, 3},
        // {2, 3, 5}
    // }};
    // double b[3]={5,3,9};
    // double x[3] = {1, 1, 1};

    // MATH::TENSOR::KRYLOV::gmres<double, int, Matrix<double, int>>(
        // A, x, b, 0.0000001, 10
    // );
    // EXPECT_NEAR(x[0], 1.2727, .0001);
    // EXPECT_NEAR(x[1], -1.8636, .0001);
    // EXPECT_NEAR(x[2], 2.4091, .0001);

// }