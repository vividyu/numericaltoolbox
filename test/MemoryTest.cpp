#include <Numtool/memory.hpp>
#include <gtest/gtest.h>

TEST(Test_data_pointer, test_resize){
    using namespace MEMORY;

    data_pointer<double> p1{30};
    
    double *p2 = p1.resize(40);

    ASSERT_NE(p2, nullptr);
}