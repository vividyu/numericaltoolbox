# Temporary file to sync notes between my Windows and Ubuntu OS
```
\NumericalToolbox\optimization\alm.hpp  139 //example
\src\solvers\MDGSolver.hpp //add solver
\NumericalToolbox\test\NonlinearOptimizationTest.cpp //nt_test
```
Git lab token
```
glpat-ZhRoAF1Tb3TVKBGXzS7c
```

code example:
```
#include "optimization.hpp"
#include <Numtool/tensor.hpp>
#include <Numtool/tensorUtils.hpp>
#pragma once

namespace OPTIMIZATION {
    template<typename T, typename IDX>
    class PSO {
        T solve(CostFunction<T, IDX> &cost, MATH::TENSOR::Vector<T, IDX> &x, std::vector<T> &residuals){
            using namespace MATH::TENSOR;
            Vector<T, IDX> r(cost.getM());

            Vector<T, IDX> x1(cost.getN());

            cost.eval(x1.data, r.data);
            T objective = r.norm();
        }
    };
}
```

matlab PSO code:
```
%%initialize
w = 0.5;
c1 = 2;
c2 = 2;
max_gen = 100;
p_size = 10;

vMax = 1;
vMin = -1;
xMax = 10;
xMin = -10;

best_val = [];
worst_val = [];
avg_val = zeros(1,max_gen);
pBest = [];
xPoint = [];

%%initialize the particle and velocity
for i = 1:p_size
    x(i,:) = 10*rands(1,2);     %initial x of the particles random(-10,10)
    pBest = x;
    v(i,:) = rands(1,2);        %initial v of the particles random(-1,1)
    %value of function
    f(i) = funPso(x(i,:));      %fi
    avg_val(1) = avg_val(1) + f(i);
end

%%find the values we need
[bestFitness, bestIndex] = min(f);    %the best res
[worstFitness, worstIndex] = max(f);  %the worst res

gBest = x(bestIndex,:);

best_val(1) = bestFitness;
worst_val(1) = worstFitness;
avg_val(1) = avg_val(1)/p_size;

%%loop to generate i-th generation
for i = 2:max_gen

    for j = 1:p_size        %j-th particles, generate x，v
        r = rand(1,2);
        s = rand(1,2);

        v(j,:) = v(j,:) + c1*r.*(pBest(j,:) - x(j,:)) + c2*s.*(gBest - x(j,:));
        v(j,find(v(j,:)>vMax)) = vMax;
        v(j,find(v(j,:)<vMin)) = vMin;

        x(j,:) = x(j,:) + v(j,:);
        x(j,find(x(j,:)>xMax)) = xMax;
        x(j,find(x(j,:)<xMin)) = xMin;

    end
    xPoint(end+1) = x(j);
    for j = 1:p_size        %generate fi, pBest, gBest
        temp = funPso(x(j,:));
        if temp < f(j)
            f(j) = temp;
            pBest(j,:) = x(j,:);
        end
        avg_val(i) = avg_val(i) + f(j);
    end

    [bestFitness, bestIndex] = min(f);    %the best res
    [worstFitness, worstIndex] = max(f);  %the worst res

    gBest = x(bestIndex,:);

    best_val(i) = bestFitness;
    worst_val(i) = worstFitness;
    avg_val(i) = avg_val(i)/p_size;

end

%% plot
plot(best_val,'-o');
title("Best Values");

plot(worst_val,'-o');
title("Worst Values");

plot(avg_val,'-o');
title("Average Values");
```
# Some references collected from Internet

## Galerkin method explaination in English
https://www.youtube.com/watch?v=tK1HbAiCPuw&list=PLc4eVvgECLtptmgobxxXmVikejqPf4f8D&index=1&t=0s

## Galerkin method explaination in Chinese
https://zhuanlan.zhihu.com/p/161372939

### Introduction to Discontinuous Galerkin Methods in Chinese
https://www.bilibili.com/video/BV1GP4y1x7hV
https://www.bilibili.com/video/BV12P4y1x7B1
https://www.bilibili.com/video/BV1qq4y1Q7F9
https://www.bilibili.com/video/BV1mq4y1Q7xC
https://www.bilibili.com/video/BV14q4y1M7A9
https://www.bilibili.com/video/BV1n44y1C7gY

## High-order method
https://numath.dmae.upm.es/research/high-order-methods/#:~:text=High%20order%20methods%20are%20the,traditional%20or%20low%20order%20methods.

## Levenberg-Marquardt nonlinear least squares algorithms in C/C++
http://users.ics.forth.gr/~lourakis/levmar/