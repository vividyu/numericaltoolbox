/**
 * @file Interpolator.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief 
 * @version 0.1
 * @date 2022-04-09
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#pragma once
namespace MATH::INTERP{

    template<typename T, typename IDX>
    class Interpolator1D {
        public:

        /**
         * @brief Perform any necessary precomputation
         * if defined, must be called before interp
         */
        virtual void precompute(){}

        /**
         * @brief The number of outputs
         * 
         * @return int the number of tables interpolated over the domain
         */
        virtual int nOutputs() = 0;

        /**
         * @brief The minimum value in the domain represented by the table
         * 
         * @return T the minimum domain value
         */
        virtual T minInterp() = 0;

        /**
         * @brief The maximum value in the domain represented by the table
         * 
         * @return T the minimum domain value
         */
        virtual T maxInterp() = 0;

        /**
         * @brief Interpolate a value
         * 
         * @param [in] x the domain value
         * @param [out] y interpolated data (size = nOutputs)
         */
        virtual void interp(T x, T *y) = 0;

        /**
         * @brief Add a data entry
         * interface for interpolators to take data line by line
         * 
         * @param y_i the interpolation values
         */
        virtual void addData(T *y_i) = 0;
    };
}
