
#pragma once
#include <Numtool/Interpolation/Interpolator.hpp>
#include <array>
namespace MATH::INTERP{
    /**
     * @brief Evenly Spaced Domain 1D Cubic Spline Interpolator
     * 
     * @tparam T the floating point type
     * @tparam IDX the index type
     */
    template<typename T, typename IDX>
    class Spline1DES final : public Interpolator1D<T, IDX> {
        private:
        int ny;
        T xmin;
        T dx;
        std::vector< std::vector<T> > ydata;
        std::vector< std::vector<T> > y2coeff;
        
        inline T xv(IDX i){
            return xmin + i * dx;
        }

        public:

        Spline1DES(int ny, T xmin, T dx) 
        : ny(ny), xmin(xmin), dx(dx),
          ydata(std::vector<std::vector<T>>(ny)),
          y2coeff(std::vector<std::vector<T>>(ny)) {}

        /**
         * @brief Add one set of output 
         * abscisse: current maxInterp() + dx
         * 
         * @param data the data to add for each output (size nOutputs())
         */
        void addData(T *data){
            for(int i = 0; i < ny; i++){
                ydata[i].push_back(data[i]);
            }
        }

        void addDataToIdx(int idx, T data){
            ydata[idx].push_back(data);
        }

        /**
         * @brief precompute the second derivative coefficients
         * Numerical Recipies (Press et al.)
         * 
         * @param yp1 the derivative at the left point for each dataset
         *            greater than .99e99 uses natural bc
         * @param ypn the derivative at the right point for each dataset
         *            greater than .99e99 uses natural bc
         */
        void precompute(T *yp1, T *ypn){
            IDX i, k;
            T p, qn, sig, un;
            IDX n = ydata[0].size();
            T *u = new T[n - 1];
            for(int iy = 0; iy < ny; iy++){
                y2coeff[iy].resize(n);
                const std::vector<T> yv = ydata[iy];
                if(yp1[iy] > 0.99e99){
                    y2coeff[iy][0] = u[0] = 0.0;
                } else {
                    y2coeff[iy][0] = -0.5;
                    u[0] = (3.0/(xv(1)-xv(0)))*((yv[1]-yv[0])/(xv(1)-xv(0))-yp1[iy]);
                }
                for(i = 1; i < n-1; i++) {
                    sig = (xv(i)-xv(i-1))/(xv(i+1)-xv(i-1));
                    p = sig*y2coeff[iy][i-1]+2.0;
                    y2coeff[iy][i]=(sig-1.0)/p;
                    u[i]=(yv[i+1]-yv[i])/(xv(i+1)-xv(i)) - (yv[i]-yv[i-1])/(xv(i)-xv(i-1));
                    u[i]=(6.0*u[i]/(xv(i+1)-xv(i-1))-sig*u[i-1])/p;
                } 
                if(ypn[iy] > 0.99e99){
                    qn = un = 0.0;
                } else {
                    qn = 0.5;
                    un=(3.0/(xv(n-1)-xv(n-2)))*(ypn[iy]-(yv[n-1]-yv[n-2])/(xv(n-1)-xv(n-2)));
                }
                y2coeff[iy][n-1]=(un-qn*u[n-2])/(qn*y2coeff[iy][n-2]+1.0);
                for(k=n-2; k>=0; k--) y2coeff[iy][k] = y2coeff[iy][k]*y2coeff[iy][k+1]+u[k];
            }
            delete[] u;
        }

        /**
         * @brief Precompute the 2nd derivative coefficients with natural bc
         * 
         */
        void precompute() override{
            T *ypnatural = new T[ny];
            for(int i = 0; i < ny; i++) ypnatural[i] = 1e99;
            precompute(ypnatural, ypnatural);
        }

        /**
         * @brief The number of outputs
         * 
         * @return int the number of tables interpolated over the domain
         */
        int nOutputs() { return ny; }

        /**
         * @brief The minimum value in the domain represented by the table
         * 
         * @return T the minimum domain value
         */
        T minInterp() { return xmin; };

        /**
         * @brief The maximum value in the domain represented by the table
         * 
         * @return T the minimum domain value
         */
        T maxInterp() { return xmin + (ydata[0].size() - 1) * dx; };

        /**
         * @brief Interpolate a value
         * WARNING: do not use unless precomputation done
         * @param [in] x the domain value
         * @param [out] y interpolated data (size = nOutputs)
         */
        void interp(T x, T *y){
            IDX klo = (IDX) ((x - xmin) / dx);
            IDX khi=klo+1;
            T h, b, a; 
            h = dx;
            a = (xv(khi)-x)/h;
            b = (x-xv(klo))/h;
            for(int iy = 0; iy < ny; iy++){
                y[iy] = a * ydata[iy][klo]
                    + b * ydata[iy][khi]
                    + (
                        (a*a*a-a)*y2coeff[iy][klo]
                        + (b*b*b-b)*y2coeff[iy][khi]
                    ) * (h*h)/6.0;
            }
        }
    };
}