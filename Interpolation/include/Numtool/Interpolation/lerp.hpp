/**
 * @file lerp.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Linear Interpolation
 * @version 0.1
 * @date 2022-04-09
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#pragma once
#include <Numtool/Interpolation/Interpolator.hpp>
#include <vector>
namespace MATH::INTERP {

    /**
     * @brief Evenly Spaced Domain 1D Linear Interpolator
     * 
     * @tparam T the floating point type
     * @tparam IDX the index type
     */
    template<typename T, typename IDX>
    class Lerp1DES final : public Interpolator1D<T, IDX> {
        private:
        int ny;
        T xmin;
        T dx;
        std::vector< std::vector<T> > ydata;
        

        public:

        Lerp1DES(int ny, T xmin, T dx) 
        : ny(ny), xmin(xmin), dx(dx),
          ydata(std::vector<std::vector<T>>(ny)) {}

        /**
         * @brief Add one set of output 
         * abscisse: current maxInterp() + dx
         * 
         * @param data the data to add for each output (size nOutputs())
         */
        void addData(T *data){
            for(int i = 0; i < ny; i++){
                ydata[i].push_back(data[i]);
            }
        }

        void addDataToIdx(int idx, T data){
            ydata[idx].push_back(data);
        }

        /**
         * @brief The number of outputs
         * 
         * @return int the number of tables interpolated over the domain
         */
        int nOutputs() { return ny; }

        /**
         * @brief The minimum value in the domain represented by the table
         * 
         * @return T the minimum domain value
         */
        T minInterp() { return xmin; };

        /**
         * @brief The maximum value in the domain represented by the table
         * 
         * @return T the minimum domain value
         */
        T maxInterp() { return xmin + (ydata[0].size() - 1) * dx; };

        /**
         * @brief Interpolate a value
         * 
         * @param [in] x the domain value
         * @param [out] y interpolated data (size = nOutputs)
         */
        void interp(T x, T *y){
            IDX iLow = (IDX) ((x - xmin) / dx);
            T xLow = xmin + dx * iLow;
            T xfrac = (x - xLow) / dx;
            for(int i = 0; i < ny; i++){
                T yLow = ydata[i][iLow];
                T dy = ydata[i][iLow + 1] - yLow;
                y[i] = yLow + xfrac * dy;
            }
        }
    };

}