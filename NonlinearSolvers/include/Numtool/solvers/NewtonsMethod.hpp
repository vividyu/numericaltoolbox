/**
 * @file NewtonsMethod.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Nonlinear solver
 * @version 0.1
 * @date 2022-02-06
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#pragma once
#include "functional"
#include <cmath>
namespace MATH::NONLINEAR 
{

    /**
     * @brief Single input single output nonlinear function
     * 
     * @tparam T the floating point type
     */
    template<typename T>
    using nlfunc = std::function<T(T)>;

    template<typename T>
    T NewtonsMethod(T x0, T EPSILON, int kmax, nlfunc<T> &func, bool &converged){
        converged = false;
        T x = x0;
        T r0 = func(x0);
        for(int i = 0; i < kmax; i++){
            T r = func(x);
            if(abs(r) < EPSILON * abs(r0)){
                converged = true;
                return x;
            }
            T xph = x + EPSILON;
            T rph = func(xph);
            T rprime = (rph - r) / EPSILON;
            x -= r / (rprime);
        }
        return x;
    }
    

}