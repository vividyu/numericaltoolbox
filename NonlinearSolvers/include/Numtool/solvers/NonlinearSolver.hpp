/**
 * @file NonlinearSolver.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Nonlinear Solver definition
 * @version 0.1
 * @date 2022-02-10
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#pragma once
#include <Numtool/tensor.hpp>
#include <Numtool/tensorUtils.hpp>
namespace MATH::NONLINEAR{

    template<typename T, typename IDX, MATH::TENSOR::ORDERING matrix_ordering>
    class DenseResidualFunction{
        private:
            static constexpr T EPSILON = 1e-8;

        public:
        virtual void calculateResidual(T *x, T *r) = 0;

        virtual IDX getN() = 0;

        virtual void fillJacobian(T *x, MATH::TENSOR::Matrix<T, IDX, matrix_ordering> &jac){
            T *r = new T[getN()]; // todo: think about extracting to class variable
            T *r_peturb = new T[getN()];
            calculateResidual(x, r);
            T epsilon = std::max(EPSILON, MATH::TENSOR::norm<T, IDX, 2>(r, getN()) * EPSILON); // scale by norm
            //T epsilon = EPSILON;
            for(IDX j = 0; j < getN(); j++){
                T temp = x[j];
                x[j] += epsilon;
                calculateResidual(x, r_peturb);
                for(IDX i = 0; i < getN(); i++){
                    jac[i][j] = (r_peturb[i] - r[i]) / epsilon;
                }
                x[j] = temp;
            }
            delete[] r;
            delete[] r_peturb;
        }
    };

    template<typename T, typename resfunc>
    concept ResidualFunction = requires(resfunc &func, T *x, T *r){
        {func.calculateResidual(x, r)};
        {func.getN()} -> std::convertible_to<int>;
    };

    template<typename T, typename IDX, MATH::TENSOR::ORDERING ordering>
    class MatrixFreeJacobian {
        private:
            T *x;
            DenseResidualFunction<T, IDX, ordering> *func;
            IDX n;
            T epsilon;
        public:

        /**
         * @brief Construct a new Matrix Free Jacobian object
         * 
         * @param x the location to evaluate the jacobian at
         */
        MatrixFreeJacobian(T *x, DenseResidualFunction<T, IDX, ordering> *func, T epsilon) 
        : x(x), func(func), n(func->getN()), epsilon(epsilon) {}

        void mult(const T *v, T *out) const {
            T h = epsilon * MATH::TENSOR::norm<T, IDX, 2>(x, n) / MATH::TENSOR::norm<T, IDX, 2>(v, n); 
            T *r = new T[n];
            T *xph = new T[n];
            memcpy(xph, x, n * sizeof(T));
            for(IDX i = 0; i < n; i++) xph[i] += epsilon * v[i];
            func->calculateResidual(xph, out);
            func->calculateResidual(x, r);
            for(IDX i = 0; i < n; i++) out[i] = (out[i] - r[i]) / epsilon;
            delete[] r;
            delete[] xph;
        }

        IDX getM() const { return n; }
        IDX getN() const { return n; }
    };

    namespace STATUS {
        /**
         * @brief Convergence status
         * can be used in an if statement
         * if(CONVERGENCE_STATUS) gives true if converged
         */
        enum CONVERGENCE_STATUS {
            NOT_CONVERGED = 0,
            CONVERGED = 1
        };

        /**
         * @brief Error states
         * if(ERROR_STATE) will be true if there is an error state
         */
        enum ERROR_STATE {
            NO_ERROR = 0,
            NAN_ERROR = 1,
        };
    }

    template<typename IDX>
    struct SolverStatus {
        STATUS::CONVERGENCE_STATUS convergenceStatus;
        STATUS::ERROR_STATE errorStatus;
        IDX niter; /// the number of iterations performed
    };

    template<typename T, typename IDX, MATH::TENSOR::ORDERING matrix_ordering>
    class DenseNonlinearSolver{

        virtual
        SolverStatus<IDX> performIterations(
            IDX kmax,
            DenseResidualFunction<T, IDX, matrix_ordering> &residualFunc,
            T *x,
            std::vector<T> &residuals    
        ) = 0;
    };

}