/**
 * @file NonlinearSolvers.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Includes for Nonlinear Solvers
 * @version 0.1
 * @date 2022-05-06
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#pragma once
#include <Numtool/solvers/NonlinearSolver.hpp>
#include <Numtool/solvers/NewtonSystem.hpp>
#include <Numtool/solvers/NewtonsMethod.hpp>