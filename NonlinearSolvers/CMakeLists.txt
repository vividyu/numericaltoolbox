cmake_minimum_required(VERSION 3.14)

project(nt_nonlinear VERSION 1.0.0 LANGUAGES CXX)

add_library(nt_nonlinear INTERFACE)
target_include_directories(nt_nonlinear 
    INTERFACE $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)
target_link_libraries(nt_nonlinear INTERFACE nt_tensor)

target_compile_features(nt_nonlinear INTERFACE cxx_std_17)
