/**
 * @file multi_index.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief A compile-time multi-index
 * @version 0.1
 * @date 2022-08-30
 * 
 * @copyright Copyright (c) 2022
 * 
 */

/**
 * @brief Template metaprogramming namespace
 * 
 */
namespace MATH::TMP {

    /**
     * @brief A multi index that reaches a maximum sum
     * @tparam indices the indices
     */
    template<int... indices>
    struct multi_index{};

    /**
     * @brief Concatenate two multi_indexes
     * 
     * @tparam idxs1 the template indices of the first multi_index
     * @tparam idxs2 the template indices of the second multi_index
     * @param midx1 the first multi_index
     * @param midx2 the second multi_index
     * @return multi_index<idxs1..., idxs2...> a concatenation of the two multi_indices
     */
    template<int... idxs1, int... idxs2>
    multi_index<idxs1..., idxs2...> concat(const multi_index<idxs1...> &midx1, const multi_index<idxs2...> &midx2){
        return multi_index<idxs1..., idxs2...>{};
    }

    /**
     * @brief Helper for get() on a multi_index
     * 
     * @tparam loc the location
     * @tparam first the first index
     * @tparam idxs the indices
     * @return constexpr int the index to get
     */
    template<int loc, int first, int... idxs>
    constexpr int get_(){
        if constexpr (loc == 0){ return first; }
        else { return get_<loc - 1, idxs...>(); }
    }

    /**
     * @brief get the value at a given index
     * 
     * @tparam loc the index in the multi_index
     * @tparam idxs the indices
     * @param midx the multi_index
     * @return constexpr int the loc-th element of the multi_index
     */
    template<int loc, int... idxs>
    constexpr int get(const multi_index<idxs...> &midx){
        return get_<loc, idxs...>();
    }

    /**
     * @brief get the number of elements in a multi_index
     * 
     * @tparam idxs the indices
     * @param mindex the multi_index
     * @return constexpr int the size of the multi_index
     */
    template<int... idxs>
    constexpr int size(const multi_index<idxs...> &mindex){ return sizeof...(idxs); }

    /**
     * @brief Get the sum of the indices in a multi_index
     * 
     * @tparam first the first index
     * @tparam indices the remainder of the indices 
     * @param mindex the multi-index
     * @return constexpr int the sum
     */
    template<int first, int... indices>
    constexpr int index_sum(const multi_index<first, indices...> &mindex){
        return first + index_sum<indices...>(multi_index<indices...>{});   
    }

    template<int first>
    constexpr int index_sum(const multi_index<first> &mindex){ return first; }

    /**
     * @brief generate the next in the sequence of multi indices where the sum of the indices cannot exceed sum_limit
     * or an empty multi_index if the given multi_index would be the last in the sequence
     * @tparam sum_limit the maximum index sum
     * @tparam first the first index
     * @tparam second the second index
     * @tparam idxs the indices
     * @param mindex the multi_index
     * @return auto the next multi_index in the sequence
     */
    template<int sum_limit, int first, int second, int... idxs>
    auto sum_limited_increment(multi_index<first, second, idxs...> mindex){
        auto inc_tail = sum_limited_increment<sum_limit - first, second, idxs...>(multi_index<second, idxs...>{});
        if constexpr(size(inc_tail) > 0){
            return concat(multi_index<first>{}, inc_tail);
        } else if constexpr(index_sum(multi_index<first, 0, idxs...>{}) < sum_limit) {
            return multi_index<first + 1, 0, idxs...>{};
        } else {
            return multi_index<>{};
        }
    }

    template<int sum_limit, int idx>
    auto sum_limited_increment(const multi_index<idx> &mindex){
        if constexpr (idx < sum_limit){ return multi_index<idx + 1>{}; }
        else{ return multi_index<>{}; }
    }

    constexpr bool equals(const multi_index<> &mindex1, const multi_index<> &mindex2){ return true; }

    /**
     * @brief Compare two multi indices for equality in all the indices
     * 
     * @tparam first1 the first element of the first multi index
     * @tparam idxs1 the indices of the first multi index
     * @tparam first2 the first index of the second multi_index
     * @tparam idxs2 the indices of the second multi_index
     * @param mindex1 the first multi_index
     * @param mindex2 the second multi_index
     * @return true if all indices are the same
     * @return false otherwise
     */
    template<int first1, int... idxs1, int first2, int... idxs2>
    constexpr bool equals(const multi_index<first1, idxs1...> &mindex1, const multi_index<first2, idxs2...> &mindex2){
        if constexpr (sizeof...(idxs1) == sizeof...(idxs2)){
            return (first1 == first2) && equals(multi_index<idxs1...>{}, multi_index<idxs2...>{});
        } else {
            return false;
        }
    }

    /**
     * @brief print a multi_index
     * 
     * @tparam idxs the indices
     * @param mindex the multi_index
     */
    template<int... idxs>
    void print(multi_index<idxs...> mindex){
        std::cout << "Multi Index of size " << size(mindex) << ": ";
        ((std::cout << idxs << ", "), ...);
        std::cout << "\n";
    }

}