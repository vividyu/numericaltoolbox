/**
 * @file MathTypes.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Math Types
 * @version 0.1
 * @date 2021-05-26
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#pragma once
#include <vector>
#include <algorithm>
#include <type_traits>
#include <cstddef>
#include <cmath>
#include <limits>
#include <functional>

/**
 * @brief Namespace for math related definitions and functions
 * 
 */
namespace MATH {
   typedef std::vector< std::vector< std::vector< double > > > vecd3;
   typedef std::vector< std::vector< double > > vecd2;
   typedef std::vector< double > vecd1;
   
   typedef std::vector< std::vector< std::vector< int > > > veci3;
   typedef std::vector< std::vector< int> > veci2;
   typedef std::vector< int > veci1;

   struct Itriple {
      int i1, i2, i3;
   };

   // zero
   template<class T>
   static const T ZERO = { 0.00000000000000000000000000000000000000000000000000000000000000000000L };

   template<class T>
   static const T ONE_HALF = {1.0 / 2.0};

   template<class T>
   static const T ONE_THIRD = {1.0 / 3.0};

   template<class T>
   static const T ONE_FOURTH = {1.0 / 4.0};

   template<class T>
   static const T ONE_FIFTH = {1.0 / 5.0};

   template<class T>
   static const T ONE_SIXTH = {1.0 / 6.0};

   template<class T>
   static const T TWO_THIRDS = {2.0 / 3.0};

   template<class T>
   static const T TWO_SIXTHS = {2.0 / 6.0};

   template<class T>
   static const T TWO = {2.000000000000000000000000000000000000000000000000000L};
   template<class T>
   static const T SQRT_TWO = {std::sqrt(TWO<T>)};

   // inv square roots
   template<class T>
   static const T INV_SQRT_3 = {1.0 / std::sqrt(3)};

    /**
     * @brief Determine if an entry is effectively zero
     * uses 1 std::numeric_limits epsilon
     */
    template<class T>
    inline bool isTiny(T arg){
        return std::abs(arg) <= 4 * std::numeric_limits<T>::epsilon();
    }
}