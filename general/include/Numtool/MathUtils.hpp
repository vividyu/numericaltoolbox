#pragma once

// Include C standard Math functions
#include <cmath>

// Efficient Power Defintions
#define SQUARED(X) ((X) * (X))
#define CUBED(X) ((X) * (X) * (X))

// NumericalToolbox includes
#include <Numtool/MathTypes.hpp>
#include <Numtool/TMPDefs.hpp>
#include <Numtool/matrixT.hpp>

namespace MATH {
    
    /**
     * @brief Quadratic solve from Numerical Recipies
     * 
     * @tparam T the floating point type
     * @param [in] a the coefficient for x^2
     * @param [in] b the coefficient for x
     * @param [in] c the constant coefficient
     * @param [out] x1 the first root
     * @param [out] x2 the second root
     */
    template<typename T>
    void quadraticSolve(T a, T b, T c, T &x1, T &x2){
        T q = -ONE_HALF<T> * (b + 
            copysign(
                std::sqrt(SQUARED(b) - 4.0 * a * c),
                b
            )
        );
        x1 = q / a;
        x2 = c / q;
    }

    /**
     * @brief Cubic solve from numerical recipies (Press et al)
     * solves x^3 + ax^2 + bx + c = 0
     * 
     * If two roots are complex, then x2, and x3 are the real portion of the roots
     * @tparam T 
     * @param a the coefficient for x^2
     * @param b the coefficient for x
     * @param c the constant coefficient
     * @param [out] x1 The first root
     * @param [out] x2 The second root
     * @param [out] x3 The third root
     */
    template<typename T>
    void cubicSolve(T a, T b, T c, T &x1, T &x2, T &x3){
        static constexpr T ONE_NINTH = {1.0 / 9.0000000000000000000000000000000000000000000000000000000000L};
        static constexpr T ONE_54 = {1.0 / 54.0000000000000000000000000000000000000000000000000000000000L};
        T Q = (SQUARED(a) - 3 * b) * ONE_NINTH;
        T R = (2 * CUBED(a) - 9 * a * b + 27 * c) * ONE_54;
        T Qcubed = CUBED(Q);
        if(SQUARED(R) < Qcubed){
            T theta = std::acos(R / std::sqrt(Qcubed));
            x1 = -2*sqrt(Q) * cos(ONE_THIRD<T> * theta) - ONE_THIRD<T> * a;
            x2 = -2*sqrt(Q) * cos(ONE_THIRD<T> * (theta + 2 * M_PI)) - ONE_THIRD<T> * a;
            x3 = -2*sqrt(Q) * cos(ONE_THIRD<T> * (theta - 2 * M_PI)) - ONE_THIRD<T> * a;
        } else {
            T A, B;
            A = std::cbrt(std::abs(R) + std::sqrt(SQUARED(R) - Qcubed));
            A = -std::copysign(A, R);
            if(A == 0){
                B = 0;
            } else {
                B = Q / A;
            }
            x1 = (A + B) - ONE_THIRD<T> * a;
            x2 = -0.5 * (A + B) - ONE_THIRD<T> * a; // real component only
            x3 = -0.5 * (A + B) - ONE_THIRD<T> * a; // real component only
        }
    }

    /**
     * @brief Takes a to the power of an integer b in O(log(b)) multiplications
     * 
     * @tparam T the floating point type
     * @param a the base
     * @param b the exponent (unsigned integer!)
     */
    template<typename T>
    T integer_pow(T a, unsigned int b){
        T ret = 1;
        while (b > 0){
            if(b % 2 == 1) ret *= a;
            b = b >> 1; // divide by 2
            a *= a;
        }
        return ret;
    }

    /**
     * @brief takes an integer a to the power of positive integer b
     * using exponentiation by squaring
     * 
     * @param a the base
     * @param b the exponent
     * @return int a^b
     */
    inline int integer_pow(int a, int b){
        int ret = 1;
        while(b > 0){
            if(b & 1) ret *= a;
            b >>= 1;
            a *= a;
        }
        return ret;
    }

    /**
     * @brief get the average of two numbers
     * 
     * @tparam T the floating point type
     * @param a the first number
     * @param b the second number
     * @return T the average
     */
    template<typename T>
    T avg(T a, T b){
        return 0.5 * (a + b);
    }

}