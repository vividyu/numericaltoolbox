/**
 * @file matrixT.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Template Optimized Matrix and vector Methods
 * @version 0.1
 * @date 2021-06-03
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#pragma once
#include <Numtool/MathTypes.hpp>

namespace MATH{
/**
 * @brief Template optimized Matrix methods
 * 
 */
namespace MATRIX_T {

    /**
     * @brief Get the dot product of two vectors
     * 
     * @tparam vecsize the size of the vectors
     * @param a the first vector
     * @param b the second vector
     * @return double the dot product
     */
    template<typename T, std::size_t vecsize>
    inline double dotprod(const T *a, const T *b){
        T prod = 0;
        for(int i = 0; i < vecsize; i++){
            prod += a[i] * b[i];
        }
        return prod;
    }

    /**
     * @brief Multiply a scalar and a vector
     * 
     * @tparam vecsize the size of the vector
     * @param a the scalar
     * @param v the vector
     * @param result the result of the multiplication
     */
    template<typename T, std::size_t vecsize>
    inline void scalarProd(const T a, const T *v, T *result){
        for(int i = 0; i < vecsize; i++){
            result[i] = a * v[i];
        }
    }

    /**
     * @brief Project vectors
     * 
     * @tparam vecsize the size of the vector
     * @param a the first vector a
     * @param b the vector a will be projected onto
     * @param vp the resulting projection
     */
    template<typename T, std::size_t vecsize>
    inline void project(const T *a, const T *b, T *vp){
        T scale = dotprod<T, vecsize>(a, b) / dotprod<T, vecsize>(b, b);
        for(int i = 0; i < vecsize; i++){
            vp[i] = scale * b[i];
        }
    }

    /**
     * @brief Gets the vector pointing form a to b (b - a)
     * 
     * @tparam vecsize the size of the vectors
     * @param a the first vector
     * @param b the second vector
     * @param r the distance
     */
    template<typename T, std::size_t vecsize>
    inline void subtract(const T *a, const T *b, T *r){
        for(int i = 0; i < vecsize; i++){
            r[i] = b[i] - a[i];
        }
    }

    /**
     * @brief Adds the two vectors
     * 
     * @tparam vecsize the size of the vectors
     * @param a the first vector
     * @param b the second vector
     * @param sum the sum
     */
    template<typename T, std::size_t vecsize>
    inline void add(const T *a, const T *b, T *sum){
        for(int i = 0; i < vecsize; i++){
            sum[i] = b[i] + a[i];
        }
    }

    /**
     * @brief Adds the two vectors
     * 
     * @tparam vecsize the size of the vectors
     * @param a the first vector
     * @param b the second vector
     * @param sum the sum
     */
    template<typename T,std::size_t vecsize>
    inline void add(const double *a, const double *b, double *sum){
        for(int i = 0; i < vecsize; i++){
            sum[i] = b[i] + a[i];
        }
    }

    /**
     * @brief Gets the adjugate of A for 1x1, 2x2, or 3x3 matrix
     * 
     * @tparam matsize the matrix size
     * @tparam T the floating point type
     * @param A the matrix
     * @param adj [out] the adjugate (must be presized)
     */
    template<std::size_t matsize, typename T>
    void adjugate(const T *Ain, T *adjin){
        T (* A)[matsize] = (T (*)[matsize])Ain;
        T (* adj)[matsize] = (T (*)[matsize])adjin;
        if constexpr (matsize == 1){
            adj[0][0] = 1;
        } else if constexpr(matsize == 2){
            adj[0][0] = A[1][1];
            adj[0][1] = -A[0][1];

            adj[1][0] = -A[1][0];
            adj[1][1] = A[0][0];
        } else {
            adj[0][0] = A[1][1] * A[2][2] - A[1][2] * A[2][1];
            adj[0][1] = A[0][2] * A[2][1] - A[0][1] * A[2][2];
            adj[0][2] = A[0][1] * A[1][2] - A[1][1] * A[0][2];

            adj[1][0] = A[1][2] * A[2][0] - A[1][0] * A[2][2];
            adj[1][1] = A[0][0] * A[2][2] - A[0][2] * A[2][0];
            adj[1][0] = A[0][1] * A[2][0] - A[0][0] * A[2][1];

            adj[2][0] = A[0][1] * A[1][2] - A[0][2] * A[1][1];
            adj[2][1] = A[0][1] * A[2][0] - A[0][0] * A[2][1];
            adj[2][2] = A[0][0] * A[1][1] - A[0][1] * A[1][0];
        }
    }

    template<std::size_t matsize>
    double determinant(double *Ain){
        double (* A)[matsize] = (double (*)[matsize])Ain;
        if constexpr (matsize == 1){
            return A[0][0];
        } else if constexpr(matsize == 2){
            return A[0][0] * A[1][1] - A[1][0] * A[0][1];
        } else if constexpr(matsize == 3){
            return A[0][0] * A[1][1] * A[2][2] +//aei
                    A[0][1] * A[1][2] * A[2][0] +//bfg
                    A[0][2] * A[1][0] * A[2][1] -//cdh
                    A[0][2] * A[1][1] * A[2][0] -//ceg
                    A[0][1] * A[1][0] * A[2][2] -//bdi
                    A[0][0] * A[1][2] * A[2][1]; //afh
        } else {
            return std::nan("0"); //
        }
    }

    template<std::size_t matsize, typename T>
    T determinant(T *Ain){
       T (* A)[matsize] = (T (*)[matsize])Ain;
        if constexpr (matsize == 1){
            return A[0][0];
        } else if constexpr(matsize == 2){
            return A[0][0] * A[1][1] - A[1][0] * A[0][1];
        } else if constexpr(matsize == 3){
            return A[0][0] * A[1][1] * A[2][2] +//aei
                    A[0][1] * A[1][2] * A[2][0] +//bfg
                    A[0][2] * A[1][0] * A[2][1] -//cdh
                    A[0][2] * A[1][1] * A[2][0] -//ceg
                    A[0][1] * A[1][0] * A[2][2] -//bdi
                    A[0][0] * A[1][2] * A[2][1]; //afh
        } else {
            return std::nan("0"); //
        }
    }
} // END NAMESPACE MATRIX_T
}// END NAMESPACE MATH