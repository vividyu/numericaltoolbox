/**
 * @file ode.hpp
 * @author Gianni Absillis
 * @brief Includes for ODE solvers
 * @version 0.1
 * @date 2022-05-05
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#pragma once
// Definitions
#include <Numtool/Definition/ODE.hpp>
// Explicit Solvers
#include <Numtool/ExplicitSolvers/ExplicitEuler.hpp>
#include <Numtool/ExplicitSolvers/RK3TVD.hpp>