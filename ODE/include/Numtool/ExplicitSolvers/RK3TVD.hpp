/**
 * @file RK3TVD.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief RK3 total variation diminishing
 * @version 0.1
 * @date 2022-02-26
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#pragma once
#include <Numtool/Definition/ODE.hpp>
#include <cmath>
#include <cstring>
#include <Numtool/MathUtils.hpp>

namespace MATH::ODE {

    template<typename T, typename IDX>
    class RK3_TVD {
        public:
        /**
         * @brief Perform a single timestep
         * 
         * @param [in] nEq the number of equations
         * @param [in] gs the ODE function
         * @param [in] dt the timestep
         * @param [in] t the current time
         * @param [in] x_in the current solution
         * @param [in] x_step the stepped solution (it is safe to use the same pointer as x_in)
         */
        void step(IDX nEq, ode_func<T> &gs, T t, T dt, T *x_in, T *x_step){
            constexpr T one_third = 1.0 / 3.0;
            constexpr T two_third = 2.0 / 3.0;
            T *x1 = new T[nEq];
            T *x2 = new T[nEq];
            T *R0 = new T[nEq];
            T *R1 = new T[nEq];
            T *R2 = new T[nEq];
            memcpy(x1, x_in, nEq * sizeof(T));

            // calculate x1
            gs(x_in, t, R0);
            for(IDX i = 0; i < nEq; i++)
                x1[i] += dt * R0[i];

            // calculate x2
            gs(x1, t + 0.25 * dt, R1);
            for(IDX i = 0; i < nEq; i++)
                x2[i] = 0.75 * x_in[i] + 0.25 * x1[i] + 0.25 * dt * R1[i];

            // calculate x_step
            gs(x2, t + two_third * dt, R2);
            for(IDX i = 0; i < nEq; i++)
                x_step[i] = one_third * x_in[i] + two_third * x2[i] + two_third * dt * R2[i];

            delete[] x1;
            delete[] x2;
            delete[] R0;
            delete[] R1;
            delete[] R2;
        }

    };
}