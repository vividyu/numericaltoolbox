/**
 * @file ExplicitEuler.hpp
 * @author Gianni Absillis (gabsill@ncsu.edu)
 * @brief Explicit Euler integration
 * @version 0.1
 * @date 2022-01-30
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#pragma once
#include <Numtool/Definition/ODE.hpp>
#include <cmath>
#include <Numtool/MathUtils.hpp>
namespace MATH::ODE {


    /**
     * @brief Explicit Euler time integration
     * 
     * @tparam T The floating point type
     * @tparam IDX the indexing type
     */
    template<typename T, typename IDX>
    class ExplicitEuler {
        public:
        /**
         * @brief Perform a single timestep
         * 
         * @param [in] nEq the number of equations
         * @param [in] gs the ODE function
         * @param [in] dt the timestep
         * @param [in] t the current time
         * @param [in] x_in the current solution
         * @param [in] x_step the stepped solution (it is safe to use the same pointer as x_in)
         */
        inline void step(IDX nEq, ode_func<T> &gs, T dt, T t, T *x_in, T *x_step){
            T *K0 = new T[nEq]; // TODO: consider extracting to class level, would need to think about interface for changing the size nEq
            gs(x_in, t, K0);
            for(IDX i = 0; i < nEq; i++) x_step[i] = x_in[i] + K0[i] * dt;
            delete[] K0;
        }

        /**
         * @brief Perform a single timestep and get the L2 norm of the ode function
         * 
         * @param [in] nEq the number of equations
         * @param [in] gs the ODE function
         * @param [in] dt the timestep
         * @param [in] t the current time
         * @param [in] x_in the current solution
         * @param [in] x_step the stepped solution (it is safe to use the same pointer as x_in)
         * @return the 2 norm of the ode function evaluation
         */
        inline T stepandL2(IDX nEq, ode_func<T> &gs, T dt, T t, T *x_in, T *x_step){
            T *K0 = new T[nEq]; // TODO: consider extracting to class level, would need to think about interface for changing the size nEq
            gs(x_in, t, K0);
            for(IDX i = 0; i < nEq; i++) x_step[i] = x_in[i] + K0[i] * dt;
            T K0L2 = 0;
            for(IDX i = 0; i < nEq; i++) K0L2 += SQUARED(K0[i]);
            delete[] K0;
            return std::sqrt(K0L2);
        }

    };

}